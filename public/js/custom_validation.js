jQuery(document).ready(function() {
    jQuery("#messageElement").delay(5000).fadeOut();
    jQuery.validator.addMethod("ckeditorRequired", function(value, textarea) {
        CKEDITOR.instances[textarea.id].updateElement(); // update textarea
        var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags

        if (editorcontent.length === 0) {
            return false;
        } else {
            return true;
        }
    }, translate('Please enter content.'));

    jQuery(".drivers-form, .staffs-form").validate({
        rules: {
            "name": "required",
            "phone_number": "required",
            "email": "required"
        },
        messages: {
            "name": translate("The name field is required."),
            "phone_number": translate("The phone number field is required."),
            "email": translate("The email field is required."),
        },
        errorLabelContainer: "#messageBox",
        wrapper: "li",
        invalidHandler: function(event, validator) {
            jQuery("#messageElement").show().delay(5000).fadeOut();
        }
    });

    jQuery(".products-form, .stocks-form").validate({
        rules: {
            "name": "required",
            "amount": {
                required: true,
                number: true,
            }
        },
        messages: {
            "name": translate("The name field is required."),
            "amount": {
                required: translate("The amount field is required."),
                number: translate("The amount must be a number."),
            },
        },
        errorLabelContainer: "#messageBox",
        wrapper: "li",
        invalidHandler: function(event, validator) {
            jQuery("#messageElement").show().delay(5000).fadeOut();
        }
    });



    jQuery(".sales-form").validate({
        ignore: ":hidden:not(.productCount)",
        rules: {
            "delivery_date": "required",
            "pickup_date": "required",
            "ok_date": "required",
            "operator_name": "required",
            "order_number": "required",
            "products": "required",
        },
        messages: {
            "delivery_date": translate("The delivery date field is required."),
            "pickup_date": translate("The pickup date field is required."),
            "ok_date": translate("The ok date field is required."),
            "operator_name": translate("The operator name field is required."),
            "order_number": translate("The order number field is required."),
            "products": translate("The products field is required."),
        },
        errorLabelContainer: "#messageBox",
        wrapper: "li",
        invalidHandler: function(event, validator) {
            jQuery("#messageElement").show().delay(5000).fadeOut();
        }
    });

    jQuery(".agendas-form").validate({
        rules: {
            "title": "required",
            "agenda_date": "required",
            "time": "required",
        },
        messages: {
            "title": translate("The title field is required."),
            "agenda_date": translate("The agenda date field is required."),
            "time": translate("The time field is required."),
        },
        errorLabelContainer: "#messageBox",
        wrapper: "li",
        invalidHandler: function(event, validator) {
            jQuery("#messageElement").show().delay(5000).fadeOut();
        }
    });

    jQuery(".agendas-modal-form").validate({
        rules: {
            "title": "required",
            "agenda_date": "required",
            "time": "required",
        },
        messages: {
            "title": translate("The title field is required."),
            "agenda_date": translate("The agenda date field is required."),
            "time": translate("The time field is required."),
        },
        errorLabelContainer: "#messageBoxModal",
        wrapper: "li",
        invalidHandler: function(event, validator) {
            jQuery("#messageElementModal").show().delay(5000).fadeOut();
        }
    });

    jQuery(".clients-form").validate({
        rules: {
            "hospital": "required",
            "address" : "required",
            "postcode" : "required",
            "place" : "required",
            "customer_number" : "required",
            "house_number" : "required",
            "remarks" : "required",
            "Contact[1][first_name]" : "required",
            "Contact[1][last_name]" : "required",
            "Contact[1][function]" : "required",
            "Contact[1][phone_number]" : "required",
            "Contact[1][email]" : {
                "required": true,
                "email": true
            }
        },
        messages: {
            "hospital": translate("The hospital field is required."),
            "address" : translate("The address field is required."),
            "postcode" : translate("The postcode field is required."),
            "place" : translate("The place field is required."),
            "customer_number" : translate("The customer number field is required."),
            "house_number" : translate("The house number field is required."),
            "remarks" : translate("The remarks field is required."),
            "Contact[1][first_name]" : translate("The first name field is required."),
            "Contact[1][last_name]" : translate("The last name field is required."),
            "Contact[1][function]" : translate("The function field is required."),
            "Contact[1][phone_number]" : translate("The phone number field is required."),
            "Contact[1][email]" : {
                required: translate("The email field is required."),
                email: translate("The email must be a valid email address."),
            }
        },
        errorLabelContainer: "#messageBox",
        wrapper: "li",
        invalidHandler: function(event, validator) {
            jQuery("#messageElement").show().delay(5000).fadeOut();
        }
    });
    
    validate_contacts();    
    
    
    jQuery(".products-form, .stocks-form").validate({
        rules: {
            "name": "required",
            "amount": {
                required: true,
                number: true
            }
        },
        messages: {
            "name": translate("The name field is required."),
            "amount": {
                required: translate("The amount field is required."),
                number: translate("The amount must be a number.")
            }
        },
        errorLabelContainer: "#messageBox",
        wrapper: "li",
        invalidHandler: function(event, validator) {
            jQuery("#messageElement").show().delay(5000).fadeOut();
        }
    });
    
    jQuery(".emailtemplates-form").validate({
        ignore: [],   
        rules: {
            "name": "required",
            "code": "required",
            "subject": "required",
            "attachment": {                
                extension: "docx|doc|pdf"
            },
            "description": "required",
        },
        messages: {
            "name": translate("The name field is required."),
            "code": translate("The code field is required."),
            "subject": translate("The subject field is required."),
            "attachment": {
                extension: translate("The attachment must be a file of type: doc, docx, pdf."),
            },
            "description": translate("The description field is required."),
        },
        errorLabelContainer: "#messageBox",
        wrapper: "li",
        invalidHandler: function(event, validator) {
            jQuery("#messageElement").show().delay(5000).fadeOut();
        }
    });

});

/**
 * Function to validate contacts in add client form
 */
function validate_contacts(){
    
    jQuery('.contact_first_name').each(function() {
        jQuery(this).rules( "add", {
            required: true,
            messages: {
              required: translate("The first name field is required.")
            }
        });
    });

    jQuery('.contact_last_name').each(function() {
        jQuery(this).rules( "add", {
            required: true,
            messages: {
              required: translate("The last name field is required.")
            }
        });
    });

    jQuery('.contact_function').each(function() {
        jQuery(this).rules( "add", {
            required: true,
            messages: {
              required: translate("The function field is required.")
            }
        });
    });

    jQuery('.contact_phone_number').each(function() {
        jQuery(this).rules( "add", {
            required: true,
            messages: {
              required: translate("The phone number field is required.")
            }
        });
    });

    jQuery('.contact_email').each(function() {
        jQuery(this).rules( "add", {
            required: true,
            email: true,
            messages: {
              required: translate("The email field is required."),
              email: translate("The email must be a valid email address."),
            }
        });
    });
    
    jQuery(".users-form").validate({
        rules: {
            "username": "required",
            "email": "required",
            "password": "required",
            "password_confirmation": {
                required: "required",
                equalTo: "#password"
            }
        },
        messages: {
            "username": translate("The username field is required."),
            "email": translate("The email field is required."),
            "password": translate("The password field is required."),
            "password_confirmation": {
                required: translate("The password confirmation field is required."),
                equalTo: translate("Please enter same password as new password.")
            }
        },
        errorLabelContainer: "#messageBox",
        wrapper: "li",
        invalidHandler: function(event, validator) {
            jQuery("#messageElement").show().delay(5000).fadeOut();
        }
	});
        
}
