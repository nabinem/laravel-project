jQuery(function() {
var add;

    jQuery(".set_single").click(function() {
        var pid = jQuery(this).attr("data-pid");
        var rid = jQuery(this).attr("data-rid");
        var iconClass = jQuery(this).children('i').attr('class');
        var newIconClass;
        jQuery.ajax({
            type: "POST",
            url: "ajax_set_permission",
            data: {
                permission_id: pid,
                role_id: rid,
            },
            dataType: "json",
            success: function(data) {
                if (data.status) {
                    if (iconClass === 'fa fa-times') {
                        newIconClass = 'fa fa-check';
                    } else {
                        newIconClass = 'fa fa-times';
                    }
                    jQuery(".set_single[data-pid=" + pid + "][data-rid=" + rid + "]").children('i').attr('class', newIconClass);
                }
            }
        });

    });

    jQuery(".set_all").click(function() {
        var rid = jQuery(this).attr("data-rid");
        var iconClass = jQuery(this).children('i').attr('class');
        group_assign(rid, iconClass, 1);
    });
    
    jQuery(".reset_all").click(function() {
        var rid = jQuery(this).attr("data-rid");
        var iconClass = jQuery(this).children('i').attr('class');
        group_assign(rid, iconClass, 0);
    });

    function group_assign(rid, iconClass, add) {
        jQuery.ajax({
            type: "POST",
            url: "ajax_set_permission",
            data: {
                role_id: rid,
                group_assign: add
            },
            dataType: "json",
            success: function(data) {
                if (data.status) {
                    if (iconClass === 'fa fa-times') {
                        newIconClass = 'fa fa-check';
                    } else {
                        newIconClass = 'fa fa-times';
                    }
                    jQuery(".set_single[data-rid=" + rid + "]").children('i').attr('class', iconClass);
                }
            }
        });
    }

});


