jQuery(document).ready(function() {
    jQuery("#messageElement").delay(5000).fadeOut();
    
    jQuery(".userlogin-form").validate({
        rules: {
            "email": "required",
            "password": "required",
        },
        messages: {
            "email": translate("The email field is required."),
            "password": translate("The password field is required."),
        },
        errorLabelContainer: "#messageBox",
        wrapper: "li",
        invalidHandler: function(event, validator) {
            jQuery("#messageElement").show().delay(5000).fadeOut();
        }
	});

});
