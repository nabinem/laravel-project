jQuery(function() {
    jQuery('a.serial-number').editable({
        url: 'ajax_add_serial',  
        onblur: 'submit',
        validate: function(value) {
            if (jQuery.trim(value) == '') {
                return null;
            }
        },
        success: function(response) {
            var response = jQuery.parseJSON(response);
            if (response.status == false)
                return response.msg;
        }
    });

});