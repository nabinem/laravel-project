/*date of birth datepicker*/
var d = new Date();
//format is DD/MM/YYYY is used
//since getMonth gives the month in (0-11)
var today = d.getDate() + '-' + (d.getMonth() + 1) + '-' + d.getFullYear();
var tomorrowDate = new Date(d.getTime() + 1000 * 60 * 60 * 24);
var tomorrow = tomorrowDate.getDate() + '-' + (tomorrowDate.getMonth() + 1) + '-' + tomorrowDate.getFullYear();
var after5daysDate = new Date(d.getTime() + (1000 * 60 * 60 * 24 * 5)); //5 days
var fifthDay = after5daysDate.getDate() + '-' + (after5daysDate.getMonth() + 1) + '-' + after5daysDate.getFullYear();

jQuery(function() {    
	//jQuery(".alert").show().delay(5000).fadeOut();
    
    jQuery('a.add_contact').on('click', '', function() {
        cloneContact();
    })
    
    jQuery('.past_date').datetimepicker({
        pickTime: false,
        format: 'DD-MM-YYYY',
        maxDate: today
    });
    
    jQuery('.future_date').datetimepicker({
        pickTime: false,
        format: 'DD-MM-YYYY',
        minDate: today
    });
    
    jQuery('#delivery_date, #ok_date').datetimepicker({
        pickTime: false,
        format: 'DD-MM-YYYY',
        minDate: today,
        defaultDate: today,
    });
    
    jQuery('#pickup_date').datetimepicker({
        pickTime: false,
        format: 'DD-MM-YYYY',
        minDate: tomorrow,
        defaultDate: fifthDay,
    });
    
    jQuery('.timepicker').datetimepicker({
        pickDate: false,
        format: 'HH:mm'
    });
    
    jQuery("#delivery_date").on("dp.change", function(e) {
        //var addfivedays = moment(e.date).add(5, 'days');        
        //var newdate = new Date(addfivedays);                
        var tomorrow_day = moment(e.date).add(1, 'days'); 
        var add_days = moment(e.date).add(5, 'days');    //5 days    
        var newdate = new Date(add_days);
        var newday = ( '0' + newdate.getDate() ).slice( -2 );
        var newmonth = ( '0' + (newdate.getMonth() + 1) ).slice( -2 );
        var newyear = newdate.getFullYear();
        var new_pickup_date = newday + '-' + newmonth + '-' + newyear;
        //jQuery('#pickup_date').data("DateTimePicker").setMinDate(addfivedays);
        jQuery('#pickup_date').data("DateTimePicker").setMinDate(tomorrow_day);
        jQuery('#pickup_date input').val(new_pickup_date);
    });
    
    jQuery('#add_product_button').click(function() {
        if(jQuery(".productsInput").val() == null){
            var message = translate("Please Select atleast one product.");
            jQuery(".addProductserror").html(message); 
            jQuery("#addProductsElement").show().delay(3000).fadeOut();
        }else{
            var productIDs = jQuery(".productsInput").val();            
            var productQuantity = jQuery(".productQuantity").val();
            var productNames = []; 
            var product = []; 
            var productText;
            var count = 0;
            var error = 0;
            jQuery('.productsInput :selected').each(function(i, selected){
                productText = jQuery(selected).text().split('(');
                productNames[this.value] = jQuery.trim(productText[0]);
                var id = this.value;
                var name = jQuery.trim(productText[0]);
                var quantity = productQuantity;
                var available = get_available_products(id);

                if(available<quantity){
                    var message = translate("Insufficient product.");
                    jQuery(".addProductserror").html(message); 
                    jQuery("#addProductsElement").show().delay(3000).fadeOut();
                    error = 1;
                }
                product[count] = [id, name, quantity];    
                count = count +1;
            });   
            
            if(!error){
                jQuery.each(product, function( key, productArray ) {
                    var productId = productArray[0];
                    var productName = productArray[1];
                    var productAmount = productArray[2];
                    var inputProduct = new Array(productId, productAmount);
                    var productHtml = '<span class="lbl lbl-remavable font-sm" id="selectedProductDiv_'+productId+'">'+
                            '<input type="hidden" name="product_ids[]" value="'+inputProduct+'">'+
                            '<span class="lbl-product">'+productName+'</span>  '+'(<span id="selectedProductAmount_'+productId+'">'+productAmount+'</span>)  '+
                            '<a href="javascript:;" onclick="minusProduct('+productId+')" class="lbl-button lbl-minus"><span class="fa fa-minus"></span></a>'+
                            '<a href="javascript:;" onclick="plusProduct('+productId+')" class="lbl-button lbl-plus"><span class="fa fa-plus"></span></a>'+
                            '<a href="javascript:;" onclick="removeProduct('+productId+')" class="lbl-button lbl-remove"><span class="fa fa-remove"></span></a></span>';

                    var exist = 0;
                    jQuery("div.selected_products span.lbl-remavable span.lbl-product").each(function(){
                        if(jQuery( this ).text() == productName){
                            exist = 1;
                            var currentAmount = get_selected_products(productId);
                            var newQuantity = parseInt(productAmount)+currentAmount;
                            jQuery("#selectedProductAmount_"+productId).html(newQuantity);
                        }

                    });
                    if(!exist){                    
                        jQuery(".selected_products").append(productHtml);      
                        var productCount = jQuery(".productCount").val();
                        if(!productCount){
                            jQuery(".productCount").val(1);
                        }else{
                            jQuery(".productCount").val(parseInt(jQuery(".productCount").val())+1);
                        }                        
                    }  

                });           

                jQuery('#add_products_modal').modal('toggle');
            }
        }     
    });
           
    
    jQuery('#add_to_cart').click(function() {
        var delivery_date = jQuery("#delivery_date input").val();
        var pickup_date = jQuery('#pickup_date input').val();
        jQuery.ajax({
            type: 'POST',
            url: '/products/ajax_get_product_options',
            data: {                
                delivery_date : delivery_date,  
                pickup_date : pickup_date, 
            },
            dataType: 'JSON',
            success: function(response){
                
                if(response.status == true){
                    var Products = response.products;
                    var ProductsDetail = response.productsDetail;
                    var options='';
                    jQuery.each(Products, function( key, name ) {
                        
                        var onCart = get_selected_products(key);
                        var available = ProductsDetail[key]['available'] - onCart;
                        var total = ProductsDetail[key]['total'];
                        
                        if(available == 0){
                            disableStatus = 'disabled';
                        }else{
                            disableStatus = '';
                        }
                        options = options+'<option value="'+key+'"'+ disableStatus + '>'+ name +'('+available+'/'+total+')'+'</option>';                                        
                    });
                jQuery('.productsInput').html(options); 
                }
            }
        });
    });
    
    
    // trigger support staffs required check 
    jQuery('input[name=support_required]:checked').trigger("change");
    
    
    jQuery('.sales_select_clients').trigger("change");
        
    if(!jQuery('.selected_products span')) {
        jQuery("input[name=products]").attr("value","");
    }
    
    //Planning main checkbox
    jQuery('#selecctall').click(function(event) {  //on click 
        if(this.checked) { // check select status
            jQuery('.planning_check').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "planning_check"               
            });
        }else{
            jQuery('.planning_check').each(function() { //loop through each checkbox
                this.checked = false;  //select all checkboxes with class "planning_check"               
            });
        }
    });
    
    jQuery("#delivery_date,#pickup_date").on("dp.change", function(e) {
        reset_cart();
    });
    
  
    jQuery('.sales-form').submit(function( event ) {  
//    jQuery("#delivery_date,#pickup_date").on("dp.change", function(e) {
        var delivery_date = jQuery("#delivery_date input").val();
        var pickup_date = jQuery('#pickup_date input').val();
        var products = [];
        var product_id;
        var quantity;
        var count = 0;
        jQuery("div.selected_products span.lbl-remavable").each(function( index ) {
            product_id = jQuery( this ).attr('id').slice(19);// slicing selectedProductDiv_ = 19 letters
            quantity = get_selected_products(product_id);
            products[count] = { product_id:product_id, quantity:quantity };
            count = count + 1;
        }); 
        
        var modal = false;
        validate_product_purchase(delivery_date, pickup_date, products, modal, event);        
    });
    
    
    
    
    
               
});
/*end of jQuery(function)*/

jQuery('.sales-form').ready(function() {
    jQuery("#product-hidden").html('<input name="products" value="" type="hidden" class="productCount">');
});


// Changing support staffs required check according to radio button change 
jQuery('input[name=support_required]').change(function() {
    var staff_support = jQuery('input[name=support_required]:checked').val();
    
    //show hide value field for staff support
    if (staff_support == 1) {      
		jQuery('.support_staff_div').show();
		jQuery(".staff_id").prop('disabled', false);
	} else {
		jQuery('.support_staff_div').hide();
		jQuery(".staff_id").prop('disabled', true);
	}
});

// subtract html product from selected list
function minusProduct(id) {
    var onCart = get_selected_products(id);
    if(onCart > 1){        
        var newQuantity = onCart - 1;
    }else{
        var newQuantity = 1;        
        var message = translate("Product cannot be less than 1.");
        jQuery(".addProductsCreateerror").html(message); 
        jQuery("#addProductsCreateElement").show().delay(3000).fadeOut();
    }
    jQuery("#selectedProductAmount_" + id).html(newQuantity);
}

// add html product from selected list
function plusProduct(id) {
    var available = get_available_products(id);
    var onCart = get_selected_products(id);
    if(available > onCart){        
        var newQuantity = onCart + 1;
    }else{
        var newQuantity = available;        
        var message = translate("Product cannot be greater than available.");
        jQuery(".addProductsCreateerror").html(message); 
        jQuery("#addProductsCreateElement").show().delay(3000).fadeOut();
    }
    jQuery("#selectedProductAmount_" + id).html(newQuantity);
}

// remove html product from selected list
function removeProduct(id) {
    jQuery("#selectedProductDiv_" + id).remove();
    
    var productCount = parseInt(jQuery(".productCount").val());
    if (productCount == 1 || productCount < 1) {
        jQuery(".productCount").val('');
    } else {
        jQuery(".productCount").val(parseInt(jQuery(".productCount").val()) - 1);
    } 
}


jQuery('.sales_select_clients').change(function() {

    var selectedClient = jQuery('.sales_select_clients').val();
    
    jQuery.ajax({
        type: 'POST',
        url: '/clients/ajax_get_contacts_list',
        data: {
            selectedClient : selectedClient,            
        },
        dataType: 'JSON',
        success: function(response) {
            var optionsHtml;
            if (response.status == true) {
                jQuery.each(response.contacts, function(index, value) {
                    optionsHtml += '<option value="' + index + '">' + value + '</option>';
                });                
                jQuery('.sales_client_contacts').html('');
                jQuery('.sales_client_contacts').html(optionsHtml);
            } 
        }
    });
    
});



    function cloneContact() {
        var lastBoxNumber = jQuery('div.contact-slots-row div.contact-slot:last-child').find('button.close').attr('data-close');
        var newNumber = parseInt(lastBoxNumber) + 1;
        var newContact = jQuery('div.contact-slot:first').clone();    
        newContact.find('button.close').attr('data-close', newNumber);
        newContact.find('input.contact_first_name').attr('name', 'Contact[' + newNumber + '][first_name]').val("");
        newContact.find('input.contact_last_name').attr('name', 'Contact[' + newNumber + '][last_name]').val("");
        newContact.find('input.contact_function').attr('name', 'Contact[' + newNumber + '][function]').val("");
        newContact.find('input.contact_phone_number').attr('name', 'Contact[' + newNumber + '][phone_number]').val("");
        newContact.find('input.contact_email').attr('name', 'Contact[' + newNumber + '][email]').val("");
        newContact.appendTo('div.contact-slots-row');
        
        /*This is in custom_validation.js*/
        validate_contacts();  
    }
    
    jQuery(document).on('click', 'button.close', function() {
        var totalNumber = jQuery("div.contact-slot").length;
        if(totalNumber > 1){
            jQuery( this ).parentsUntil("div.contact-slots-row").remove();
        }
    });    
    
    
    function get_available_products(productID){ 
        var delivery_date = jQuery("#delivery_date input").val();
        var pickup_date = jQuery('#pickup_date input').val();
        var productAmount;
        jQuery.ajax({
            type: 'POST',
            url: '/products/ajax_get_product_availability',
            data: {                
                delivery_date : delivery_date,  
                pickup_date : pickup_date, 
                productID : productID,            
            },
            dataType: 'JSON',
            async: false,
            success: function(response){
                productAmount = response.quantity;
            }
        });
        var onCart = get_selected_products(productID);
        var totalProduct = productAmount - onCart;
        return productAmount;
        
    }
    
    function get_selected_products(productID){
        var onCart = jQuery("#selectedProductAmount_"+productID).text();
        if(onCart){
            return parseInt(onCart);
        }else{
            return parseInt(0);
        }
    }
    
    function validate_product_purchase(delivery_date, pickup_date, products, modal, event){
        jQuery.ajax({
            type: 'POST',
            url: '/sales/ajax_check_product_validity',
            data: {
                delivery_date : delivery_date,  
                pickup_date : pickup_date, 
                products : products          
            },
            dataType: 'JSON',
            success: function(response){
                    if(response.valid){
                        jQuery("#messageBox").remove("li.validity");
                    }else{
                        jQuery("#messageBox").html('<li class="validity"><label class="error" style="display: inline-block;">'+response.message+'</label></li>'); 
                        jQuery("#messageBox, #messageElement").show().delay(5000).fadeOut();                        
                        event.preventDefault();
                    }
            }
        });   
    }
         
        
    function reset_cart(){
        jQuery(".productCount").val('');
        jQuery(".selected_products").html('');
    }
