/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
var lang = new Array();

//language for login validation.js
lang["The email field is required."] = "Het veld email is verplicht.";                     
lang["The password field is required."] = "Wachtwoord is een verplicht veld."; 

//language for custom.js custom message
lang["Please Select atleast one product."] = "Selecteer minimaal 1 product.";        
lang["Insufficient product."] = "Onvoldoende producten.";                                  
lang["Product cannot be less than 1."] = "Product waarde mag niet minder dan 1 zijn.";                
lang["Product cannot be greater than available."] = "Product hoeveelheid kan niet groter zijn dan beschikbare hoeveelheid.";        


//language for custom_validation.js
//CKeditor
lang["Please enter content."] = "Vul content in.";  
//drivers-form, staffs-form
lang["The name field is required."] = "Naam is verplicht.";                    
lang["The phone number field is required."] = "Telefoonnummer is verplicht.";         
lang["The email field is required."] = "Email is verplicht.";              

//products-form, stocks-form
lang["The name field is required."] = "Naam is verplicht.";       
lang["The amount field is required."] = "Bedrag is verplicht.";    
lang["The amount must be a number."] = "Het bedrag moet een nummer zijn.";      
//sales-form
lang["The delivery date field is required."] = "Bezorgdatum is een verplicht veld.";     
lang["The pickup date field is required."] = "Ophaal datum is een verplicht veld.";         
lang["The ok date field is required."] = "De Ok datum een verplicht veld.";                  
lang["The operator name field is required."] = "OK ondersteuner is een verplicht veld.";      
lang["The order number field is required."] = "Ordernummer is een verplicht veld.";        
lang["The products field is required."] = "Producten is een verplicht veld.";               

//agendas-form, agendas-modal-form
lang["The title field is required."] = "Titel is een verplicht veld.";                   
lang["The agenda date field is required."] = "Agenda datum is een verplicht veld.";        
lang["The time field is required."] = "Tijd is een verplicht veld.";                     

//clients-form
lang["The hospital field is required."] = "Ziekenhuis is een verplicht veld.";             
lang["The address field is required."] = "Adres is een verplicht veld.";                  
lang["The postcode field is required."] = "Postcode is een verplicht veld.";                     
lang["The place field is required."] = "Plaats is een verplicht veld.";                            
lang["The customer number field is required."] = "Klantnummer is een verplicht veld.";       
lang["The house number field is required."] = "Huisnummer is een verplicht veld.";            
lang["The remarks field is required."] = "Opmerkingen is een verplicht veld.";                   
lang["The first name field is required."] = "Voornaam is een verplicht veld.";             
lang["The last name field is required."] = "Achternaam is een verplicht veld.";               
lang["The function field is required."] = "Functie is een verplicht veld.";                
lang["The phone number field is required."] = "Telefoonnummer is een verplicht veld.";          
lang["The email field is required."] = "Email is een verplicht veld.";                         
lang["The email must be a valid email address."] = "Email moet een geldig email adres zijn.";     

//products-form, stocks-form 
lang["The name field is required."] = "Naam is een verplicht veld.";                   
lang["The amount field is required."] = "Bedrag is een verplicht veld.";                
lang["The amount must be a number."] = "Bedrag moet in cijfers ingevuld worden.";                

//emailtemplates-form"] = "//emailtemplates-form";
lang["The name field is required."] = "Naam invullen is een verplicht veld.";                         
lang["The code field is required."] = "Het code veld is een verplicht veld.";                          
lang["The subject field is required."] = "Onderwerp is een verplicht veld.";                         
lang["The attachment must be a file of type: doc, docx, pdf."] = "De bijlage moet een bestand zijn: doc, docx, pdf.";   
lang["The description field is required."] = "Beschrijving is een verplicht veld.";             

//users-form
lang["The username field is required."] = "Gebruikersnaam is verplicht.";                                
lang["The email field is required."] = "Email is een verplicht veld.";                                
lang["The password field is required."] = "Wachtwoord is een verplicht veld.";                              
lang["The password confirmation field is required."] = "Bevestigen wachtwoord is een verplicht veld.";        
lang["Please enter same password as new password."] = "Vul wachtwoord opnieuw in.";           

/**
 * language to validate contacts in add client form
 */    
lang["The first name field is required."] = "Voornaam is een verplicht veld.";                
lang["The last name field is required."] = "Achternaam is een verplicht veld.";                    
lang["The function field is required."] = "Functie is een verplicht veld.";                      
lang["The phone number field is required."] = "Telefoonnummer is een verplicht  veld.";               
lang["The email field is required."] = "Email is een verplicht veld.";                               
lang["The email must be a valid email address."] = "Email moet een geldig email adres zijn.";     


//Langugae translation
function translate(string) {
    if (string) {
        if (language == "nl") {
            if (lang.hasOwnProperty(string)) {
                return lang[string];
            }
        }
        return string;
    }
}
