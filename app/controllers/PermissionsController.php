<?php

class PermissionsController extends BaseController {

    protected $permission;

    public function __construct(Permission $permission) {
        $this->permission = $permission;
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     * GET /permissions
     *
     * @return Response
     */
    public function index() {
        $permissions = $this->permission->getActionsToSync();
        if (Session::has('sync_permission'))
        {
            Session::forget('sync_permission');
        }
        if(!empty($permissions['add'])){
            Session::put('sync_permission', $permissions['add']);
        }
        if (Session::has('delete_permission_action'))
        {
            Session::forget('delete_permission_action');
        }
        if(!empty($permissions['delete'])){
            Session::put('delete_permission_action', $permissions['delete']);
        }
        return View::make('permissions.index', ['permissions' => $permissions['add'], 'delete' => $permissions['delete']]);
    }
    
        
    public function synchronize() {        
        $deletePermissions = Session::get('delete_permission_action'); 
        if(!empty($deletePermissions)){
            foreach($deletePermissions as $permission){
                DB::table('permission_role')->where('permission_id', '=', $permission->id)->delete();
                DB::table('permissions')->where('id', '=', $permission->id)->delete();
            }    
        }     
        if (Session::has('delete_permission_action'))
        {
            Session::forget('delete_permission_action');
        }
        
        $permissions = Session::get('sync_permission'); 
        if(!empty($permissions)){
            foreach($permissions as $permission){
                foreach($permission['action'] as $action){
                    $manageAction = new Permission;
                    $manageAction->name = $action;  
                    $manageAction->display_name = $permission['controller'];
                    $manageAction->save();
                }
            }
        }
        if (Session::has('sync_permission'))
        {
            Session::forget('sync_permission');
        }   
        
        return Redirect::to('permissions');
    }
    
    
    public function manage_role(){
        $role = new Role;
        
        $roles = $role->all();        
        $permissions = $this->permission->all();        
        $assignedPermissions = $this->permission->getAssignedPermissions();
        
        return View::make('permissions.manage_role', [
            'permissions' => $permissions, 
            'roles' => $roles, 
            'assignedPermissions' => $assignedPermissions
        ]);
        
    }
    
    public function ajax_set_permission(){        
        $newPermissionData = Input::all();    
        
        if(!isset($newPermissionData['group_assign'])){   //only for group assign         
            $assignPermission = $this->permission->assignPermission($newPermissionData);
        }else{            
            $assignPermission = $this->permission->groupAssignPermission($newPermissionData);
        }
        
        if ($assignPermission) {
            echo json_encode(array('status' => true, 'msg' => 'Permission Assigned Successfully'));exit;
        } else {
            echo json_encode(array('status' => false, 'msg' => 'Permission Assigned Failed'));exit;
        }
    }

}
