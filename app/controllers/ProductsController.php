<?php

class ProductsController extends \BaseController {

    protected $product;

    public function __construct(Product $product) {
        $this->Product = $product;
        parent::__construct();
    }

    /**
     * Display a listing of products
     *
     * @return Response
     */
    public function index() {
        $products = $this->Product->paginate(LIMIT);
        foreach($products as $product){
            $product->availability = $this->Product->getAvailabilityforProductTable($product->id);
            $product->firstback = $this->Product->getProductFirstBack($product->id);
        }        
        return View::make('products.index', compact('products','product_availability'));
    }

    /**
     * Show the form for creating a new product
     *
     * @return Response
     */
    public function create() {
        return View::make('products.create');
    }

    /**
     * Store a newly created product in storage.
     *
     * @return Response
     */
    public function store() {
        
        $data = Input::all();
        
        if (!$this->Product->fill($data)->isValid()) {
            return Redirect::back()->withInput()->withErrors($this->Product->errors);
        }  

        $amount = $data['amount'];
        unset($data['amount']);

        $product = Product::create($data);

        $StockModel = new Stock;
        for ($i = 1; $i <= $amount; $i++) {
            $stock['product_id'] = $product->id;
            $StockModel->insert($stock);
        }

        return Redirect::route('products.index');
    }

    /**
     * Display the specified product.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        $product = Product::findOrFail($id);

        return View::make('products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified product.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $product = Product::find($id);

        return View::make('products.edit', compact('product'));
    }

    /**
     * Update the specified product in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        $product = Product::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Product::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $product->update($data);

        return Redirect::route('products.index');
    }

    /**
     * Remove the specified product from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        Product::destroy($id);

        return Redirect::route('products.index');
    }
    
    
    public function ajax_get_product_options(){    
        $delivery_date = normalDateFormat(Input::get('delivery_date'));  
        $pickup_date = normalDateFormat(Input::get('pickup_date')); 
        
        $products = Product::lists('name', 'id');
        $saleModel = new Sale;
        $productsDetail = array();

        foreach ($products as $id => $product) {
            $total_product = $saleModel->getTotalProductCount($id);
            $available_product = $this->Product->getAvailabilityforProductTable($id, $delivery_date, $pickup_date);
            //$sold_product = $saleModel->getSoldProductCount($id);
            //$available_product = $total_product - $sold_product;

            $productsDetail[$id]['available'] = $available_product;
            $productsDetail[$id]['total'] = $total_product;
        }
        
        if (empty($products)) {
            echo json_encode(array('status' => false));exit;
        } else {
            echo json_encode(array('status' => true, 'products' => $products, 'productsDetail' => $productsDetail));exit;        
        }        
    }
    
    public function ajax_get_product_availability(){              
        $delivery_date = normalDateFormat(Input::get('delivery_date'));  
        $pickup_date = normalDateFormat(Input::get('pickup_date')); 
        
        $productID = Input::get('productID');         
        $availableAmount = $this->Product->getAvailabilityforProductTable($productID,$delivery_date,$pickup_date);
        echo json_encode(array('quantity' => $availableAmount));exit;        
    }

}
