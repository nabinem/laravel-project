<?php

class BaseController extends Controller {

    public function __construct()
    {
        $this->beforeFilter(function()
        {
            $this->setLanguage();//setting language
            
            if($this->checkPermission()){
                return Redirect::to('/');
            }
        });
    }

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
    
    private function setLanguage() {
        //if local english
        if($_SERVER['REMOTE_ADDR'] == '127.0.0.1' || $_SERVER['REMOTE_ADDR'] == '::1'){            
            App::setLocale("en");
        }
        else {
            App::setLocale("nl");
        }
    }
    
    
    private function checkPermission(){
        $user = Auth::user();
        if($user){
            
            //allow routes to every user here example = 'HomeController@dashboard'
            $allowedRoutes = array(
                'HomeController@dashboard',
                'UsersController@logout',
            );
            
            $user_id = Auth::user()->id;
            $currentController_Action = Route::currentRouteAction();
            
            $current_url = explode("@", $currentController_Action);
            
            /*Allow permission controller only to admin users*/
            if(($current_url[0] == 'PermissionsController') && ($user->hasRole("Admin"))){
                return false;
            }
            
            /*allow routes list*/
            if(in_array($currentController_Action, $allowedRoutes)){
                return false;
            }
            
            /*check permission from database*/
            $permissionExists = DB::table('permissions')                
                    ->leftJoin('permission_role', 'permission_role.permission_id', '=', 'permissions.id')
                    ->leftJoin('assigned_roles', 'assigned_roles.role_id', '=' ,'permission_role.role_id')
                    ->where('display_name', '=', $current_url[0])//controller
                    ->where('name', '=', $current_url[1])//action                
                    ->where('user_id', '=', $user_id)//user
                    ->first();
            
            if(!$permissionExists){
                return true;
            }
        }
        return false;
    }

}
