<?php

class PlanningsController extends \BaseController {

    protected $plannings;
    
    public function __construct(Planning $planning) {
        $this->Planning = $planning;
        parent::__construct();
    }

    /**
     * Display a listing of plannings
     *
     * @return Response
     */
    public function overview() {
        $saleModel = new Sale;
        $agendaModel = new Agenda;
        
//        $today_date = date('Y-m-d');
//        $sevendays = strtotime("+".PLANNINGSDAYSNUMBER." day");
//        $next_date = date('Y-m-d', $sevendays);
        
        $deliveries = $saleModel 
                ->select( array(
                    'sales.delivery_date', 
                    'sales.order_number', 
                    'sales.remarks', 
                    DB::raw('clients.hospital as client_name'), 
                    DB::raw('products.name as product_name'), 
                    DB::raw('sale_products.id as sale_product_id'), 
                    DB::raw('drivers.name as driver_name')
                ) ) 
                ->leftJoin('clients', 'clients.id', '=', 'sales.client_id')
                ->leftJoin('sale_products', 'sale_products.sale_id', '=', 'sales.id')
                ->leftJoin('products', 'products.id', '=', 'sale_products.product_id')
                ->leftJoin('drivers', 'sales.driver_id', '=', 'sales.driver_id')
//                ->where('delivery_date', '>=', $today_date)
//                ->where('delivery_date', '<', $next_date)
                ->where('delivery_archived', '=', 0)
                ->get();
        
        $pickups = $saleModel
                ->select( array(
                    'sales.pickup_date', 
                    'sales.order_number', 
                    'sales.remarks', 
                    DB::raw('clients.hospital as client_name'), 
                    DB::raw('products.name as product_name'), 
                    DB::raw('sale_products.id as sale_product_id'), 
                    DB::raw('drivers.name as driver_name')
                ) )    
                ->leftJoin('clients', 'clients.id', '=', 'sales.client_id')
                ->leftJoin('sale_products', 'sale_products.sale_id', '=', 'sales.id')
                ->leftJoin('products', 'products.id', '=', 'sale_products.product_id')
                ->leftJoin('drivers', 'sales.driver_id', '=', 'sales.driver_id')
//                ->where('pickup_date', '>=', $today_date)
//                ->where('pickup_date', '<', $next_date)
                ->where('pickup_archived', '=', 0)
                ->get();
        
        $staffs = $saleModel
                ->select( array(
                    'sales.ok_date', 
                    'sales.time', 
                    'sales.order_number', 
                    'sales.remarks', 
                    DB::raw('clients.hospital as client_name'), 
                    DB::raw('products.name as product_name'), 
                    DB::raw('sale_products.id as sale_product_id'), 
                    DB::raw('drivers.name as driver_name') 
                ))    
                ->leftJoin('clients', 'clients.id', '=', 'sales.client_id')
                ->leftJoin('sale_products', 'sale_products.sale_id', '=', 'sales.id')
                ->leftJoin('products', 'products.id', '=', 'sale_products.product_id')
                ->leftJoin('drivers', 'sales.driver_id', '=', 'sales.driver_id')
                //->leftJoin('staffs', 'staffs.id', '=', 'sales.staff_id')
//                ->where('ok_date', '>=', $today_date)
//                ->where('ok_date', '<', $next_date)
                ->where('support_required', '=', 1)
                ->where('support_archived', '=', 0)
                ->get();
        
        $agendas = $agendaModel
//                ->where('agenda_date', '>=', $today_date)
//                ->where('agenda_date', '<', $next_date)
                ->where('agenda_archived', '=', 0)
                ->get();
        
        $min_max_date = $this->Planning->getMinAndMaxDate($deliveries,$pickups,$staffs,$agendas);
        
        return View::make('plannings.overview', compact('deliveries','pickups','staffs','agendas','min_max_date'));
    }
    
    
    public function action()
    {
        $data = Input::get();
        if(!isset($data['Planning'])){            
            $error = trans('messages.Select at least one plan.');        
            return Redirect::back()->withErrors($error)->withInput();
        }
        //check which submit was clicked on
        if(Input::get('print')) {
            $saleModel = new Sale;
            $saleModel->print_for_driver($data['Planning'],true);            
        } elseif(Input::get('archive')) {
            /*Archive planning data*/
            foreach($data['Planning'] as $key => $plan){                                
                $attr = explode("_", $key);
                $save_data = array();
                if($attr[0] == 'agenda'){
                    $table = 'agendas';
                }
                else{
                    $table = 'sale_products';
                }
                $table_id = $attr[1];
                $table_field = $attr[0].'_archived';
                
                DB::table($table)
                ->where('id', $table_id)
                ->update(array($table_field => 1)); 
            }
        }
        return Redirect::to('plannings');
    }  
    

    /**
     * Display a listing of plannings
     *
     * @return Response
     */
    public function archives($archive_type = 'delivery') {
        $saleModel = new Sale;
        $agendaModel = new Agenda;
        
        
        
        $search = Input::get('search');        
        
        if($archive_type == 'delivery'){
            $archives = $saleModel->getDeliveryArchive($search);
        }elseif($archive_type == 'pickup'){
            $archives = $saleModel->getPickupArchive($search);
        }elseif($archive_type == 'staff'){
            $archives = $saleModel->getStaffArchive($search);
        }elseif($archive_type == 'agenda'){
            $archives = $agendaModel->getAgendaArchive($search);
        }else{
            return Redirect::to('plannings');
        }
        
        return View::make('plannings.archives', compact('archives','archive_type','search'));
    }

}
