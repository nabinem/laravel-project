<?php

class DriversController extends \BaseController {

    protected $driver;

    public function __construct(Driver $driver) {
        $this->Driver = $driver;
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     * GET /drivers
     *
     * @return Response
     */
    public function index($id = null) {
        
        if(!empty($id)){
            
            $driver = $this->Driver->findorFail($id);
            
        }

        $drivers = $this->Driver->paginate(LIMIT);

        return View::make('drivers.index',compact('driver', 'drivers'));
    }

    /**
     * Show the form for creating a new resource.
     * GET /drivers/create
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     * POST /drivers
     *
     * @return Response
     */
    public function store() {
        
        $input = Input::all();
        
        if (!$this->Driver->fill($input)->isValid()) {
            return Redirect::back()->withInput()->withErrors($this->Driver->errors);
        }        
        
        $this->Driver->save();

        return Redirect::route('drivers.index')->with('success', 'Record successfully updated');
        
    }

    /**
     * Display the specified resource.
     * GET /drivers/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * GET /drivers/{id}/edit
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     * PUT /drivers/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        
        $driver = $this->Driver->findorFail($id);
        
        $driver->name = Input::get('name');
        $driver->phone_number = Input::get('phone_number');
        $driver->email = Input::get('email');
        
        if ($driver->isValid($id)) {
            $driver->update();
            return Redirect::route('drivers.index')->with('success', 'Record successfully updated');
        }
        return Redirect::back()->withInput()->withErrors($driver->errors);
        
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /drivers/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {

        $driver = $this->Driver->find($id);
        if ($driver) {
            $driver->delete();
            return Redirect::route('drivers.index')->with('success', Lang::get('messages.Record deleted successfully.'));
        } else {
            return Redirect::back()->with('error', 'Record does not exists.');
        }

    }

}
