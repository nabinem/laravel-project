<?php

class EmailTemplatesController extends \BaseController {

    protected $email_templates;

    public function __construct(EmailTemplate $email_templates) {
        $this->EmailTemplate = $email_templates;
        parent::__construct();
    }

	/**
	 * Display a listing of email_templates
	 *
	 * @return Response
	 */
	public function index()
	{
            
            $email_templates = $this->EmailTemplate
                ->select( array('email_templates.*', DB::raw('countries.name as country') ) )    
                ->leftJoin('countries', 'countries.id', '=', 'email_templates.country_id')
                ->get();
            
            return View::make('email_templates.index', compact('email_templates'));
	}

	/**
	 * Show the form for creating a new emailtemplate
	 *
	 * @return Response
	 */
	public function create()
	{
        
                $countries = DB::table('countries')->lists('name', 'id');
                
		return View::make('email_templates.create', ['countries' => $countries]);
	}

	/**
	 * Store a newly created emailtemplate in storage.
	 *
	 * @return Response
	 */
	public function store()
	{       
        $input = Input::all();
        if (!$this->EmailTemplate->fill($input)->isValid()) {
            return Redirect::back()->withInput()->withErrors($this->EmailTemplate->errors);
        }     
        
        
        if(Input::hasFile('attachment'))
        {
            $file = Input::file('attachment');
            $name = time() . '-' . $file->getClientOriginalName();
            $file = $file->move(EMAILATTACHMENTLOCATION, $name);
            $input['attachment'] = $name;
        }
        
            EmailTemplate::create($input);

            return Redirect::route('email_templates.index');
	}

	/**
	 * Display the specified emailtemplate.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$emailtemplate = EmailTemplate::findOrFail($id);

		return View::make('email_templates.show', compact('emailtemplate'));
	}

	/**
	 * Show the form for editing the specified emailtemplate.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$emailtemplate = EmailTemplate::find($id);
                
                $countries = DB::table('countries')->lists('name', 'id');

		return View::make('email_templates.create', compact('emailtemplate','countries'));
	}

	/**
	 * Update the specified emailtemplate in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{        
        $input = Input::all();
        
        if (!$this->EmailTemplate->fill($input)->isValid($id)) {            
            return Redirect::back()->withInput()->withErrors($this->EmailTemplate->errors);        
        }
        
        $emailtemplate = $this->EmailTemplate->findorFail($id);
        
        $emailtemplate->country_id = Input::get('country_id');
        $emailtemplate->name = Input::get('name');
        //$emailtemplate->code = Input::get('code');
        $emailtemplate->subject = Input::get('subject');
        $emailtemplate->description = Input::get('description');
        
        if(Input::hasFile('attachment'))
        {
            $oldfile = EMAILATTACHMENTLOCATION. $emailtemplate->attachment;
            
            if (File::exists($oldfile)) {
                File::delete($oldfile);
            } 

            $file = Input::file('attachment');
            $name = time() . '-' . $file->getClientOriginalName();
            $file = $file->move(EMAILATTACHMENTLOCATION, $name);
            $emailtemplate->attachment = $name;;
        }
        
        $emailtemplate->update();
        
        return Redirect::route('email_templates.index')->with('success', 'Record successfully updated');        
	}

	/**
	 * Remove the specified emailtemplate from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		EmailTemplate::destroy($id);

		return Redirect::route('email_templates.index');
	}
    
    public function reminder_email(){
        
        $sent = $this->EmailTemplate->reminder_email();
        
    }

}
