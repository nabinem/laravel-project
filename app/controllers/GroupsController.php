<?php

class GroupsController extends \BaseController {

    protected $group;

    public function __construct(Group $group) {
        $this->Group = $group;
        parent::__construct();
    }

    /**
     * Display a listing of groups
     *
     * @return Response
     */
    public function index() {

        $search = Input::get('search');

        $groups = Group::where('name', 'like', '%' . $search . '%')
                ->paginate(LIMIT);

        return View::make('groups.index', compact('groups', 'search'));
    }

    /**
     * Show the form for creating a new group
     *
     * @return Response
     */
    public function create() {
        return View::make('groups.form');
    }

    /**
     * Store a newly created group in storage.
     *
     * @return Response
     */
    public function store() {
        
        $input = Input::all();
        if (!$this->Group->fill($input)->isValid()) {
            return Redirect::back()->withInput()->withErrors($this->Group->errors);
        }     

        Group::create($input);

        return Redirect::route('groups.index');
    }

    /**
     * Show the form for editing the specified group.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {

        $group = Group::where('id', '!=', 1)->findorFail($id);//basically 1 is for admin. this should not be changed;

        // show the edit form and pass the nerd
        return View::make('groups.form')->with('group', $group);
    }

    /**
     * Update the specified group in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        
        $input = Input::all();
        
        if (!$this->Group->fill($input)->isValid($id)) {            
            return Redirect::back()->withInput()->withErrors($this->Group->errors);        
        }
        
        
        $group = $this->Group->findorFail($id);
        
        $group->name = Input::get('name');

        $group->update();

        return Redirect::route('groups.index');
    }

    /**
     * Remove the specified group from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        
        $assigned_roles_results = DB::table('assigned_roles')->where('role_id', '=', $id)->count();
        
        //$permission_role_results = DB::table('permission_role')->where('role_id', '=', $id)->count();
        
        //if no found
        //if(($assigned_roles_results < 1) && ($permission_role_results < 1)){
        if($assigned_roles_results < 1){

            Group::destroy($id);
            
            return Redirect::route('groups.index')->with('success', Lang::get('messages.Record deleted successfully.'));

        }
        
        //return Redirect::route('groups.index');
        return Redirect::back()->with('error', Lang::get('messages.You cannot delete this group.'));
    }

}
