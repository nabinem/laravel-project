<?php

/**
 * UsersController Class
 *
 * Implements actions regarding user management
 */
class UsersController extends BaseController
{
    
    protected $user;
    
    public function __construct(User $user) {
        $this->user = $user;
        parent::__construct();
    }

    /**
     * Displays the form for account creation
     *
     * @return  Illuminate\Http\Response
     */
    public function create()
    {
        
        $groups = DB::table('roles')->lists('name', 'id');
        
         /*if (Confide::user()) {            
            return Redirect::to('/');
        } else {     */       
            return View::make('users.form', ['groups' => $groups]);
       /* } */
        
    }

    /**
     * Stores new account
     *
     * @return  Illuminate\Http\Response
     */
    public function store()
    {
        $repo = App::make('UserRepository');
        
        $input_data = Input::all();
        $role_id = Input::get('group');
        unset($input_data['group']);
        
        $user = $repo->signup($input_data);

        if ($user->id) {                             
            $assignRole = new Role();
            $assignRole->assign_manager_role($user->id, $role_id);

            return Redirect::action('UsersController@index')->with('success', 'Record successfully updated');
        } else {
            $error = $user->errors()->all(':message');

            return Redirect::action('UsersController@create')
                ->withInput(Input::except('password'))
                ->with('error', $error);
        }
    }        
        
    /**
     * Displays the login form
     *
     * @return  Illuminate\Http\Response
     */
    public function login()
    {
        if (Confide::user()) {            
            return Redirect::to('/');
            //return Redirect::to('/');
        } else {            
            return View::make('users.login');
            //return View::make(Config::get('confide::login_form'));
        }
    }

    /**
     * Attempt to do login
     *
     * @return  Illuminate\Http\Response
     */
    public function doLogin()
    {
        $repo = App::make('UserRepository');
        $input = Input::all();

        if ($repo->login($input)) {
            //return Redirect::intended('/');            
            return Redirect::to('/');
        } else {
            if ($repo->isThrottled($input)) {
                $err_msg = Lang::get('confide::confide.alerts.too_many_attempts');
            } elseif ($repo->existsButNotConfirmed($input)) {
                $err_msg = Lang::get('confide::confide.alerts.not_confirmed');
            } else {
                $err_msg = Lang::get('confide::confide.alerts.wrong_credentials');
            }

            return Redirect::action('UsersController@login')
                ->withInput(Input::except('password'))
                ->with('error', $err_msg);
        }
    }

    /**
     * Attempt to confirm account with code
     *
     * @param  string $code
     *
     * @return  Illuminate\Http\Response
     */
    public function confirm($code)
    {
        if (Confide::confirm($code)) {
            $notice_msg = Lang::get('confide::confide.alerts.confirmation');
            return Redirect::action('UsersController@login')
                ->with('notice', $notice_msg);
        } else {
            $error_msg = Lang::get('confide::confide.alerts.wrong_confirmation');
            return Redirect::action('UsersController@login')
                ->with('error', $error_msg);
        }
    }

    /**
     * Displays the forgot password form
     *
     * @return  Illuminate\Http\Response
     */
    public function forgotPassword()
    {
        return View::make(Config::get('confide::forgot_password_form'));
    }

    /**
     * Attempt to send change password link to the given email
     *
     * @return  Illuminate\Http\Response
     */
    public function doForgotPassword()
    {
        if (Confide::forgotPassword(Input::get('email'))) {
            $notice_msg = Lang::get('confide::confide.alerts.password_forgot');
            return Redirect::action('UsersController@login')
                ->with('notice', $notice_msg);
        } else {
            $error_msg = Lang::get('confide::confide.alerts.wrong_password_forgot');
            return Redirect::action('UsersController@doForgotPassword')
                ->withInput()
                ->with('error', $error_msg);
        }
    }

    /**
     * Shows the change password form with the given token
     *
     * @param  string $token
     *
     * @return  Illuminate\Http\Response
     */
    public function resetPassword($token)
    {
        return View::make(Config::get('confide::reset_password_form'))
                ->with('token', $token);
    }

    /**
     * Attempt change password of the user
     *
     * @return  Illuminate\Http\Response
     */
    public function doResetPassword()
    {
        $repo = App::make('UserRepository');
        $input = array(
            'token'                 =>Input::get('token'),
            'password'              =>Input::get('password'),
            'password_confirmation' =>Input::get('password_confirmation'),
        );

        // By passing an array with the token, password and confirmation
        if ($repo->resetPassword($input)) {
            $notice_msg = Lang::get('confide::confide.alerts.password_reset');
            return Redirect::action('UsersController@login')
                ->with('notice', $notice_msg);
        } else {
            $error_msg = Lang::get('confide::confide.alerts.wrong_password_reset');
            return Redirect::action('UsersController@resetPassword', array('token'=>$input['token']))
                ->withInput()
                ->with('error', $error_msg);
        }
    }

    /**
     * Log the user out of the application.
     *
     * @return  Illuminate\Http\Response
     */
    public function logout()
    {
        Confide::logout();

        return Redirect::to('/users/login');
    }
    
    public function index(){          

        $search = Input::get('search');

        $users = User::where('username', 'like', '%'.$search.'%')
                ->orWhere('email', 'like', '%'.$search.'%')
                ->paginate(LIMIT);

        return View::make('users.index', compact('users', 'search'));
        
    }
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {

        $groups = DB::table('roles')->lists('name', 'id');
        $defaultGroupId = DB::table('assigned_roles')->where('user_id','=', $id)->pluck('role_id');

        $user = User::where('id', '!=', 1)->findorFail($id);

        // show the edit form and pass the nerd
        return View::make('users.form',compact('groups','user','defaultGroupId'));

    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        $repo = App::make('UserRepository');
        
        $user = User::findorFail($id);
        
        $user->username = Input::get('username');
        $role_id = Input::get('group');

        if ($repo->save($user)) { //Updating the uniqe same data from ardent plugin
            
            DB::table('assigned_roles')->where('user_id', '=', $id)->update(array('role_id' => $role_id));            
            return Redirect::route('users.index')->with('success', Lang::get('messages.Record edited successfully.'));
        }
        return Redirect::back()->withInput()->withErrors($user->errors());

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {

        $user = User::where('id', '!=', 1)->find($id);//1 is for admin
        if ($user) {
            $user->delete();
            return Redirect::route('users.index')->with('success', Lang::get('messages.Record deleted successfully.'));
        } else {
            return Redirect::back()->with('error', 'Record cannot be deleted.');
        }

    }
}
