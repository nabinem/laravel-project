<?php

class ClientsController extends \BaseController {

    protected $client;

    public function __construct(Client $client) {
        $this->Client = $client;
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     * GET /clientdetails
     *
     * @return Response
     */
    public function index() {
        
        $search = Input::get('search');      
        
        $clients = $this->Client
                ->select( array(   
                    'clients.*',
                    DB::raw('contacts.id as first_contact_id'),
                    DB::raw('contacts.first_name as first_contact'),
                ) )
                ->leftJoin('contacts', 'clients.id', '=', 'contacts.client_id')
                ->where('hospital', 'like', '%'.$search.'%')
                ->orWhere('customer_number', 'like', '%'.$search.'%')
                ->orWhere('first_name', 'like', '%'.$search.'%')
                ->groupBy('client_id')
                ->paginate(LIMIT);
        
        return View::make('clients.index', compact('clients', 'search'));
            
    }

    /**
     * Show the form for creating a new resource.
     * GET /clientdetails/create
     *
     * @return Response
     */
    public function create() {
        
        $countries = DB::table('countries')->lists('name', 'id');
        
        return View::make('clients.create', ['countries' => $countries]);
        
    }

    /**
     * Store a newly created resource in storage.
     * POST /clientdetails
     *
     * @return Response
     */
    public function store() {
        
        $input = Input::all();
        $ContactModel = new Contact;
        $contacts = $input['Contact'];
        unset($input['Contact']);
        $contact = new Contact;
        
        $checkError = $this->Client->fill($input)->isValid($contacts);
        if ($checkError != 'valid') {
            return Redirect::back()->withInput()->withErrors($checkError);
        }     
        
        $this->Client->save();
        for($i = 1; $i<=count($contacts); $i++){ //cuz we started from 1 in form
            $contacts[$i]['client_id'] = $this->Client->id;            
        }
        $ContactModel->insert($contacts);

        return Redirect::route('clients.index');
    }

    /**
     * Display the specified resource.
     * GET /clientdetails/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * GET /clientdetails/{id}/edit
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {        

        $client = $this->Client->findorFail($id);
        //echo '<pre>';print_r($client->contacts[0]);exit;
        $country = $client->country;  
        
        $client->country_id = $country->id;
        
        $countries = DB::table('countries')->lists('name', 'id');
        
        return View::make('clients.create',compact('client', 'countries'));
        //return View::make('clients.create', ['client' => $client, 'countries' => $countries]);
        
    }

    /**
     * Update the specified resource in storage.
     * PUT /clientdetails/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
               
        $client = $this->Client->findOrFail($id);
        
        $input = Input::all();
        $ContactModel = new Contact;
        $contacts = $input['Contact'];
        unset($input['Contact']);
        $contact = new Contact;
        
        $checkError = $this->Client->fill($input)->isValid($contacts);
        if ($checkError != 'valid') {
            return Redirect::back()->withInput()->withErrors($checkError);
        } 
        /*
        if (!$this->Client->fill($input)->isValid()) {
            if (!$ContactModel->fill($contacts[1])->isValid()) {
                $this->Client->errors->merge($ContactModel->errors); 
            }
            return Redirect::back()->withInput()->withErrors($this->Client->errors);
        }      */
        
        $client->update($input);
        
        foreach($contacts as $key => $row){
            $contacts[$key]['client_id'] = $id;            
        }
        $ContactModel->where('client_id', $id)->delete();
        $ContactModel->insert($contacts);        

        return Redirect::route('clients.index')->with('success', 'Record successfully updated');
        
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /clientdetails/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {

        $client = $this->Client->find($id);
        if ($client) {
            $client->delete();
            $client->deleteAssociatedContacts();           
            
            return Redirect::route('clients.index')->with('success', Lang::get('messages.Record deleted successfully.'));
        } else {
            return Redirect::back()->with('error', 'Record does not exists.');
        }

    }
    
    public function contact_person($contact_id){        
        
        $contact = DB::table('contacts')->where('id', $contact_id)->first();
        
        return View::make('clients.contact_person', ['contact' => $contact]);
        
    }
    
    public function ajax_get_contacts_list(){        
        $client_id = Input::get('selectedClient'); 
        
        $contacts = DB::table('contacts')
                ->where('client_id', '=', $client_id)
                ->lists('first_name', 'id'); 
        
        if (empty($contacts)) {
            echo json_encode(array('status' => false));exit;
        } else {
            echo json_encode(array('status' => true, 'contacts' => $contacts));exit;
        }
        
    }


}