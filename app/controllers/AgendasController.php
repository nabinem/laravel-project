<?php

class AgendasController extends \BaseController {

    protected $agenda;

    public function __construct(Agenda $agenda) {
        $this->Agenda = $agenda;
        parent::__construct();
    }

    /**
     * Display a listing of agendas
     *
     * @return Response
     */
    public function index($id = null) {
        
        if(!empty($id)){
            
            $agenda = $this->Agenda->findorFail($id);
            $agenda->agenda_date = dutchDateFormat($agenda->agenda_date);
            $agenda->time = hideSecondsFromTime($agenda->time);
        }
        
        $agendas = $this->Agenda->paginate(LIMIT);

        return View::make('agendas.index', compact('agenda', 'agendas'));
    }

    /**
     * Show the form for creating a new agenda
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created agenda in storage.
     *
     * @return Response
     */
    public function store() {
        $data = Input::all();
        
        if(isset($data['planning'])){
            $planning_redirect = true;
            unset($data['planning']);
        }
        $validator = Validator::make($data, Agenda::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        
        $data['agenda_date'] = normalDateFormat($data['agenda_date']);

        Agenda::create($data);
        
        if(isset($planning_redirect)){            
            return Redirect::to('plannings')->with('success', Lang::get('messages.Agenda added successfully.'));
        }
        return Redirect::route('agendas.index')->with('success', Lang::get('messages.Agenda added successfully.'));
    }

    /**
     * Display the specified agenda.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        $agenda = Agenda::findOrFail($id);

        return View::make('agendas.show', compact('agenda'));
    }

    /**
     * Show the form for editing the specified agenda.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified agenda in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        $agenda = Agenda::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Agenda::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        
        $data['agenda_date'] = normalDateFormat($data['agenda_date']);

        $agenda->update($data);

        return Redirect::route('agendas.index');
    }

    /**
     * Remove the specified agenda from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        Agenda::destroy($id);

        return Redirect::route('agendas.index');
    }

}
