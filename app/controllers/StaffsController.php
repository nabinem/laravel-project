<?php

class StaffsController extends \BaseController {

    protected $staff;

    public function __construct(Staff $staff) {
        $this->Staff = $staff;
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     * GET /staffs
     *
     * @return Response
     */
    public function index($id = null) {
        
        if(!empty($id)){
            
            $staff = $this->Staff->findorFail($id);
            
        }

        $staffs = $this->Staff->paginate(LIMIT);

        return View::make('staffs.index',compact('staff', 'staffs'));
    }

    /**
     * Show the form for creating a new resource.
     * GET /staffs/create
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     * POST /staffs
     *
     * @return Response
     */
    public function store() {
        
        $input = Input::all();
        
        if (!$this->Staff->fill($input)->isValid()) {
            return Redirect::back()->withInput()->withErrors($this->Staff->errors);
        }        
        
        $this->Staff->save();

        return Redirect::route('staffs.index')->with('success', 'Record successfully updated');
        
    }

    /**
     * Display the specified resource.
     * GET /staffs/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * GET /staffs/{id}/edit
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     * PUT /staffs/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        
        $staff = $this->Staff->findorFail($id);
        
        $staff->name = Input::get('name');
        $staff->phone_number = Input::get('phone_number');
        $staff->email = Input::get('email');
        
        if ($staff->isValid($id)) {
            $staff->update();
            return Redirect::route('staffs.index')->with('success', 'Record successfully updated');
        }
        return Redirect::back()->withInput()->withErrors($staff->errors);
        
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /staffs/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {

        $staff = $this->Staff->find($id);
        if ($staff) {
            $staff->delete();
            return Redirect::route('staffs.index')->with('success', Lang::get('messages.Record deleted successfully.'));
        } else {
            return Redirect::back()->with('error', 'Record does not exists.');
        }

    }

}
