<?php

class StocksController extends \BaseController {

    protected $stock;

    public function __construct(Stock $stock) {
        $this->Stock = $stock;
        parent::__construct();
    }

    /**
     * Display a listing of stocks
     *
     * @return Response
     */
    public function index($product_id) {
        $product = Product::findOrFail($product_id);   
        
        $stocks = $this->Stock
                ->where('product_id', '=', $product_id)
                ->paginate(LIMIT);
        
        foreach($stocks as $id => $stock){
            $stockDetail[$id] = $this->Stock->getStockDetails($stock->id); 
        } 
        return View::make('stocks.index', compact('stocks','product','stockDetail'));
    }

    /**
     * Show the form for creating a new stock
     *
     * @return Response
     */
    public function create() {
        return View::make('stocks.create');
    }

    /**
     * Store a newly created stock in storage.
     *
     * @return Response
     */
    public function store() {
        
        $validator = Validator::make($data = Input::all(), Stock::$amount_rule);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }        
        
        $amount = $data['amount'];
        
        for ($i = 1; $i <= $amount; $i++) {
            $stock['product_id'] = $data['product_id'];
            $this->Stock->insert($stock);
        }

        return Redirect::to('stocks/'.$data['product_id']);
    }

    /**
     * Display the specified stock.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        $stock = Stock::findOrFail($id);

        return View::make('stocks.show', compact('stock'));
    }

    /**
     * Show the form for editing the specified stock.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $stock = Stock::find($id);

        return View::make('stocks.edit', compact('stock'));
    }

    /**
     * Update the specified stock in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        $stock = Stock::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Stock::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $stock->update($data);

        return Redirect::route('stocks.index');
    }

    /**
     * Remove the specified stock from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        
        $stock = Stock::find($id);

        Stock::destroy($id);

        return Redirect::to('stocks/'.$stock->product_id);
    }
    
    
    
    public function ajax_add_serial(){          
        $stock_data = Input::all();  
        
        if(empty($stock_data['value'])){
            $stock_data['value'] = null;
        }
        $stock = $this->Stock->findorFail($stock_data['pk']);
        $stock->serial_number = $stock_data['value'];        
               
        if ($stock->isValid($stock->id)) {
            $stock->update();
            echo json_encode(array('status' => true, 'msg' => 'Serial Assigned Successfully'));exit;
        }
        
        echo json_encode(array('status' => false, 'msg' => 'Serial Assigned Failed'));exit;
    }

}
