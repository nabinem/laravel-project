<?php

class SalesController extends \BaseController {

    protected $sale;

    public function __construct(Sale $sale) {
        $this->Sale = $sale;
        parent::__construct();
    }

    /**
     * Display a listing of sales
     *
     * @return Response
     */
    public function index() {
        $sales = $this->Sale->select( array('sales.*', DB::raw('clients.hospital as client_id'), DB::raw('contacts.first_name as contact_id'), DB::raw('sale_products.id as sale_products_id'), DB::raw('products.name as product_name') ) )   
                ->leftJoin('clients', 'clients.id', '=', 'sales.client_id')
                ->leftJoin('contacts', 'contacts.id', '=', 'sales.contact_id')
                ->leftJoin('sale_products', 'sale_products.sale_id', '=', 'sales.id')
                ->leftJoin('products', 'products.id', '=', 'sale_products.product_id')
                ->paginate(LIMIT);
        
        return View::make('sales.index', compact('sales'));
    }

    /**
     * Show the form for creating a new sale
     *
     * @return Response
     */
    public function create() {

        $clients = DB::table('clients')->lists('hospital', 'id');
        
        $contacts = array();

        $drivers = DB::table('drivers')->lists('name', 'id');

        $staffs = DB::table('staffs')->lists('name', 'id');

        return View::make('sales.create', compact('clients','contacts','drivers','staffs'));
    }

    /**
     * Store a newly created sale in storage.
     *
     * @return Response
     */
    public function store() {
        $data = Input::all();
        $productDetails = array();
        if(isset($data['product_ids'])){
            $productDetails = $data['product_ids'];
            unset($data['product_ids']);
        }
        $validator = Validator::make($data, Sale::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        
        /* if it is present then only convert date format */
        $data['delivery_date'] = normalDateFormat($data['delivery_date']);
        $data['pickup_date'] = normalDateFormat($data['pickup_date']);
        $data['ok_date'] = normalDateFormat($data['ok_date']);
        
        unset($data['products']);

        $sale = Sale::create($data);

        $saleProductModel = new SaleProduct;
        foreach($productDetails as $productDetail){
            $singleProductDetail = explode(",", $productDetail);
            //in $singleProductDetail array, 0 key is product_id and 1 key is product quantity
            for($i=0; $i<$singleProductDetail[1];$i++){
                $sale_product['sale_id'] = $sale->id;
                $sale_product['product_id'] = $singleProductDetail[0];
                $sale_product['stock_id'] = $this->Sale->getFirstAvailableStockId($singleProductDetail[0],$data['delivery_date'],$data['pickup_date']);
                $saleProductModel->insert($sale_product);                
            }            
        }     
        $emailTemplateModel = new EmailTemplate;
        
        $emailTemplateModel->confirmation_email($data['contact_id'],$data['client_id']);

        //return Redirect::route('sales.index');
        return Redirect::route('sales.create')->with('success', Lang::get('messages.Sales added successfully.'));
    }

    /**
     * Display the specified sale.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($sale_product_id) {
        
        $sale = $this->Sale->select( array('sales.*', DB::raw('clients.hospital as client_name'), DB::raw('products.name as product_name'), DB::raw('contacts.first_name as contact_name'), DB::raw('drivers.name as driver_name'), DB::raw('staffs.name as staff_name'), DB::raw('stocks.serial_number as serial_number') ) )   
                        ->leftJoin('clients', 'clients.id', '=', 'sales.client_id')
                        ->leftJoin('sale_products', 'sale_products.sale_id', '=', 'sales.id')
                        ->leftJoin('contacts', 'contacts.id', '=', 'sales.contact_id')
                        ->leftJoin('drivers', 'drivers.id', '=', 'sales.driver_id')
                        ->leftJoin('staffs', 'staffs.id', '=', 'sales.staff_id')
                        ->leftJoin('products', 'products.id', '=', 'sale_products.product_id')
                        ->leftJoin('stocks', 'stocks.id', '=', 'sale_products.stock_id')
                        ->where('sale_products.id', '=', $sale_product_id)
                        ->first();
        
        return View::make('sales.show', compact('sale'));
    }

    /**
     * Show the form for editing the specified sale.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $sale = Sale::find($id);

        return View::make('sales.edit', compact('sale'));
    }

    /**
     * Update the specified sale in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        $sale = Sale::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Sale::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $sale->update($data);

        return Redirect::route('sales.index');
    }

    /**
     * Remove the specified sale from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        
        $sale = $this->Sale->find($id);
        if ($sale) {
            $sale->delete();
            $sale->saleproducts()->delete();
            
            return Redirect::route('sales.index')->with('success', Lang::get('messages.Record deleted successfully.'));
        } else {
            return Redirect::back()->with('error', 'Record does not exists.');
        }
        
    }
    
    
    public function print_for_driver($sale_products_id){
        $this->Sale->print_for_driver($sale_products_id);
    }
    
    /**
     * Function to check product validity if it is available for given delivery date and pickup date and desired quantity
     */
    public function ajax_check_product_validity(){
        
        $productModel = new Product;
        $delivery_date = normalDateFormat(Input::get('delivery_date'));  
        $pickup_date = normalDateFormat(Input::get('pickup_date')); 
        $products = Input::get('products');        
        
        foreach($products as $product){
            //$valid = $this->Sale->check_product_avaibility($delivery_date, $pickup_date, $product['product_id'],$product['quantity']);                
            $available_product = $productModel->getAvailabilityforProductTable($product['product_id'], $delivery_date, $pickup_date);
            if($available_product >= $product['quantity']){
                echo json_encode(array('valid' => true));
            }else{
                echo json_encode(array('valid' => false, 'message' => Lang::get('messages.Product unavailable for following date or quantity.')));exit;
            }
        }
        exit;      
        
    }

}
