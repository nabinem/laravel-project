<div class="modal fade" id="add_agendas_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{ trans('messages.Add agenda') }}</h4>
            </div>

            {{ Form::open(['url' => 'agendas','class' => 'form-horizontal agendas-modal-form']) }}
            <div class="modal-body">  

                @include('elements.modal_validation')
                <input type="hidden" name="planning" value="1">
                <div class="form-group">
                    <label class="control-label col-md-4  col-lg-3">{{ trans('messages.Title') }}</label>
                    <div class="col-md-8">
                        {{ Form::text('title',null,array('class' => 'form-control','placeholder' => trans('messages.Title'))) }}
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-8  col-lg-3">{{ trans('messages.Agenda Date') }}</label>
                    <div class="col-md-8">
                        <div class='input-group date datepicker future_date'>
                            {{ Form::text('agenda_date',null,array('class' => 'form-control', 'readonly'=>true, 'placeholder' => trans('messages.Agenda Date'))) }}
                            <span class="input-group-addon"><span class="fa fa-calendar"></span> </span> 
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-8  col-lg-3">{{ trans('messages.Time') }}</label>
                    <div class="col-md-8">
                        <div class='input-group date timepicker' id='time'>
                            {{ Form::text('time',null,array('class' => 'form-control', 'readonly'=>true, 'placeholder' => trans('messages.Time'))) }}
                            <span class="input-group-addon"><span class="fa fa-clock-o"></span> </span> 
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" id='add_product_button'>{{ trans('messages.Add') }}</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('messages.Cancel') }}</button>
            </div>


            {{ Form::close() }}
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
