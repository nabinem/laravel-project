@extends('layouts.default')

@section('content')

<section class="section-content col-sm-8 col-md-9 col-lg-10">
    <h1>{{ trans('messages.Archives') }}</h1>
    <div class="form-horizontal">
        <div class="form-group">
            {{ Form::open(['method' => 'get']) }}
                <div class="col-md-4 col-md-offset-6 col-sm-6 col-sm-offset-1">                        
                    {{ Form::text('search',$search ,array('class' => 'form-control spacer-bottom-s','placeholder' => trans('messages.Search'))) }}
                </div>
                <div class="col-sm-3 col-md-2">
                    {{ Form::button(trans('messages.Submit'),array('class' => 'btn btn-warning btn-block', 'type' => 'submit')) }}
                </div>
            {{ Form::close() }}
        </div>
    </div>    
    <div class="clearfix"></div>
    
    <hr>
            
        <div class="archive-buttons">
            <div class="form-group">
                <div class="col-sm-2">{{ link_to("/archives/delivery", trans('messages.Delivery'), array('class' => 'btn btn-primary btn-block spacer-bottom-sm')) }}</div>
                <div class="col-sm-2">{{ link_to("/archives/pickup", trans('messages.Pickup'), array('class' => 'btn btn-primary btn-block spacer-bottom-sm')) }}</div>
                <div class="col-sm-2">{{ link_to("/archives/staff", trans('messages.Staff'), array('class' => 'btn btn-primary btn-block spacer-bottom-sm')) }}</div>
                <div class="col-sm-2">{{ link_to("/archives/agenda", trans('messages.Agenda'), array('class' => 'btn btn-primary btn-block spacer-bottom-sm')) }}</div>
                
            </div>
        </div>
    
    
    
    
    <div class="table-responsive">
        @if (!$archives->count())
            {{ trans('messages.No archive found!') }}       
        @else 
            <table class="table table-bordered  table-hover">
                <thead>
                    <tr>
                        <th>{{ trans('messages.Date') }}</th>
                        @if($archive_type == 'agenda')
                            <th>{{ trans('messages.Detail') }}</th>
                        @else                       
                            <th>{{ trans('messages.Product') }}</th>
                            <th>{{ trans('messages.Client Name') }}</th>
                            <th>{{ trans('messages.Driver') }}</th>
                            <th>{{ trans('messages.Order Number') }}</th>
                            <th>{{ trans('messages.Remarks') }}</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @if($archive_type == 'delivery')

                        @foreach($archives as $delivery)
                            <tr>
                                <td>{{ dutchDateFormat($delivery->delivery_date) }}</td>
                                <td>
                                    <a href="{{ URL::to('sales', $delivery->sale_product_id) }}">
                                        {{ $delivery->product_name }}
                                    </a>
                                </td>
                                <td>{{ $delivery->client_name }}</td>
                                <td>{{ $delivery->driver_name }}</td>
                                <td>{{ $delivery->order_number }}</td>
                                <td>{{ $delivery->remarks }}</td>
                            </tr>
                        @endforeach

                    @elseif($archive_type == 'pickup')

                        @foreach($archives as $pickup)
                            <tr>
                                <td>{{ dutchDateFormat($pickup->pickup_date) }}</td>
                                <td>
                                    <a href="{{ URL::to('sales', $pickup->sale_product_id) }}">
                                        {{ $pickup->product_name }}
                                    </a>
                                </td>
                                <td>{{ $pickup->client_name }}</td>
                                <td>{{ $pickup->driver_name }}</td>
                                <td>{{ $pickup->order_number }}</td>
                                <td>{{ $pickup->remarks }}</td>
                            </tr>
                        @endforeach

                    @elseif($archive_type == 'staff')

                        @foreach($archives as $staff)
                            <tr>
                                <td>
                                    {{ dutchDateFormat($staff->ok_date) }}
                                    <span>{{ hideSecondsFromTime($staff->time) }}</span>  
                                </td>
                                <td>
                                    <a href="{{ URL::to('sales', $staff->sale_product_id) }}">
                                        {{ $staff->product_name }}
                                    </a>
                                </td>
                                <td>{{ $staff->client_name }}</td>
                                <td>{{ $staff->driver_name }}</td>
                                <td>{{ $staff->order_number }}</td>
                                <td>{{ $staff->remarks }}</td>
                            </tr>
                        @endforeach

                    @elseif($archive_type == 'agenda')

                        @foreach($archives as $agenda)
                            <tr>
                                <td>
                                    {{ dutchDateFormat($agenda->agenda_date) }}
                                    <span>{{ hideSecondsFromTime($agenda->time) }}</span> 
                                </td>
                                <td>           
                                    <a href="{{ URL::to('agendas/view', $agenda->id) }}">         
                                        <span>{{ $agenda->title }}</span> 
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                    @endif
                </tbody>
            </table>
        @endif
    </div>
    
@include('elements.pagination', array('paginator'=> $archives))
</section>

@stop