@extends('layouts.default')

@section('content')

<section class="section-content col-sm-8 col-md-9 col-lg-10">
    <h1>{{ trans('messages.Plannings') }}</h1>
    <hr>
            
    @include('elements.flash_message')
    @include('elements.validation')
    {{ Form::open(array('url' => 'plannings/action')) }}
    
        <div class="table-responsive">
            <table class="table table-bordered  table-hover">
                <thead>
                    <tr>
                        <th colspan="7">
                            <div class="btn-group btn-group-sm">    
                                <input type="submit" name="print" value="{{ trans('messages.Print') }}" class="btn btn-default">
                                <a class="btn btn-default" data-toggle="modal" data-target="#add_agendas_modal">{{ trans('messages.Agenda') }}</a>
                                <input type="submit" name="archive" value="{{ trans('messages.Archive') }}" class="btn btn-default">
                            </div>
                        </th>
                    </tr>
                </thead>
                <thead>
                    <tr>
                        <td><input name="checkall" type="checkbox" id="selecctall"></td>
                        <th>{{ trans('messages.Date') }}</th>
                        <th>{{ trans('messages.Product') }}</th>
                        <th>{{ trans('messages.Client Name') }}</th>
                        <th>{{ trans('messages.Driver') }}</th>
                        <th>{{ trans('messages.Order Number') }}</th>
                        <th>{{ trans('messages.Remarks') }}</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $date = $min_max_date['min'];//initial date?>                    
                    @while ($date <= $min_max_date['max']) 
                        
                        @foreach($deliveries as $delivery)
                            @if($date == $delivery->delivery_date)
                                <tr class="planning-blue">
                                    <td>
                                        {{ Form::checkbox('Planning[delivery_'.$delivery->sale_product_id.']',null,null,array('class' => 'planning_check')) }} 
                                    </td>
                                    <td>{{ dutchDateFormat($delivery->delivery_date) }}</td>
                                    <td>
                                        <a href="{{ URL::to('sales', $delivery->sale_product_id) }}">
                                            {{ $delivery->product_name }}
                                        </a>
                                    </td>
                                    <td>{{ $delivery->client_name }}</td>
                                    <td>{{ $delivery->driver_name }}</td>
                                    <td>{{ $delivery->order_number }}</td>
                                    <td>{{ $delivery->remarks }}</td>
                                </tr>
                            @endif
                        @endforeach
                    
                        @foreach($pickups as $pickup)
                            @if($date == $pickup->pickup_date)
                                <tr class="planning-green">
                                    <td>
                                        {{ Form::checkbox('Planning[pickup_'.$pickup->sale_product_id.']',null,null,array('class' => 'planning_check')) }}
                                    </td>
                                    <td>{{ dutchDateFormat($pickup->pickup_date) }}</td>
                                    <td>
                                        <a href="{{ URL::to('sales', $pickup->sale_product_id) }}">
                                            {{ $pickup->product_name }}
                                        </a>
                                    </td>
                                    <td>{{ $pickup->client_name }}</td>
                                    <td>{{ $pickup->driver_name }}</td>
                                    <td>{{ $pickup->order_number }}</td>
                                    <td>{{ $pickup->remarks }}</td>
                                </tr>
                            @endif
                        @endforeach
                    
                        
                        @foreach($staffs as $staff)
                            @if($date == $staff->ok_date)                            
                                <tr class="planning-yellow">
                                    <td>                                        
                                        {{ Form::checkbox('Planning[support_'.$staff->sale_product_id.']',null,null,array('class' => 'planning_check')) }} 
                                    </td>
                                    <td>
                                        {{ dutchDateFormat($staff->ok_date) }}
                                        <span>{{ hideSecondsFromTime($staff->time) }}</span> 
                                    </td>
                                    <td>
                                        <a href="{{ URL::to('sales', $staff->sale_product_id) }}">
                                            {{ $staff->product_name }}
                                        </a>
                                    </td>
                                    <td>{{ $staff->client_name }}</td>
                                    <td>{{ $staff->driver_name }}</td>
                                    <td>{{ $staff->order_number }}</td>
                                    <td>{{ $staff->remarks }}</td>
                                </tr>
                            @endif
                        @endforeach
                        
                        @foreach($agendas as $agenda)
                            @if($date == $agenda->agenda_date)
                                <tr class="planning-gray">
                                    <td>
                                        {{ Form::checkbox('Planning[agenda_'.$agenda->id.']',null,null,array('class' => 'planning_check')) }}
                                    </td>
                                    <td>
                                        {{ dutchDateFormat($agenda->agenda_date) }}
                                        <span>{{ hideSecondsFromTime($agenda->time) }}</span> 
                                    </td>
                                    <td colspan="5">
                                        <a href="{{ URL::to('agendas/view', $agenda->id) }}">    
                                            <span>{{ $agenda->title }}</span> 
                                        </a>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                        
                        
                        <?php $date = date('Y-m-d',strtotime("+1 day", strtotime($date))); //date increment?>
                    @endwhile    
                </tbody>
            </table>
        </div>
    
    {{ Form::close() }}    
</section>


@include('plannings.add_agendas')
@stop