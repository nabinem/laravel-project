@extends('layouts.default')

@section('content')

<section class="section-content col-sm-8 col-md-9 col-lg-10">
    <h1>{{ trans('messages.Permissions') }}</h1>
    <hr>
        
    @include('elements.flash_message')
    
    <div>{{ link_to("/permissions/manage_role", trans('messages.Manage Role Permission')) }}</div>
    
    @if (empty($permissions) && empty($delete))
        {{ trans('messages.No any actions to synchronize!') }}        
    @else
        @if(!empty($permissions))
            <h4>{{ trans('messages.Add Permission') }}</h4>
            <ul>
                @foreach ($permissions as $permission)           
                    @foreach($permission['action'] as $action)
                    <li>{{ $permission['controller'] }}->{{ $action }} </li>
                    @endforeach
                @endforeach
            </ul>        
        @endif
        
        <div class="clearfix pad10"></div>
        @if(!empty($delete))
            <h4>{{ trans('messages.Delete Permission') }}</h4>
            <ul>
                @foreach ($delete as $permission)           
                    <li>{{ $permission->display_name }}->{{ $permission->name }} </li>
                @endforeach
            </ul>
        @endif
    
        {{ link_to("/permissions/synchronize", trans('messages.Click to synchronize all permissions.')) }}
    
    @endif    

</section>

@stop



