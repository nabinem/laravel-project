@extends('layouts.default')

@section('content')

<section class="section-content col-sm-8 col-md-9 col-lg-10">
    <h1>{{ trans('messages.Permissions') }}</h1>
    <hr>
        
    @include('elements.flash_message')
    
    
    @if (empty($permissions))
        {{ trans('messages.No methods found!') }}        
    @else
    
        <div class="table-responsive">
            
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>{{ trans('messages.Roles') }}</th>
                        <th>{{ trans('messages.Grant All') }}</th>
                        <th>{{ trans('messages.Delete All') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($roles as $role)             
                    <tr>
                        <td>{{ $role->name }} </td> 
                        <td><a href="javascript:void(0);" class="set_all" data-rid="{{ $role->id }}"><i class="fa fa-check"></i></a> </td> 
                        <td><a href="javascript:void(0);" class="reset_all" data-rid="{{ $role->id }}"><i class="fa fa-times"></i></a> </td>
                    </tr>    
                    @endforeach
                </tbody>
            </table>
            
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>{{ trans('messages.Action') }}</th>
                        @foreach ($roles as $role)  
                            <th>{{ $role->name }}</th>
                        @endforeach  
                    </tr>
                </thead>
                <tbody>
                    @foreach ($permissions as $permission)                
                    <tr>
                        <td>{{ $permission->display_name }}->{{ $permission->name }} </td>                
                        @foreach ($roles as $role)  
                            <td>
                                <a href="javascript:void(0);" class="set_single" data-pid="{{ $permission->id }}" data-rid ="{{ $role->id }}">
                                <?php $checkset = false; ?>
                                @foreach($assignedPermissions as $setPermission)  
                                    @if(($setPermission->permission_id == $permission->id) && ($setPermission->role_id == $role->id))

                                        <i class="fa fa-check"></i>
                                        <?php $checkset = true; ?>
                                        @break;
                                    @endif       
                                    
                                @endforeach  
                                
                                @if(!$checkset) 
                                    <i class="fa fa-times"></i>
                                @endif
                                </a>
                            </td>
                        @endforeach  
                    </tr>    
                    @endforeach
                </tbody>
            </table>
        </div>
    
    @endif    

</section>

@stop

@section('scriptbottom')
    {{ HTML::script('js/acl.js') }}
@stop