<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
        {{ HTML::style('css/bootstrap.min.css') }}
        {{ HTML::style('css/font-awesome.min.css') }}
        {{ HTML::style('css/main.css') }}
        {{ HTML::script('js/vendor/modernizr-2.6.2.min.js') }}
        <?php 
        $language = Config::get('app.locale');
        if(isset($language) && $language != ''){ ?>            
            {{ HTML::script('js/lang.nl.js') }}
        <?php }?>
        <script type="text/javascript">
            var language = '<?php echo $language; ?>';
        </script>
    </head>
    <body>

        @yield('content')

        @include('elements.footer')

        {{ HTML::script('js/vendor/jquery-1.10.2.min.js') }}        
        {{ HTML::script('js/bootstrap.min.js') }}
        {{ HTML::script('js/plugins.js') }}
        {{ HTML::script('js/main.js') }}
        {{ HTML::script('js/jquery.validate.min.js') }}
        {{ HTML::script('js/login_validation.js') }}

    </body>
</html>
