@extends('layouts.default')

@section('content')

<section class="section-content col-sm-8 col-md-9 col-lg-10">
    <h1>{{ trans('messages.Email Template') }}</h1>
    <hr>

    
    @if(isset($emailtemplate))
        {{ Form::model($emailtemplate, array('route' => array('email_templates.update', $emailtemplate->id), 'method' => 'PUT','class' => 'form-horizontal emailtemplates-form','role'=>'form', 'files' => true)) }}
    @else
        {{ Form::open(['url' => 'email_templates','class' => 'form-horizontal emailtemplates-form', 'files' => true]) }}
    @endif
        
        @include('elements.validation')
        
        <div class="form-group">
            <label class="control-label col-md-4  col-lg-2">{{ trans('messages.Country') }}</label>
            <div class="col-md-8">
                @if(isset($client))
                {{ Form::select('country_id', $countries, null, array('class' => 'form-control','placeholder' => trans('messages.Country'))) }}
                @else
                {{ Form::select('country_id', $countries, null, array('class' => 'form-control','placeholder' => trans('messages.Country'))) }}
                @endif
            </div>
        </div>
        
        <div class="form-group">
            <label class="control-label col-md-4  col-lg-2">{{ trans('messages.Name') }}</label>
            <div class="col-md-8">
                {{ Form::text('name',null,array('class' => 'form-control','placeholder' => trans('messages.Name'))) }}
            </div>
        </div>
        
        @if(!isset($emailtemplate))
            <div class="form-group">
                <label class="control-label col-md-4  col-lg-2">{{ trans('messages.Code') }}</label>
                <div class="col-md-8">                
                    {{ Form::text('code',null,array('class' => 'form-control','placeholder' => trans('messages.Code'))) }}
                </div>
            </div>
        @endif
        
        <div class="form-group">
            <label class="control-label col-md-4  col-lg-2">{{ trans('messages.Subject') }}</label>
            <div class="col-md-8">                
                {{ Form::text('subject',null,array('class' => 'form-control','placeholder' => trans('messages.Subject'))) }}
            </div>
        </div>
        
        <div class="form-group">
            <label class="control-label col-md-4  col-lg-2">{{ trans('messages.Attachment') }}</label>
            <div class="col-md-8">  
                {{ Form::file('attachment', ['class' => 'form-control']) }}
                
                <?php
                    if(isset($emailtemplate->attachment) && !empty($emailtemplate->attachment)){ 
                        $oldfile = EMAILATTACHMENTLOCATION. $emailtemplate->attachment; 
                    ?>
                        @if(File::exists($oldfile))
                            {{ link_to_asset('files/email_templates/'.$emailtemplate->attachment, $emailtemplate->attachment, array('class' => 'btn btn-default', 'target' => 'blank')) }} 
                        @endif
                <?php } ?>
            </div>
        </div>
        
        <div class="form-group">
            <label class="control-label col-md-4  col-lg-2">{{ trans('messages.Description') }}</label>
            <div class="col-md-8">                
                {{ Form::textarea('description', null,array('id' => 'ckeditor_email_template','class' => 'form-control', 'rows' => "7")) }}
            </div>

        </div>

        <div class="form-group">
            <div class="col-sm-4 ol-sm-offset-4  col-lg-offset-2">
                {{ Form::button(trans('messages.Submit'),array('class' => 'btn btn-primary', 'type' => 'submit')) }}
                {{ link_to('email_templates', trans('messages.Cancel'), array('class' => 'btn btn-default')) }} 
            </div>
        </div>
            
    {{ Form::close() }}
        
    <hr>

</section>

@stop

@include('elements.ckeditor')