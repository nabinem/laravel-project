@extends('layouts.default')

@section('content')
<section class="section-content col-sm-8 col-md-9 col-lg-10">
    <h1>{{ trans('messages.Email Templates') }}</h1>
    <hr>
        
    @include('elements.flash_message')
    
    <div class="form-inline text-right spacer-bottom-md">
        <div class="form-group">
            {{ link_to('email_templates/create', trans('messages.Add'), array('class' => 'btn btn-info')) }}
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>{{ trans('messages.Country') }}</th>
                    <th>{{ trans('messages.Name') }}</th>
                    <th>{{ trans('messages.Code') }}</th>
                    <th>{{ trans('messages.Subject') }}</th>
                    <th>{{ trans('messages.Action') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($email_templates as $email_template)
                <tr>
                    <td>{{ $email_template->country }}</td>
                    <td>{{ $email_template->name }}</td>
                    <td>{{ $email_template->code }}</td>
                    <td>{{ $email_template->subject }}</td>	
                    <td align="center">
                        <a href="{{ URL::to('email_templates/'.$email_template->id.'/edit') }}" class="pull-left">
                            <i class="fa fa-pencil"></i>
                        </a>
                        &nbsp;
                        {{-- Form::open(array('url' => 'email_templates/' . $email_template->id, 'class' => 'pull-left')) }}
                            {{ Form::hidden('_method', 'DELETE') }}
                            {{ Form::button('<i class="fa fa-trash-o"></i>', array('type' => 'submit', 'class' => 'delete-button', 'onclick' => "return confirm('".trans('messages.Are you sure you want to delete this?')."');"))}}
                        {{ Form::close() --}}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    {{--
    @include('elements.pagination', array('paginator'=> $email_templates))
    --}}

</section>

@stop