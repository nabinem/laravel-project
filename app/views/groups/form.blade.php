@extends('layouts.default')

@section('content')


<section class="section-content col-sm-8 col-md-9 col-lg-10">
    <h1>{{ trans('messages.Group') }}</h1>
    <hr>
    
    @include('elements.validation')
    
    @if(isset($group))
        {{ Form::model($group, array('route' => array('groups.update', $group->id), 'method' => 'PUT','class' => 'form-horizontal groups-form','role'=>'form')) }}
    @else
        {{ Form::open(['url' => 'groups','class' => 'form-horizontal groups-form']) }}
    @endif
          
        <div class="form-group">
            <label class="control-label col-md-4  col-lg-2">{{ trans('messages.Name') }}</label>
            <div class="col-md-4">
                {{ Form::text('name', null, array('class' => 'form-control','placeholder' => trans('messages.Name'))) }} 
            </div>
        </div>

        <div class="form-group">

            <div class="col-sm-4 ol-sm-offset-4  col-lg-offset-2">
                {{ Form::button(trans('messages.Save'),array('class' => 'btn btn-primary', 'type' => 'submit')) }}
                {{ link_to('groups', trans('messages.Cancel'), array('class' => 'btn btn-default')) }} 
            </div>
        </div>
        
    {{ Form::close() }}

</section>

@stop



