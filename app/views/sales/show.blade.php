@extends('layouts.default')

@section('content')
<section class="section-content col-sm-8 col-md-9 col-lg-10">
    <h1>{{ trans('messages.Order Detail') }}</h1>
    <hr>
    
    <div class="row">
        <label class="control-label col-md-2  col-lg-2">{{ trans('messages.Product Name') }}</label>
        <div class="col-md-4">{{ $sale->product_name }}</div>
        
        <label class="control-label col-md-2  col-lg-2">{{ trans('messages.Product Serial') }}</label>
        <div class="col-md-4">{{ $sale->serial_number }}</div>
    </div>
    <div class="row">
        <label class="control-label col-md-2  col-lg-2">{{ trans('messages.Delivery date') }}</label>
        <div class="col-md-4">{{ $sale->delivery_date }}</div>
        
        <label class="control-label col-md-2  col-lg-2">{{ trans('messages.Pickup date') }}</label>
        <div class="col-md-4">{{ $sale->pickup_date }}</div>
    </div>
    
    <div class="row">
        <label class="control-label col-md-2  col-lg-2">{{ trans('messages.OK-date') }}</label>
        <div class="col-md-4">{{ $sale->ok_date }}</div>
        
        <label class="control-label col-md-2  col-lg-2">{{ trans('messages.Time') }}</label>
        <div class="col-md-4">{{ $sale->time }}</div>
    </div>
    
    <div class="row">
        <label class="control-label col-md-2  col-lg-2">{{ trans('messages.Client Name') }}</label>
        <div class="col-md-4">{{ $sale->client_name }}</div>
        
        <label class="control-label col-md-2  col-lg-2">{{ trans('messages.Contact Person') }}</label>
        <div class="col-md-4">{{ $sale->contact_name }}</div>
    </div>
    
    <div class="row">
        <label class="control-label col-md-2  col-lg-2">{{ trans('messages.Operator Name') }}</label>
        <div class="col-md-4">{{ $sale->operator_name }}</div>
        
        <label class="control-label col-md-2  col-lg-2">{{ trans('messages.Order Number') }}</label>
        <div class="col-md-4">{{ $sale->order_number }}</div>
    </div>
    
    <div class="row">
        <label class="control-label col-md-2  col-lg-2">{{ trans('messages.Driver') }}</label>
        <div class="col-md-4">{{ $sale->driver_name }}</div>
        
        <label class="control-label col-md-2  col-lg-2">{{ trans('messages.Supportive staff') }}</label>
        <div class="col-md-4">{{ $sale->staff_name }}</div>
    </div>
    
    <div class="row">
        <label class="control-label col-md-2  col-lg-2">{{ trans('messages.Special address') }}</label>
        <div class="col-md-4">{{ $sale->special_address }}</div>
        
        <label class="control-label col-md-2  col-lg-2">{{ trans('messages.Remarks') }}</label>
        <div class="col-md-4">{{ $sale->remarks }}</div>
    </div>
    
</section>

@stop