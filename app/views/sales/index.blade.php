@extends('layouts.default')

@section('content')
<section class="section-content col-sm-8 col-md-9 col-lg-10">
    <h1>{{ trans('messages.Sales') }}</h1>
    <hr>
    <div class="form-inline text-right spacer-bottom-md">
        <div class="form-group">
            {{ link_to('sales/create', trans('messages.Add'), array('class' => 'btn btn-info')) }}
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>{{ trans('messages.Product Name') }}</th>
                    <th>{{ trans('messages.Delivery date') }}</th>
                    <th>{{ trans('messages.Pickup date') }}</th>
                    <th>{{ trans('messages.OK-date') }}</th>
                    <th>{{ trans('messages.Time') }}</th>
                    <th>{{ trans('messages.Client') }}</th>
                    <th>{{ trans('messages.Contact Person') }}</th>
                    <th>{{ trans('messages.Action') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($sales as $sale)
                <tr>
                    <td>{{ $sale->product_name }}</td>
                    <td>{{ dutchDateFormat($sale->delivery_date) }}</td>
                    <td>{{ dutchDateFormat($sale->pickup_date) }}</td>
                    <td>{{ dutchDateFormat($sale->ok_date) }}</td>
                    <td>{{ $sale->time }}</td>
                    <td>{{ $sale->client_id }}</td>
                    <td>{{ $sale->contact_id }}</td>
                    <td align="center">
                        
                            <a href="{{ URL::to('sales', $sale->sale_products_id) }}" class="pull-left margin5">
                                <i class="fa fa-eye"></i>
                            </a>
                        &nbsp;
                            <a href="{{ URL::to('sales/print_for_driver', $sale->sale_products_id) }}" class="pull-left">
                                <i class="fa fa-print"></i>
                            </a>
                        <?php /*
                        {{ Form::open(array('url' => 'sales/' . $sale->id, 'class' => 'pull-left')) }}
                            {{ Form::hidden('_method', 'DELETE') }}
                            {{ Form::button('<i class="fa fa-trash-o"></i>', array('type' => 'submit', 'class' => 'delete-button', 'onclick' => "return confirm('".trans('messages.Are you sure you want to delete this?')."');"))}}
                        {{ Form::close() }}
                        */ ?>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    
    @include('elements.pagination', array('paginator'=> $sales))
    
</section>

@stop