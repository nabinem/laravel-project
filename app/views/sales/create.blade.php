@extends('layouts.default')

@section('content')

<section class="section-content col-sm-8 col-md-9 col-lg-10">
    <h1>{{ trans('messages.Sales') }}</h1>
    <hr>

    @include('elements.flash_message')
    
    @if(isset($sale))
        {{ Form::model($sale, array('route' => array('sales.update', $sale->id), 'method' => 'PUT','class' => 'form-horizontal sales-form','role'=>'form')) }}
    @else
        {{ Form::open(['url' => 'sales','class' => 'form-horizontal sales-form']) }}
    @endif
        
        @include('elements.validation')
        
        <div class="form-group">
            <label class="control-label col-md-4  col-lg-2">{{ trans('messages.Delivery date') }}</label>
            <div class="col-md-2">
                <div class='input-group date datepicker' id='delivery_date'>
                    {{ Form::text('delivery_date',null,array('class' => 'form-control', 'readonly'=>true, 'placeholder' => trans('messages.Delivery date'))) }}
                    <span class="input-group-addon"><span class="fa fa-calendar"></span> </span> 
                </div>
            </div>
            <label class="control-label col-md-4  col-lg-2">{{ trans('messages.Pickup date') }}</label>
            <div class="col-md-2">
                <div class='input-group date datepicker' id='pickup_date'>                    
                    {{ Form::text('pickup_date',null,array('class' => 'form-control', 'readonly'=>true, 'placeholder' => trans('messages.Pickup date'))) }}
                    <span class="input-group-addon"><span class="fa fa-calendar"></span> </span> 
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-4  col-lg-2">{{ trans('messages.OK-date') }}</label>
            <div class="col-md-2">
                <div class='input-group date datepicker' id='ok_date'>
                    {{ Form::text('ok_date',null,array('class' => 'form-control', 'readonly'=>true, 'placeholder' => trans('messages.OK-date'))) }}
                    <span class="input-group-addon"><span class="fa fa-calendar"></span> </span> 
                </div>
            </div>
            <label class="control-label col-md-4  col-lg-2">{{ trans('messages.Time') }}</label>
            <div class="col-md-2">
                <div class='input-group date timepicker' id='time'>
                    {{ Form::text('time',null,array('class' => 'form-control', 'readonly'=>true, 'placeholder' => trans('messages.Time'))) }}
                    <span class="input-group-addon"><span class="fa fa-clock-o"></span> </span> 
                </div>
            </div>
        </div>
        <hr>
        
        <div class="col-md-6">  
            <div class="form-group">
                <label class="control-label col-md-8  col-lg-4">{{ trans('messages.Select Client') }}</label>
                <div class="col-md-8">
                    @if(isset($sale))
                        {{ Form::select('client_id', $clients, null, array('class' => 'form-control sales_select_clients','placeholder' => trans('messages.Client'))) }}
                    @else
                        {{ Form::select('client_id', $clients, null, array('class' => 'form-control sales_select_clients','placeholder' => trans('messages.Client'))) }}
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-8  col-lg-4">{{ trans('messages.Select Contact Person') }}</label>
                <div class="col-md-8">
                    @if(isset($sale))
                        {{ Form::select('contact_id', $contacts, null, array('class' => 'form-control sales_client_contacts','placeholder' => trans('messages.Contact Person'))) }}
                    @else
                        {{ Form::select('contact_id', $contacts, null, array('class' => 'form-control sales_client_contacts','placeholder' => trans('messages.Contact Person'))) }}
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-8  col-lg-4">{{ trans('messages.Operator Name') }}</label>
                <div class="col-md-8">
                    {{ Form::text('operator_name',null,array('class' => 'form-control','placeholder' => trans('messages.Operator Name'))) }}
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-8  col-lg-4">{{ trans('messages.Order Number') }}</label>
                <div class="col-md-8">                
                    {{ Form::text('order_number',null,array('class' => 'form-control','placeholder' => trans('messages.Order Number'))) }}
                </div>
            </div>
        </div>
        
        <div class="col-md-6"> 
            <div id="addProductsCreateElement" style="display:none;">
                <div class="alert alert-error alert-danger">
                    <ul>
                        <li class="addProductsCreateerror"></li>

                    </ul>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-8  col-lg-4">
                    {{ trans('messages.Add products') }}                    
                    <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#add_products_modal" id="add_to_cart">
                        <i class="fa fa-plus"></i>
                    </button>
                </label>
                <div id="product-hidden">
                    
                </div>
                
                <div class="col-md-8">
                    <div class="selected_products"></div>
                </div>
            </div>
        </div>
        
        <div class="clearfix"></div>
        <hr>
        
        <div class="form-group">
            <label class="control-label col-md-4  col-lg-2">{{ trans('messages.OK-support required?') }}</label>
            <div class="col-md-4">
                <label class="checkbox-inline">
                    {{ Form::radio('support_required', 1, null,['class' => 'staff_support']) }} {{ trans('messages.Yes') }}
                </label>
                <label class="checkbox-inline">  
                    {{ Form::radio('support_required', 0, true,['class' => 'staff_support']) }} {{ trans('messages.No') }}
                </label>
            </div>
        </div>
        <div class="form-group support_staff_div">
            <label class="control-label col-md-4  col-lg-2">{{ trans('messages.Select supportive staff') }}</label>
            <div class="col-md-4">
                @if(isset($sale))
                    {{ Form::select('staff_id', $staffs, null, array('class' => 'form-control staff_id')) }}
                @else
                    {{ Form::select('staff_id', $staffs, null, array('class' => 'form-control staff_id')) }}
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-4  col-lg-2">{{ trans('messages.Select driver') }}</label>
            <div class="col-md-4">
                @if(isset($sale))
                    {{ Form::select('driver_id', $drivers, null, array('class' => 'form-control')) }}
                @else
                    {{ Form::select('driver_id', $drivers, null, array('class' => 'form-control')) }}
                @endif
            </div>
        </div>

        <hr>

        <div class="form-group">
            <label class="control-label col-md-4  col-lg-2">{{ trans('messages.Special address') }}</label>
            <div class="col-md-4">                
                {{ Form::textarea('special_address', null,array('class' => 'form-control', 'rows' => "7")) }}
            </div>

        </div>

        <div class="form-group">
            <label class="control-label col-md-4  col-lg-2">{{ trans('messages.Remarks') }}</label>
            <div class="col-md-4">                
                {{ Form::textarea('remarks', null,array('class' => 'form-control', 'rows' => "7")) }}
            </div>

        </div>

        <div class="form-group">
            <div class="col-sm-4 ol-sm-offset-4  col-lg-offset-2">
                {{ Form::button(trans('messages.Submit'),array('class' => 'btn btn-primary', 'type' => 'submit')) }}
                {{ link_to('sales', trans('messages.Cancel'), array('class' => 'btn btn-default')) }} 
            </div>
        </div>
            
    {{ Form::close() }}
        
    <hr>

</section>

@include('sales.add_products')
@stop