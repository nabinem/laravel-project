<div class="modal fade" id="add_products_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{ trans('messages.Add products') }}</h4>
            </div>
            <div class="modal-body">    
                
                <div id="addProductsElement" style="display:none;">
                    <div class="alert alert-error alert-danger">
                        <ul>
                            <li class="addProductserror"></li>

                        </ul>
                    </div>
                </div>
                
                
                <div class="add_product_modal">

                    <label class="control-label col-md-2  col-lg-2">{{ trans('messages.Product') }}</label>
                    <div class="col-md-5">
                        {{-- Form::select('products', $products, null, array('class' => 'form-control productsInput', 'multiple' => true)) --}}
                        <select name="products" class="form-control productsInput" multiple="1">
                            <?php /*
                            @foreach($products as $id => $product)
                                <option value="{{ $id }}" <?php echo ($productsDetail[$id]['available'] == 0)?'disabled':'' ?>>
                                    {{ $product }} ({{$productsDetail[$id]['available']}}/{{$productsDetail[$id]['total']}})
                                </option>
                            @endforeach
                            */ ?>
                        </select>
                    </div>
                    
                    <label class="control-label col-md-2  col-lg-2">{{ trans('messages.Quantity') }}</label>
                    <div class="col-md-3">
                        {{ Form::text('quantity', 1, array('class' => 'form-control productQuantity')) }}
                    </div>  
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id='add_product_button'>{{ trans('messages.Add') }}</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('messages.Cancel') }}</button>
            </div>
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
