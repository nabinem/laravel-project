@extends('layouts.default')

@section('content')

<section class="section-content col-sm-8 col-md-9 col-lg-10">
    <h1>{{ trans('messages.Agendas') }}</h1>
    <hr>
        
    @include('elements.flash_message')
    
    <div class="form-group row">
        <label class="control-label col-sm-4  col-lg-2">{{ trans('messages.Title') }}</label>
        <div class="col-sm-8">
            {{ $agenda->title }}
        </div>
    </div>
    <div class="form-group row">
        <label class="control-label col-sm-4  col-lg-2">{{ trans('messages.Agenda Date') }}</label>
        <div class="col-sm-8">
            {{ dutchDateFormat($agenda->agenda_date) }}
        </div>
    </div>        
    <div class="form-group row">
        <label class="control-label col-sm-4  col-lg-2">{{ trans('messages.Time') }}</label>
        <div class="col-sm-8">
            {{ $agenda->time }}
        </div>
    </div>
    
</section>

@stop