@extends('layouts.default')

@section('content')

<section class="section-content col-sm-8 col-md-9 col-lg-10">
    <h1>{{ trans('messages.Agendas') }}</h1>
    <hr>
        
    @include('elements.flash_message')
    
    @if(isset($agenda))
        {{ Form::model($agenda, array('route' => array('agendas.update', $agenda->id), 'method' => 'PUT','class' => 'form-horizontal agendas-form','role'=>'form')) }}
    @else
        {{ Form::open(['url' => 'agendas','class' => 'form-horizontal agendas-form']) }}
    @endif
        
        @include('elements.validation')
        <div class="form-group">
            <label class="control-label col-sm-4  col-lg-2">{{ trans('messages.Title') }}</label>
            <div class="col-sm-4">
                {{ Form::text('title',null,array('class' => 'form-control','placeholder' => trans('messages.Title'))) }}
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4  col-lg-2">{{ trans('messages.Agenda Date') }}</label>
            <div class="col-sm-4">
                <div class='input-group date datepicker future_date'>
                    {{ Form::text('agenda_date',null,array('class' => 'form-control', 'readonly'=>true, 'placeholder' => trans('messages.Agenda Date'))) }}
                    <span class="input-group-addon"><span class="fa fa-calendar"></span> </span> 
                </div>
            </div>
        </div>        
        <div class="form-group">
            <label class="control-label col-sm-4  col-lg-2">{{ trans('messages.Time') }}</label>
            <div class="col-sm-4">
                <div class='input-group date timepicker' id='time'>
                    {{ Form::text('time',null,array('class' => 'form-control', 'readonly'=>true, 'placeholder' => trans('messages.Time'))) }}
                    <span class="input-group-addon"><span class="fa fa-clock-o"></span> </span> 
                </div>
            </div>
        </div>
        
        <div class="form-group">
            
            <div class="col-sm-4 ol-sm-offset-4 col-lg-offset-2">
                {{ Form::button(trans('messages.Submit'),array('class' => 'btn btn-primary', 'type' => 'submit')) }}
            </div>
        </div>
        
    {{ Form::close() }}
    <hr>

    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>{{ trans('messages.Title') }}</th>
                    <th>{{ trans('messages.Date') }}</th>
                    <th>{{ trans('messages.Time') }}</th>
                    <th>{{ trans('messages.Action') }}</th>
                </tr>
            </thead>            
            <tbody>
                @foreach ($agendas as $agenda)  
                    <tr>
                        <td>{{ $agenda->title }}</td>
                        <td>{{ dutchDateFormat($agenda->agenda_date) }}</td>
                        <td>{{ hideSecondsFromTime($agenda->time) }}</td>
                        <td align="center">
                            
                            <a href="{{ URL::to('agendas/view', $agenda->id) }}" class="pull-left margin5">
                                <i class="fa fa-eye"></i>
                            </a>
                        &nbsp;
                            <a href="{{ URL::to('agendas', $agenda->id) }}" class="pull-left">
                                <i class="fa fa-pencil"></i>
                            </a>
                            &nbsp;
                            {{ Form::open(array('url' => 'agendas/' . $agenda->id, 'class' => 'pull-left')) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                {{ Form::button('<i class="fa fa-trash-o"></i>', array('type' => 'submit', 'class' => 'delete-button', 'onclick' => "return confirm('".trans('messages.Are you sure you want to delete this?')."');"))}}
                            {{ Form::close() }}

                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    @include('elements.pagination', array('paginator'=> $agendas))
    
</section>

@stop