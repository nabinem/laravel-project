@extends('layouts.default')

@section('content')

<section class="section-content col-sm-8 col-md-9 col-lg-10">
    <h1>{{ trans('messages.Users') }}</h1>
    <hr>
        
    @include('elements.flash_message')
    
        <div class="form-horizontal">
            <div class="form-group">
                <div class="col-md-2 col-sm-3"> 
                    {{ link_to("/users/create", trans('messages.Add'), array('class' => 'btn btn-primary btn-block spacer-bottom-sm')) }}
                </div>
                <div class="col-md-2 col-sm-3"> 
                    {{ link_to("/groups", trans('messages.Groups'), array('class' => 'btn btn-default btn-block spacer-bottom-sm')) }}
                </div>
                {{ Form::open(['url' => 'users','method' => 'get']) }}
                    <div class="col-md-4 col-md-offset-2 col-sm-3">                        
                        {{ Form::text('search',$search,array('class' => 'form-control spacer-bottom-s','placeholder' => trans('messages.Search'))) }}
                    </div>
                    <div class="col-md-2 col-sm-3">
                        {{ Form::button(trans('messages.Submit'),array('class' => 'btn btn-warning btn-block', 'type' => 'submit')) }}
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    
    
    @if (!$users->count())
        {{ trans('messages.No users found!') }}       
    @else
    
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>{{ trans('messages.Username') }}</th>
                        <th>{{ trans('messages.Email') }}</th> 
                        <th>{{ trans('messages.Action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)                
                    <tr>
                        <td>{{ $user->username }}</td>
                        <td>{{ $user->email }}</td>
                        <td width="180">
                            @if($user->id != 1) <?php /*basically 1 is for admin. this can't be edited or deleted*/?>                         
                            
                                <a href="{{ URL::to('users/'.$user->id.'/edit') }}" class="pull-left">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                &nbsp;
                                {{ Form::open(array('url' => 'users/' . $user->id, 'class' => 'pull-left')) }}
                                    {{ Form::hidden('_method', 'DELETE') }}
                                    {{ Form::button('<i class="fa fa-trash-o"></i>', array('type' => 'submit', 'class' => 'delete-button', 'onclick' => "return confirm('".trans('messages.Are you sure you want to delete this?')."');"))}}
                                {{ Form::close() }}

                            @endif
                            
                            @if(($user->id == 1) && (Confide::user()->id == 1)) <?php /*since only admin can access*/ ?>
                                {{ link_to("/permissions/manage_role", trans('messages.Manage Role Permission')) }}
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    @endif
        
    @include('elements.pagination', array('paginator'=> $users))

</section>

@stop



