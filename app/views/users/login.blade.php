@extends('layouts.login')

@section('content')

<div class="container">
    <div class="modal block login-modal">
        <h1 class="brand h2 wt-light text-center">
            <a href="#" class="text-inverse ">
                <span class="fa fa-cog"></span> Tornier BV
            </a>
        </h1>
        <div class="modal-dialog login-modal-dialog">
            <div class="modal-content">
                
                {{ Form::open(['class' => 'userlogin-form']) }}
                <div class="modal-header">

                    <h4 class="modal-title  wt-bold text-primary">Login</h4>
                </div>
                <div class="modal-body">
                    
                    @include('elements.validation')
                    
                    <div class="input-group spacer-bottom-sm">
                        <span class="input-group-addon bg-white">
                            <span class="fa fa-user text-primary"></span>                                
                        </span>
                        {{ Form::email('email','',array('class' => 'form-control','placeholder' => trans('messages.Email'))) }} 
                    </div>
                    <div class="input-group spacer-bottom">
                        <span class="input-group-addon bg-white"><span class="fa fa-lock text-primary"></span></span>

                        {{ Form::password('password',array('class' => 'form-control','placeholder' => trans('messages.Password'))) }}
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="hidden" name="remember" value="0">
                            <input type="checkbox" name="remember" value="1"> {{ trans('messages.Keep me logged in') }}
                        </label>
                    </div>  
                </div>
                <div class="modal-footer"> 
                    {{ Form::button(trans('messages.Login'),array('class' => 'btn btn-primary', 'type' => 'submit')) }} 
                </div>
                        
                {{ Form::close() }}
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>

@stop
