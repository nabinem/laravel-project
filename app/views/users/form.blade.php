@extends('layouts.default')

@section('content')


<section class="section-content col-sm-8 col-md-9 col-lg-10">
    @if(!isset($user))
        <h1>{{ trans('messages.Add Manager') }}</h1>
    @else
        <h1>{{ trans('messages.User') }}</h1>
    @endif
    <hr>

    @if(!isset($user))
        {{ Form::open(['url' => 'users','class' => 'form-horizontal users-form']) }}
    @else
        {{ Form::model($user, array('route' => array('users.update', $user->id), 'method' => 'PUT','class' => 'form-horizontal users-form','role'=>'form')) }}
    @endif


    @include('elements.validation')

    <div class="form-group">
        <label class="control-label col-md-4  col-lg-2">{{ trans('messages.Username') }}</label>
        <div class="col-md-4">
            {{ Form::text('username',null,array('class' => 'form-control','placeholder' => trans('messages.Username'))) }} 
        </div>
    </div>
    
    <div class="form-group">
        <label class="control-label col-md-4  col-lg-2">{{ trans('messages.Group') }}</label>
        <div class="col-md-4">
            @if(!isset($user))
                {{ Form::select('group', $groups, null, array('class' => 'form-control','placeholder' => trans('messages.Username'))) }} 
            @else
                {{ Form::select('group', $groups, $defaultGroupId, array('class' => 'form-control','placeholder' => trans('messages.Username'))) }} 
            @endif
            </div>
    </div>
    
    @if(!isset($user))
        <div class="form-group">
            <label class="control-label col-md-4  col-lg-2">{{ trans('messages.Email') }}</label>
            <div class="col-md-4">
                {{ Form::email('email','',array('class' => 'form-control','placeholder' => trans('messages.Email'))) }} 
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-4  col-lg-2">{{ trans('messages.Password') }}</label>
            <div class="col-md-4">
            {{ Form::password('password',array('id' => 'password', 'class' => 'form-control','placeholder' => trans('messages.Password'))) }}
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-4  col-lg-2">{{ trans('messages.Password Confirmation') }}</label>
            <div class="col-md-4">
                {{ Form::password('password_confirmation',array('class' => 'form-control','placeholder' => trans('messages.Password Confirmation'))) }}
            </div>
        </div>
    @endif
        
    <div class="form-group">
        <div class="col-sm-4 ol-sm-offset-4  col-lg-offset-2">        
            @if(!isset($user))
                {{ Form::button(trans('messages.Create New Account'),array('class' => 'btn btn-primary', 'type' => 'submit')) }}
            @else
                {{ Form::button(trans('messages.Save'),array('class' => 'btn btn-primary', 'type' => 'submit')) }}
            @endif
            {{ link_to('users', trans('messages.Cancel'), array('class' => 'btn btn-default')) }} 
        </div>
    </div>
    
    {{ Form::close() }}
</section>

@stop
