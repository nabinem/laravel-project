@extends('layouts.default')

@section('content')


<section class="section-content col-sm-8 col-md-9 col-lg-10">
    <h1>{{ trans('messages.User') }}</h1>
    <hr>
    
    @include('elements.validation')
    {{ Form::model( $user, ['route' => ['users.update', $user->id], 'method' => 'put','class' => 'form-horizontal', 'role' => 'form'] ) }}
          
        <div class="form-group">
            <label class="control-label col-md-4  col-lg-2">{{ trans('messages.Username') }}</label>
            <div class="col-md-4">
                {{ Form::text('username', $user->username,array('class' => 'form-control','placeholder' => trans('messages.Username'))) }} 
            </div>
        </div>

        <div class="form-group">

            <div class="col-sm-4 ol-sm-offset-4  col-lg-offset-2">
                {{ Form::button(trans('messages.Save'),array('class' => 'btn btn-primary', 'type' => 'submit')) }}
                {{ link_to('users', trans('messages.Cancel'), array('class' => 'btn btn-default')) }} 
            </div>
        </div>
        
    {{ Form::close() }}

</section>

@stop



