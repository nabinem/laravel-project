@extends('layouts.default')

@section('content')

<section class="section-content col-sm-8 col-md-9 col-lg-10">
    <h1>{{ trans('messages.Products') }}</h1>
    <hr>
        
    @include('elements.flash_message')
    
    @include('elements.validation')
    
    {{ Form::open(['url' => 'products','class' => 'form-inline text-right spacer-bottom-md products-form']) }}    
        
        <div class="form-group">
            
            <label class="spacer-right-xs">{{ trans('messages.Product Name') }}</label>
            {{ Form::text('name',null,array('class' => 'form-control spacer-right','placeholder' => trans('messages.Product Name'))) }}
            
            <label class="spacer-right-xs">{{ trans('messages.Amount') }}</label>
            {{ Form::text('amount',null,array('class' => 'form-control spacer-right','placeholder' => trans('messages.Amount'), 'size' => '6')) }}
            
            {{ Form::button(trans('messages.Add'),array('class' => 'btn btn-info', 'type' => 'submit')) }}
            
        </div>
    {{ Form::close() }}
    <hr>

    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>{{ trans('messages.Product') }}</th>
                    <th>{{ trans('messages.Stock') }}</th>
                    <th>{{ trans('messages.Available') }}</th>
                    <th>{{ trans('messages.First back') }}</th>
                    <th>{{ trans('messages.Action') }}</th>
                </tr>
            </thead>         
            <tbody>
                @foreach ($products as $id => $product)  
                    <tr>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->stocks->count() }}</td>
                        <td>{{ $product->availability }}</td>
                        <td><time class="spacer-right">{{ $product->firstback }}</time></td>
                        <td width="180">                            
                            <a href="{{ URL::to('stocks', $product->id) }}" class="btn btn-xs btn-warning">{{ trans('messages.View all') }}</a></td>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    @include('elements.pagination', array('paginator'=> $products))
    
</section>

@stop