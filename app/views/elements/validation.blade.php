@if (Session::get('error'))
<div class="alert alert-error alert-danger">
    @if (is_array(Session::get('error')))
    {{ head(Session::get('error')) }}
    @else
    {{{ Session::get('error') }}}
    @endif
</div>
@endif

@if (Session::get('notice'))
<div class="alert">{{ Session::get('notice') }}</div>
@endif

@if($errors->has())
    <div id="messageElement" >
        <div class="alert alert-error alert-danger">
            <ul role="alert" style="list-style: none;">
                <?php /*<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>*/?>

                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@else
    <div id="messageElement" style="display:none;">
        <div class="alert alert-error alert-danger">
            <ul id="messageBox" role="alert" style="list-style: none;">
        <?php /*<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>*/?>
    

            </ul>
        </div>
    </div>
@endif