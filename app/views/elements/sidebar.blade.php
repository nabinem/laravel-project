<?php
$leftMenus = array();
$leftMenus[] = array(
    'label' => Lang::get('messages.Dashboard'),
    'url_to' => '/',
    'first_segment' => '',
    'icon' => 'fa-dashboard'
);
$leftMenus[] = array(
    'label' => Lang::get('messages.Stock'),
    'url_to' => 'products',
    'first_segment' => array('products','stocks'),
    'icon' => 'fa-area-chart'
);
$leftMenus[] = array(
    'label' => Lang::get('messages.Sales'),
    'url_to' => 'sales',
    'first_segment' => 'sales',
    'icon' => 'fa-shopping-cart'
);
$leftMenus[] = array(
    'label' => Lang::get('messages.Planning'),
    'url_to' => 'plannings',
    'first_segment' => 'plannings',
    'icon' => 'fa-calendar-o'
);
$leftMenus[] = array(
    'label' => Lang::get('messages.Email Templates'),
    'url_to' => 'email_templates',
    'first_segment' => 'email_templates',
    'icon' => 'fa-at'
);
$leftMenus[] = array(
    'label' => Lang::get('messages.Clients'),
    'url_to' => 'clients',
    'first_segment' => 'clients',
    'icon' => 'fa-users'
);
$leftMenus[] = array(
    'label' => Lang::get('messages.Agendas'),
    'url_to' => 'agendas',
    'first_segment' => 'agendas',
    'icon' => 'fa-clipboard'
);
$leftMenus[] = array(
    'label' => Lang::get('messages.Archives'),
    'url_to' => 'archives',
    'first_segment' => 'archives',
    'icon' => 'fa-archive'
);
$leftMenus[] = array(
    'label' => Lang::get('messages.Users'),
    'url_to' => 'users',
    'first_segment' => array('users','groups'),
    'icon' => 'fa-user'
);
$leftMenus[] = array(
    'label' => Lang::get('messages.Staffs'),
    'url_to' => 'staffs',
    'first_segment' => 'staffs',
    'icon' => 'fa-wrench'
);
$leftMenus[] = array(
    'label' => Lang::get('messages.Drivers'),
    'url_to' => 'drivers',
    'first_segment' => 'drivers',
    'icon' => 'fa-codepen'
);
?>


<aside class="section-sidebar col-sm-4 col-md-3 col-lg-2">
    <div class="list-group">
        @foreach($leftMenus as $menu)
            <?php 
                if(is_array($menu['first_segment'])){   //if array            
                    if(in_array(Request::segment(1), $menu['first_segment']))
                        $class = 'active';
                    else
                        $class = '';  
                } else { //if string
                    if(Request::segment(1) == $menu['first_segment'])
                        $class = 'active';
                    else
                        $class = '';                
                }                      
            ?>
            <a href="{{ URL::to($menu['url_to']) }}" class="list-group-item {{ $class }}">
                <span class="fa {{ $menu['icon'] }} fa-fw"></span> 
                {{ $menu['label'] }}
            </a>
        @endforeach
    </div>
</aside>