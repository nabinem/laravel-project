<?php
$dashboardMenus = array();
$dashboardMenus[] = array(
    'label' => Lang::get('messages.Stock'),
    'url_to' => 'products',
    'icon' => 'fa-area-chart'
);
$dashboardMenus[] = array(
    'label' => Lang::get('messages.Sales'),
    'url_to' => 'sales',
    'icon' => 'fa-shopping-cart'
);
$dashboardMenus[] = array(
    'label' => Lang::get('messages.Planning'),
    'url_to' => 'plannings',
    'icon' => 'fa-calendar-o'
);
$dashboardMenus[] = array(
    'label' => Lang::get('messages.Email Templates'),
    'url_to' => 'email_templates',
    'icon' => 'fa-at'
);
$dashboardMenus[] = array(
    'label' => Lang::get('messages.Clients'),
    'url_to' => 'clients',
    'icon' => 'fa-users'
);
$dashboardMenus[] = array(
    'label' => Lang::get('messages.Agendas'),
    'url_to' => 'agendas',
    'icon' => 'fa-clipboard'
);
$dashboardMenus[] = array(
    'label' => Lang::get('messages.Archives'),
    'url_to' => 'archives',
    'icon' => 'fa-archive'
);
$dashboardMenus[] = array(
    'label' => Lang::get('messages.Users'),
    'url_to' => 'users',
    'icon' => 'fa-user'
);
$dashboardMenus[] = array(
    'label' => Lang::get('messages.Staffs'),
    'url_to' => 'staffs',
    'icon' => 'fa-wrench'
);
$dashboardMenus[] = array(
    'label' => Lang::get('messages.Drivers'),
    'url_to' => 'drivers',
    'icon' => 'fa-codepen'
);
?>


@foreach($dashboardMenus as $menu)
<div class="col-lg-2 col-md-4  col-xs-6 spacer-bottom-md">
    <a href="{{ URL::to($menu['url_to']) }}" class="btn btn-default btn-block">
        <div class="btn-lg-padd">
            <span class="fa {{ $menu['icon'] }} fa-fw fa-4x text-info"></span>
            <br><br> 
            <div>{{ $menu['label'] }}</div>            
        </div>
    </a>
</div>
@endforeach