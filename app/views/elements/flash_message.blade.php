@if(Session::has('success'))
    <div class="alert alert-box alert-success" role="alert" style="list-style: none;">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
        {{ Session::get('success') }}
    </div>
@endif

@if (Session::has('error'))
    <div class="alert alert-error alert-danger" role="alert" style="list-style: none;">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
        {{ Session::get('error') }}
    </div>
@endif