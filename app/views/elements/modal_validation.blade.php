@if($errors->has())
    <div id="messageElementModal" >
        <div class="alert alert-error alert-danger">
            <ul id="messageBoxModal" role="alert" style="list-style: none;">
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@else
    <div id="messageElementModal" style="display:none;">
        <div class="alert alert-error alert-danger">
            <ul id="messageBoxModal" role="alert" style="list-style: none;">
            </ul>
        </div>
    </div>
@endif