@if(isset($search) && $search)

    {{ $paginator->appends(array('search' => $search))->links() }}

@else

    <div class="container">
        <?php foreach ($paginator as $paginate): ?>
        <?php endforeach; ?>
    </div>

    <?php echo $paginator->links(); ?>

@endif