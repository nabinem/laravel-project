@section('scriptbottom')


{{ HTML::script('../ckeditor/ckeditor.js') }}

<script>
    var Tornier = {};
    //Tornier.webroot = "";
    jQuery(document).ready(function() {
        if (jQuery('#ckeditor_custom').length > 0) {
            CKEDITOR.replace('ckeditor_custom',
            {
                toolbar: 'Tornier',
                toolbar_Tornier: [
                    {name: 'source', items: ['Source']},
                    {name: 'links', items: ['Link', 'Unlink', 'Anchor']},
                    {name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                    {name: 'colors', items: ['TextColor', 'BGColor']},
                    {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},                    
                    {name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl']},
                    {name: 'insert', items: ['HorizontalRule', 'PageBreak', 'Iframe']},
                    {name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll', '-', 'SpellChecker', 'Scayt']},
                    '/',
                    {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                    
                ]
            });
        }
        
        if (jQuery('#ckeditor_lite').length > 0) {
            CKEDITOR.replace('ckeditor_lite',
            {
                toolbar: 'Tornier_Lite',
                toolbar_Tornier_Lite: [
                    {name: 'basicstyles', items: ['Bold', 'Italic']},
                    {name: 'paragraph', items: ['NumberedList', 'BulletedList']},
                ],
                height: '100px'
            });
        }
        
        if (jQuery('#ckeditor_email_template').length > 0) {
            CKEDITOR.replace('ckeditor_email_template',
            {
                toolbar: 'Tornier',
                toolbar_Tornier: [
                    {name: 'source', items: ['Source']},
                    {name: 'links', items: ['Link', 'Unlink', 'Anchor']},
                    {name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                    {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},                    
                    {name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl']},
                    {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                    
                ]
            });
        }

        initializeBasicCKEDITOR();
    });
    
    function initializeBasicCKEDITOR(){
        if (jQuery('#ckeditor_basic').length > 0) {
            CKEDITOR.replace('ckeditor_basic',
                    {
                        toolbar: 'Tornier_Basic',
                        toolbar_Tornier_Basic: [
                            {name: 'basicstyles', items: ['Bold', 'Italic']}

                ]
            });
        }
    }
</script>

@stop