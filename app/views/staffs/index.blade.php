@extends('layouts.default')

@section('content')

<section class="section-content col-sm-8 col-md-9 col-lg-10">
    <h1>{{ trans('messages.Staffs') }}</h1>
    <hr>
        
    @include('elements.flash_message')
    
    @if(isset($staff))
        {{ Form::model($staff, array('route' => array('staffs.update', $staff->id), 'method' => 'PUT','class' => 'form-horizontal staffs-form','role'=>'form')) }}
    @else
        {{ Form::open(['url' => 'staffs','class' => 'form-horizontal staffs-form']) }}
    @endif
        
        @include('elements.validation')
        <div class="form-group">
            <label class="control-label col-sm-4  col-lg-2">{{ trans('messages.Name') }}</label>
            <div class="col-sm-4">
                {{ Form::text('name',null,array('class' => 'form-control','placeholder' => trans('messages.Name'))) }}
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4  col-lg-2">{{ trans('messages.Phone Number') }}</label>
            <div class="col-sm-4">
                {{ Form::text('phone_number', null, array('class' => 'form-control','placeholder' => trans('messages.Phone Number'))) }}
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4  col-lg-2">{{ trans('messages.Email') }}</label>
            <div class="col-sm-4">
                {{ Form::text('email', null, array('class' => 'form-control','placeholder' => trans('messages.Email'))) }}
            </div>
        </div>
        <div class="form-group">
            
            <div class="col-sm-4 ol-sm-offset-4 col-lg-offset-2">
                {{ Form::button(trans('messages.Submit'),array('class' => 'btn btn-primary', 'type' => 'submit')) }}
            </div>
        </div>
        
    {{ Form::close() }}
    <hr>

    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>{{ trans('messages.Staff') }}</th>
                    <th>{{ trans('messages.Phone Number') }}</th>
                    <th>{{ trans('messages.Email') }}</th>
                    <th>{{ trans('messages.Action') }}</th>
                </tr>
            </thead>         
            <tbody>
                @foreach ($staffs as $staff)  
                    <tr>
                        <td>{{ $staff->name }}</td>
                        <td>{{ $staff->phone_number }}</td>
                        <td>{{ HTML::mailto($staff->email) }}</td>	
                        <td align="center">
                            
                            <a href="{{ URL::to('staffs', $staff->id) }}" class="pull-left">
                                <i class="fa fa-pencil"></i>
                            </a>
                            &nbsp;
                            {{ Form::open(array('url' => 'staffs/' . $staff->id, 'class' => 'pull-left')) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                {{ Form::button('<i class="fa fa-trash-o"></i>', array('type' => 'submit', 'class' => 'delete-button', 'onclick' => "return confirm('".trans('messages.Are you sure you want to delete this?')."');"))}}
                            {{ Form::close() }}

                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    @include('elements.pagination', array('paginator'=> $staffs))
    
</section>

@stop