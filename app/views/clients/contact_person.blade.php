@extends('layouts.default')

@section('content')

<section class="section-content col-sm-8 col-md-9 col-lg-10">
    <h1>{{ trans('messages.Contact Person') }}</h1>
    <hr>
        
    
    <div class="form-horizontal">
        <div class="form-group">
            <div class="control-label col-md-4  col-lg-2">{{ trans('messages.First Name') }}</div>
            <div class="control-label col-md-4">
                {{$contact->first_name}}
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-4  col-lg-2">{{ trans('messages.Last Name') }}</label>
            <div class="control-label col-md-4">
                {{$contact->last_name}}
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-4  col-lg-2">{{ trans('messages.Function') }}</label>
            <div class="control-label col-md-4">
                {{$contact->function}}
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-4  col-lg-2">{{ trans('messages.Phone Number') }}</label>
            <div class="control-label col-md-4">
                {{$contact->phone_number}}
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-4  col-lg-2">{{ trans('messages.Email') }}</label>
            <div class="control-label col-md-4">
                {{$contact->email}}
            </div>
        </div>
        <hr>
    </div>

</section>

@stop



