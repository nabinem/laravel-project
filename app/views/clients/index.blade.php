@extends('layouts.default')

@section('content')

<section class="section-content col-sm-8 col-md-9 col-lg-10">
    <h1>{{ trans('messages.Clients') }}</h1>
    <hr>
        
    @include('elements.flash_message')
    
        <div class="form-horizontal">
            <div class="form-group">
                <div class="col-sm-2"> 
                    {{ link_to("/clients/create", 'Add', array('class' => 'btn btn-primary btn-block spacer-bottom-sm')) }}
                </div>
                {{ Form::open(['url' => 'clients','method' => 'get']) }}
                    <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-1">                        
                        {{ Form::text('search',$search ,array('class' => 'form-control spacer-bottom-s','placeholder' => trans('messages.Search'))) }}
                    </div>
                    <div class="col-sm-3 col-md-2">
                        {{ Form::button(trans('messages.Submit'),array('class' => 'btn btn-warning btn-block', 'type' => 'submit')) }}
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    

    
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>{{ trans('messages.Hospital Name') }}</th>
                        <th>{{ trans('messages.Customer number') }}</th>
                        <th>{{ trans('messages.Contact Person') }}</th>
                        <th>{{ trans('messages.Action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($clients as $key => $client)                
                    <tr>
                        <td>{{ $client->hospital }}</td>
                        <td>{{ $client->customer_number }}</td>                        
                        <td>
                            @if(isset($client->first_contact))
                            {{ link_to('clients/contact_person/'.$client->first_contact_id.'' ,$client->first_contact) }}
                            @else
                                {{ 'N/A' }}
                            @endif
                        </td>
                        <td width="180">
                            
                            <a href="{{ URL::to('clients/'.$client->id.'/edit') }}" class="pull-left">
                                <i class="fa fa-pencil"></i>
                            </a>
                            &nbsp;
                            {{ Form::open(array('url' => 'clients/' . $client->id, 'class' => 'pull-left')) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                {{ Form::button('<i class="fa fa-trash-o"></i>', array('type' => 'submit', 'class' => 'delete-button', 'onclick' => "return confirm('".trans('messages.Are you sure you want to delete this?')."');"))}}
                            {{ Form::close() }}

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    @include('elements.pagination', array('paginator'=> $clients))

</section>

@stop



