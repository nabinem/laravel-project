<div class="contact-slot spacer-top-md col-sm-6">
    <div class="panel panel-info">
        <div class="panel-title panel-heading">
            <h4 class="panel-title">{{ trans('messages.Contact Person') }} 
                <button type="button" class="close" data-close='{{ $number }}' data-dismiss="alert">×</button>
            </h4>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <label class="col-sm-4 control-label">{{ trans('messages.First Name') }}</label>
                <div class="col-sm-8">
                    <?php 
                    if(isset($error) && $error)
                        $first_name = null;
                    elseif(isset($client))
                        $first_name = $client->contacts[$number-1]->first_name;
                    else
                        $first_name = null;
                    ?>
                    {{ Form::text('Contact['.$number.'][first_name]', $first_name, array('class' => 'form-control contact_first_name','placeholder' => trans('messages.First Name'))) }}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">{{ trans('messages.Last Name') }}</label>
                <div class="col-sm-8">
                    <?php 
                    if(isset($error) && $error)
                        $last_name = null;
                    elseif(isset($client))
                        $last_name = $client->contacts[$number-1]->last_name;
                    else
                        $last_name = null;
                    ?>
                    {{ Form::text('Contact['.$number.'][last_name]', $last_name, array('class' => 'form-control contact_last_name','placeholder' => trans('messages.Last Name'))) }}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">{{ trans('messages.Function') }}</label>
                <div class="col-sm-8">                    
                    <?php 
                    if(isset($error) && $error)
                        $function = null;
                    elseif(isset($client))
                        $function = $client->contacts[$number-1]->function;
                    else
                        $function = null;
                    ?>        
                    {{ Form::text('Contact['.$number.'][function]', $function, array('class' => 'form-control contact_function','placeholder' => trans('messages.Function'))) }}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">{{ trans('messages.Phone Number') }}</label>
                <div class="col-sm-8">                
                    <?php 
                    if(isset($error) && $error)
                        $phone_number = null;
                    elseif(isset($client))
                        $phone_number = $client->contacts[$number-1]->phone_number;
                    else
                        $phone_number = null;
                    ?>    
                    {{ Form::text('Contact['.$number.'][phone_number]', $phone_number, array('class' => 'form-control contact_phone_number','placeholder' => trans('messages.Phone Number'))) }}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">{{ trans('messages.Email') }}</label>
                <div class="col-sm-8">              
                    <?php 
                    if(isset($error) && $error)
                        $email = null;
                    elseif(isset($client))
                        $email = $client->contacts[$number-1]->email;
                    else
                        $email = null;
                    ?>    
                    {{ Form::text('Contact['.$number.'][email]', $email, array('class' => 'form-control contact_email','placeholder' => trans('messages.Email'))) }}
                </div>
            </div>
        </div>
    </div>
</div>  