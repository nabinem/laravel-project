@extends('layouts.default')

@section('content')

<section class="section-content col-sm-8 col-md-9 col-lg-10">
    <h1>{{ trans('messages.Client') }}</h1>
    <hr>

    
    @if(isset($client))
        {{ Form::model($client, array('route' => array('clients.update', $client->id), 'method' => 'PUT','class' => 'form-horizontal clients-form','role'=>'form')) }}
    @else
        {{ Form::open(['url' => 'clients','class' => 'form-horizontal clients-form']) }}
    @endif
        
        @include('elements.validation')
        
        <div class="form-group">
            <div class="col-sm-6">
                <label>{{ trans('messages.Hospital/clinic') }}</label>
                {{ Form::text('hospital',null,array('class' => 'form-control','placeholder' => trans('messages.Hospital Name'))) }}
            </div>
            <div class="col-sm-4">
                <label>{{ trans('messages.Country') }}</label>
                
                @if(isset($client))
                    {{ Form::select('country_id', $countries, null, array('class' => 'form-control','placeholder' => trans('messages.Country'))) }}
                @else
                    {{ Form::select('country_id', $countries, 36, array('class' => 'form-control','placeholder' => trans('messages.Country'))) }}
                @endif
                
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">{{ trans('messages.Address') }}</label>
            <div class="col-sm-4">
                {{ Form::text('address',null,array('class' => 'form-control','placeholder' => trans('messages.Address'))) }}
            </div>
            <div class="col-sm-4">
                {{ Form::text('house_number',null,array('class' => 'form-control','placeholder' => trans('messages.House Number'))) }}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">{{ trans('messages.Postcode') }}</label>
            <div class="col-sm-4">
                {{ Form::text('postcode',null,array('class' => 'form-control','placeholder' => trans('messages.Postcode'))) }}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">{{ trans('messages.Place') }}</label>
            <div class="col-sm-4">
                {{ Form::text('place',null,array('class' => 'form-control','placeholder' => trans('messages.Place'))) }}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">{{ trans('messages.Customer number') }}</label>
            <div class="col-sm-4">
                {{ Form::text('customer_number',null,array('class' => 'form-control','placeholder' => trans('messages.Customer number'))) }}
            </div>
        </div>
        
        <div class="row contact-slots-row">            
            @if(Input::old('Contact'))
                @foreach(Input::old('Contact') as $key => $oldContact)
                    @include('clients.contacts', array('number'=> $key, 'error' => true))
                @endforeach
            @elseif(isset($client))
                @for ($i = 1; $i <= $client->contacts->count(); $i++)
                    @include('clients.contacts', array('number'=> $i))
                @endfor
            @else
                @include('clients.contacts', array('number'=> 1))
            @endif
        </div>
        
        <div class="spacer-bottom-md">
            <a href="javascript:void(0);" class="btn btn-warning btn-sm add_contact">
                <span class="fa fa-plus-circle fa-fw"></span> 
                {{ trans('messages.Add another contact person') }}
            </a>
        </div>
    
        <div class="form-group">
            <div class="col-sm-6">
                <label class="control-label">{{ trans('messages.Remarks') }}</label>
                {{ Form::textarea('remarks', null,array('class' => 'form-control')) }}
            </div>
        </div>
        
        <div class="spacer-bottom-md">                   
            {{ Form::button(trans('messages.Submit'),array('class' => 'btn btn-primary', 'type' => 'submit')) }}
            {{ link_to('clients', trans('messages.Cancel'), array('class' => 'btn btn-default')) }} 
        </div>
    
    {{ Form::close() }}
    

</section>
@stop