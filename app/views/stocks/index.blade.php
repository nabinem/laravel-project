@extends('layouts.default')

@section('content')

<section class="section-content col-sm-8 col-md-9 col-lg-10">
    <h1>{{ trans('messages.Stocks') }}</h1>
    <hr>
        
    @include('elements.flash_message')
    
    @include('elements.validation')
    
    {{ Form::open(['url' => 'stocks','class' => 'form-inline text-right spacer-bottom-md stocks-form']) }}    
        {{ Form::hidden('product_id', $product->id) }}
        <div class="form-group">
            
            <label class="spacer-right-xs">{{ trans('messages.Add more :product', array('product' => $product->name)) }}</label>
            {{ Form::text('amount',null,array('class' => 'form-control spacer-right','placeholder' => trans('messages.Amount'), 'size' => '6')) }}
            
            {{ Form::button(trans('messages.Add'),array('class' => 'btn btn-info', 'type' => 'submit')) }}
            
        </div>
    
    {{ Form::close() }}

    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>{{ trans('messages.Product Name') }}</th>  
                    <th>{{ trans('messages.Retrival Date') }}</th> 
                    <th>{{ trans('messages.Serial Number') }}</th>   
                    <th>{{ trans('messages.Action') }}</th>   
                </tr>
            </thead>         
            <tbody>       
                @foreach ($stocks as $id => $stock)  
                    <tr>
                        <td>{{ $stock->product->name }}
                            @if($stockDetail[$id])
                                [{{ $stockDetail[$id]->hospital}}]
                            @endif
                        </td>
                        <td>
                            @if($stockDetail[$id])
                            
                            <a href="{{ URL::to('sales', $stockDetail[$id]->id) }}" class="pull-left margin5">
                                    {{ dutchDateFormat($stockDetail[$id]->pickup_date) }}
                                </a>
                            @else
                                -
                            @endif
                        </td>
                        <td>
                            <a href="javascript:void(0);" data-type="text" data-pk="{{ $stock->id }}" class="serial-number">{{ $stock->serial_number }}</a>
                        </td>
                        <td>
                            @if($stockDetail[$id])
                                {{ Form::button('<i class="fa fa-trash-o"></i>', array('class' => 'delete-button', 'onclick' => "alert('".trans('messages.You cannot delete purchased product.')."');"))}}
                            @else                            
                                {{ Form::open(array('url' => 'stocks/' . $stock->id, 'class' => 'pull-left')) }}
                                    {{ Form::hidden('_method', 'DELETE') }}
                                    {{ Form::button('<i class="fa fa-trash-o"></i>', array('type' => 'submit', 'class' => 'delete-button', 'onclick' => "return confirm('".trans('messages.Are you sure you want to delete this?')."');"))}}
                                {{ Form::close() }}  
                            @endif                            
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    @include('elements.pagination', array('paginator'=> $stocks))
    
</section>

@stop

@section('scriptbottom')
    {{ HTML::style('css/inline/bootstrap-editable.css') }}
    {{ HTML::script('js/inline/bootstrap-editable.min.js') }}
    {{ HTML::script('js/inline/inline-edit.js') }}
@stop