<?php

return array(
    'Users' => 'Gebruikers',
    'Username' => 'Gebruikersnaam',   
    'Email' => 'Email',   
    'Action' => 'Actie',
    'User' => ' Gebruiker ',   
    'Save' => 'Opslaan',    
    'Cancel' => 'Annuleren',
    'Password' => 'Wachtwoord',
    'Password Confirmation' => 'Bevestig wachtwoord',
    'Create New Account' => 'Nieuw account',
    'Keep me logged in' => 'Ingelogd blijven',
    'Register' => 'Registreer',
    'Login' => 'Inloggen',
    'No users found!' => 'Geen gebruikers gevonden!',
    'Record edited successfully.' => 'Aanpassing succesvol opgeslagen.',
    'Record deleted successfully.' => 'Succesvol verwijderd.',
    'Are you sure you want to delete this?' => 'Weet u zeker dat u dit wilt verwijderen?',
    
    
    'Permissions' => 'Toestemming',
    'No any actions to synchronize!' => 'Geen acties te synchroniseren!',
    'Name' => 'Naam',
    'Display Name' => 'Weergavenaam',
    
    'Click to synchronize all permissions.' => 'Klik om alle  toestemmingen te synchroniseren.',
    'Manage Role Permission' => 'Beheer permissies',
    
    'Roles' => 'Rollen',
    'Grant All' => 'Alles toekennen',
    'Delete All' => 'Alles verwijderen',
    
    'Clients' => 'Relaties',
    'No clients found!' => 'Geen klanten gevonden!',
    'Client' => 'Klant',    
    'Hospital/clinic' => 'Ziekenhuis /Kliniek', 
    'Hospital Name' => 'Ziekenhuisnaam', 
    'Country' => 'Land',   
    'Address' => 'Adres',   
    'House Number' => 'Huisnummer', 
    'Postcode' => 'Postcode', 
    'Place' => 'Plaats', 
    'Customer number' => 'Debiteurennummer',
    
    'Add another contact person' => 'Contactpersoon toevoegen',
    'Remarks' => 'Opmerkingen',   
    'Create New Client' => 'Nieuwe klant',
    
    
    'Contact Person' => 'Contactpersoon', 
    'Phone' => 'Telefoon',  
    'Submit' => 'Verstuur', 
    'Search' => 'Zoeken',
    
    'First Name' => 'Voornaam',  
    'Last Name' => 'Achternaam',
    'Function' => 'Functie', 
    'Phone Number' => 'Telefoonnummer',  
    
    'Drivers' => 'Chauffeurs',
    'Driver' => 'Chauffeur',
    
    'Staffs' => 'OK-ondersteuners',
    'Staff' => 'OK-ondersteuner',
    
    'Dashboard' => 'Dashboard',
    'Stock' => 'Voorraad', 
    'Sales' => 'Verkoop',
    'Planning' => 'Planning',
    'Contacts' => 'Relaties',
    'Archives' => 'Archief',
    
    'Products' => 'Producten', 
    'Product' => 'Product', 
    'Available' => 'Beschikbaar',  
    'First back' => 'Eerste terug', 


    'Product Name' => 'Product naam', 
    'Amount' => 'Hoeveelheid',
    'Add' => 'Toevoegen',
    
    'No' => 'Nee',
    'Yes' => 'Ja',
    
    'Serial Number' => 'Serie nummer',
    'Client Name' => 'Klantnaam',
    
    'Stocks' => 'Effecten',
    'Retrival Date' => 'Ophaaldatum',
    'Add more :product' => 'Meer :product toevoegen',
    
    'Delivery date' => 'Bezorg datum',
    'Pickup date' => 'Ophaaldatum',
    'OK-date' => 'OK datum',
    'Time' => 'Tijd',
    'Add products' => 'Producten toevoegen', 
    'Select Client' => 'Selecteer klant',
    'Select Contact Person' => 'Selecteer contactpersoon',
    'Operator Name' => 'Naam operateur',  
    'Order Number' => 'Ordernummer', 
    'OK-support required?' => 'OK-ondersteuning nodig?', 
    'Select supportive staff' => 'Selecteer ondersteuner', 
    'Select driver' => 'Selecteer chauffeur',
    'Special address' => 'Bijzonderheden adres', 
    
    'Email Templates' => 'Email Sjablonen',
    'Email Template' => 'Email Sjabloon',
    'Code' => 'Code',  
    'Subject' => 'Onderwerp',  
    'Description' => 'Omschrijving', 
    
    'Plannings' => 'Planning', 
    'Print' => 'Print',  
    'Agenda' => 'Agenda', 
    'Archive' => 'Archief',

    'Agendas' => 'Agendas',  
    'Title' => 'Titel',
    'Agenda Date' => 'Agenda datum',


    'Agenda added successfully.' => 'Item succesvol toegevoegd',
    'Add agenda' => 'Toevoegen agenda',

    'Select at least one plan.' => 'Selecteer plan',

    'Add Manager' => 'Manager toevoegen',

    'Email should be unique.' => 'Email moet uniek zijn.',

    'Order Detail' => 'Order gegevens',
    'Product Serial' => 'Serie code', 
    'Supportive staff' => 'Ondersteunend personeel', 

    'No archive found!' => 'Geen archief gevonden', 

    'Date' => 'Datum',
    'Detail' => 'Details',

    'Attachment' => 'Bijlage',  
    'Quantity' => 'Hoeveelheid', 
    
    'View all' => 'Bekijk alles',
    'Are you sure you want to delete this?' => 'Weet u zeker dat u dit wilt verwijderen?',
    'Logout'=> 'Uitloggen',
    'Delivery' => 'Bezorgen',
    'Pickup' => 'Ophalen',
    
    'Product unavailable for following date or quantity.' => 'Product unavailable for following date or quantity.',
    'You cannot delete purchased product.'=> 'U kunt gekochte product niet verwijderen.', 
    
    'You cannot delete this group.' =>'U kunt deze groep niet verwijderen.',
    'Group' => 'Groep',
    'Groups' => 'Groepen',
    'Record cannot be deleted.' => 'Record kan niet worden verwijderd.',
        
    'Add Permission' => 'Permissies toevoegen',
    'Delete Permission' => 'Permissies verwijderen',
);
