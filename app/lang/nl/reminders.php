<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" => "Wachtwoorden moeten minimaal 6 karakters lang zijn en overeenkomen met de bevestiging.",

	"user" => "Wij kunnen geen gebruiker vinden met di email adres.",

	"token" => "Dit wachtwoord token is ongeldig.",

	"sent" => "Wachtwoord herinnering verstuurd!",

	"reset" => "Wachtwoord is gerest!",

);
