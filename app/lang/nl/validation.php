<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| as the size rules. Feel free to tweak each of these messages here.
	|
	*/

	"accepted"             => "De :eigenschap moet worden geaccepteerd.",               
	"active_url"           => "De :eigenschap is geen geldige URL.",             
	"after"                => "De :eigenschap moet een dag later zijn.",     
	"alpha"                => "De :eigenschap mag alleen letters bevatten.",       
	"alpha_dash"           => "De :eigenschap mag alleen  letters ,nummers of streepjes bevatten.",
	"alpha_num"            => "De :eigenschap mag alleen letters en nummers bevatten.",
	"array"                => "De :eigenschap bevat een ongeldige reeks.",             
	"before"               => "De :eigenschap  moet een dag eerder zijn.",   
	"between"              => array(                                           
		"numeric" => "De :eigenschap moet tussen :min en :max zijn.",        
		"file"    => "De :eigenschap moet moet tussen :min en max: kilogrammen zijn.",  
		"string"  => "De :eigenschap moet tussen :min en :max karakters zijn.",   
		"array"   => "De :eigenschap moet tussen :min en :max artikelen zijn.",
	),
	"boolean"              => "De :eigenschap veld moet goed of fout zijn.",   
	"confirmed"            => "De :eigenschap komt niet overeen.",     
	"date"                 => "De :eigenschap is geen geldige datum.",            
	"date_format"          => "De :eigenschap komt niet overeen met het format.",  
	"different"            => "De :eigenschap en :anderen moet anders zijn.",      
	"digits"               => "De :eigenschap moet cijfers zijn.",            
	"digits_between"       => "De :eigenschap moet nummers bevatten :min en :max nummers.", 
	"email"                => "De :eigenschap moet een geldig email adres zijn.",     
	"exists"               => "De geselecteerde :eigenschap is niet geldig.",            
	"image"                => "De :eigenschap moet een afbeelding zijn.",           
	"in"                   => "De :geselecteerde :eigenschap is niet geldig.",       
	"integer"              => "De :eigenschap moet integer zijn.",      
	"ip"                   => "De :eigenschap moet een geldig IP adres zijn.",
	"max"                  => array(
		"numeric" => "De :eigenschap mag niet groter zijn dan :max.",          
		"file"    => "De :eigenschap mag niet groter zijn dan  :max kilogrammen.",   
		"string"  => "De :eigenschap  mag niet groter zijn dan :max karakters.",   
		"array"   => "De :eigenschap  mag niet groter zijn dan :max artikelen.",  
	),
	"mimes"                => "De :eigenschap moet een bestand of type: :waarden.",  
	"min"                  => array(
		"numeric" => "De :eigenschap moet minimaal :min .",             
		"file"    => "De :eigenschap moet minimaal :min kilogram.",   
		"string"  => "De :eigenschap moet minimaal :min karakters.",  
		"array"   => "The :attribute must have at least :min items.",        
	),
	"not_in"               => "De geselecteerde :eigenschap is niet geldig.",                    
	"numeric"              => "De :eigenschap moet een nummer zijn.",                      
	"regex"                => "De :eigenschap  formaat is niet geldig.",                      
	"required"             => "De :eigenschap veld is verplicht .",                        
	"required_if"          => "De :eigenschap veld is verplicht wanneer  : ander is  :waarden.",    
	"required_with"        => "De :eigenschap veld is verplicht wanneer  :waarden is aanwezig.",
	"required_with_all"    => "De :eigenschap veld is verplicht wanneer  :waarden is aanwezig.", 
	"required_without"     => "De :eigenschap veld is verplicht wanneer  :waarden niet aanwezig is.",
	"required_without_all" => "De :eigenschap veld is verplicht wanneer  gaan van :waarden is aanwezig.",  
	"same"                 => "De :eigenschap en: ander moet gelijk.",                       
	"size"                 => array(
		"numeric" => "De :eigenschap moet zijn :maat.",                         
		"file"    => "De :eigenschap moet zijn :maat kilogram.",               
		"string"  => "De :eigenschap moet zijn :maat karakters .",              
		"array"   => "De :eigenschap moet zijn :maat artikelen.",              
	),
	"unique"               => "De :eigenschap is al bezetn.",          
	"url"                  => "De :eigenschap formaat is niet geldig.",                 
	"timezone"             => "De :eigenschap moet een geldig tijdzone e.",  

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => array(
		'attribute-name' => array(
			'rule-name' => 'custom-message',
		),
	),

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => array(),

);
