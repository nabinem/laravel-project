<?php

return array(
    'Users' => 'Users',
    'Username' => 'Username',
    'Email' => 'Email',
    'Action' => 'Action',
    'User' => 'User',
    'Save' => 'Save',
    'Cancel' => 'Cancel',
    'Password' => 'Password',
    'Password Confirmation' => 'Password Confirmation',
    'Create New Account' => 'Create New Account',
    'Keep me logged in' => 'Keep me logged in',
    'Register' => 'Register',
    'Login' => 'Login',
    'No users found!' => 'No users found!',
    'Record edited successfully.' => 'Record edited successfully.',
    'Record deleted successfully.' => 'Record deleted successfully.',
    'Are you sure you want to delete this?' => 'Are you sure you want to delete this?',
    
    
    'Permissions' => 'Permissions',
    'No any actions to synchronize!' => 'No any actions to synchronize!',
    'Name' => 'Name',
    'Display Name' => 'Display Name',
    
    'Click to synchronize all permissions.' => 'Click to synchronize all permissions.',
    'Manage Role Permission' => 'Manage Role Permission',
    
    'Roles' => 'Roles',
    'Grant All' => 'Grant All',
    'Delete All' => 'Delete All',
    
    'Clients' => 'Clients',
    'No clients found!' => 'No clients found!',
    'Client' => 'Client',
    'Hospital/clinic' => 'Hospital/clinic',
    'Hospital Name' => 'Hospital Name',
    'Country' => 'Country',
    'Address' => 'Address',
    'House Number' => 'House Number',
    'Postcode' => 'Postcode',
    'Place' => 'Place',
    'Customer number' => 'Customer number',
    
    'Add another contact person' => 'Add another contact person',
    'Remarks' => 'Remarks',
    'Create New Client' => 'Create New Client',
    
    'Contact Person' => 'Contact Person',
    'Phone' => 'Phone',
    'Submit' => 'Submit',
    'Search' => 'Search',
    
    'First Name' => 'First Name',
    'Last Name' => 'Last Name',
    'Function' => 'Function',
    'Phone Number' => 'Phone Number',
    
    'Drivers' => 'Drivers',
    'Driver' => 'Driver',
    
    'Staffs' => 'Staffs',
    'Staff' => 'Staff',
    
    'Dashboard' => 'Dashboard',
    'Stock' => 'Stock',
    'Sales' => 'Sales',
    'Planning' => 'Planning',
    'Contacts' => 'Contacts',
    'Archives' => 'Archives',
    
    'Products' => 'Products',
    'Product' => 'Product',
    'Available' => 'Available',
    'First back' => 'First back',
    
    'Product Name' => 'Product Name',
    'Amount' => 'Amount',
    'Add' => 'Add',
    
    'No' => 'No',
    'Yes' => 'Yes',
    
    'Serial Number' => 'Serial Number',
    'Client Name' => 'Client Name',
    
    'Stocks' => 'Stocks',
    'Retrival Date' => 'Retrival Date',
    'Add more :product' => 'Add more :product',
    
    'Delivery date' => 'Delivery date',
    'Pickup date' => 'Pickup date',
    'OK-date' => 'OK-date',
    'Time' => 'Time',
    'Add products' => 'Add products',
    'Select Client' => 'Select Client',
    'Select Contact Person' => 'Select Contact Person',
    'Operator Name' => 'Operator Name',
    'Order Number' => 'Order Number',
    'OK-support required?' => 'OK-support required?',
    'Select supportive staff' => 'Select supportive staff',
    'Select driver' => 'Select driver',
    'Special address' => 'Special address',
    
    'Email Templates' => 'Email Templates',
    'Email Template' => 'Email Template',
    'Code' => 'Code',
    'Subject' => 'Subject',
    'Description' => 'Description',
    
    'Plannings' => 'Plannings',
    'Print' => 'Print',
    'Agenda' => 'Agenda',
    'Archive' => 'Archive',
    
    'Agendas' => 'Agendas',
    'Title' => 'Title',
    'Agenda Date' => 'Agenda Date',
    
    'Agenda added successfully.' => 'Agenda added successfully.',
    'Add agenda' => 'Add agenda',
    
    'Select at least one plan.' => 'Select at least one plan.',
    
    'Add Manager' => 'Add Manager',
    
    'Email should be unique.' => 'Email should be unique.',
    
    'Order Detail' => 'Order Detail',
    'Product Serial' => 'Product Serial',
    'Supportive staff' => 'Supportive staff',
    
    'No archive found!' => 'No archive found!',
    
    'Date' => 'Date',
    'Detail' => 'Detail',
    
    'Attachment' => 'Attachment',
    'Quantity' => 'Quantity',
    
    'View all' => 'View all',
    'Are you sure you want to delete this?' => 'Are you sure you want to delete this?',
    'Logout'=> 'Logout',
    'Delivery' => 'Delivery',
    'Pickup' => 'Pickup',
    
    'Product unavailable for following date or quantity.' => 'Product unavailable for following date or quantity.',
    'You cannot delete purchased product.'=> 'You cannot delete purchased product.',    
    
    'You cannot delete this group.' =>'You cannot delete this group.',
    'Group' => 'Group',
    'Groups' => 'Groups',
    'Record cannot be deleted.' => 'Record cannot be deleted.',
        
    'Add Permission' => 'Add Permission',
    'Delete Permission' => 'Delete Permission',
);
