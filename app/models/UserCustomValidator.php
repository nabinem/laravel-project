<?php

use Zizaco\Confide\ConfideUserInterface;

class UserCustomValidator extends Zizaco\Confide\UserValidator {

    public $rules = [
        'create' => [
            'username' => 'required|alpha_dash',
            'email'    => 'required|email',
            'password' => 'required|min:4',
        ],
        'update' => [
            'username' => 'required|alpha_dash',
            'email'    => 'required|email',
            'password' => 'required|min:4',
        ]
    ];

}

