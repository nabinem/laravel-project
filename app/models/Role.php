<?php

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    
    public function assign_manager_role($user_id, $role_id){      
        
        $user = User::where('id','=',$user_id)->first();
        
        /* OR the eloquent's original: */
        $user->roles()->attach( $role_id ); // id only
    }

}