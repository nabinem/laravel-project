<?php

class Product extends \Eloquent {
    
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';

    // Add your validation rules here
    public static $rules = [
            'name' => 'required|unique:products,name',
            'amount' => 'required|numeric'
    ];

    // Don't forget to fill this array
    protected $fillable = ['name','amount'];   
    
    public function isValid($id = null) {
            
        $rules = static::$rules;
        
        $validation = Validator::make($this->attributes, $rules);

        if ($validation->passes())
            return true;
        
        $this->errors = $validation->messages();

        return false;
    }
        
    
    /**
     * Relationship
     */
    // Product__hasMany__ Stocks
    public function stocks() //plural for has many just a convention
    {
        return $this->hasMany('Stock');
    }
        
    // Product__hasMany__ SaleProducts
    public function saleproducts() //plural for has many just a convention
    {
        return $this->hasMany('SaleProduct');
    }  
    
    
    public function getAvailabilityforProductTable($product_id, $delivery_date = null, $pickup_date = null){
        
        $saleProductModel = new SaleProduct;
        $today = date('Y-m-d');
        $currentSales = $saleProductModel
                ->leftJoin('sales', 'sales.id', '=', 'sale_products.sale_id')
                ->where('product_id', '=', $product_id)
                ->where(function($query) use ($delivery_date, $pickup_date, $today) {
                    if(is_null($delivery_date) && is_null($pickup_date)){
                        $query->where('sales.delivery_date', '<=', $today)
                            ->where('sales.pickup_date', '>=', $today);
                    } else {  
                        $query->where(function($query) use ($delivery_date){
                            $query->where('sales.delivery_date', '<=', $delivery_date)
                                    ->where('sales.pickup_date', '>=', $delivery_date);
                        })
                        ->orWhere(function($query) use ($pickup_date){
                            $query->where('sales.delivery_date', '<=', $pickup_date)
                                    ->where('sales.pickup_date', '>=', $pickup_date);
                        });                    
                    }
                })
                ->lists('stock_id');
                
        if(empty($currentSales)){
            $currentSales = array(0);
        }
        $stockModel = new Stock;
        $available_count = $stockModel
                ->where('product_id', '=', $product_id)
                ->whereNotIn('id', $currentSales)
                ->count();
        
        return $available_count;
    }
    
    public function getProductFirstBack($product_id){
        
        $saleProductModel = new SaleProduct;
        $today = date('Y-m-d');
        $first_return_date = $saleProductModel
                ->leftJoin('sales', 'sales.id', '=', 'sale_products.sale_id')
                ->where('product_id', '=', $product_id)
                ->where('pickup_date', '>=', $today)
                ->orderBy('pickup_date', 'asc')
                ->pluck('pickup_date');
        return $first_return_date;
    }

}