<?php

class Planning extends \Eloquent {
    
    
    /**
     * Function to get minimum and maximum date to display the sort
     * @param type $deliveries
     * @param type $pickups
     * @param type $staffs
     * @param type $agendas
     * @return type
     */
    public function getMinAndMaxDate($deliveries,$pickups,$staffs,$agendas){
        $mindate = $maxdate = date('Y-m-d');
        
        if(!$deliveries->isEmpty()){
            $mindate = min($mindate, $deliveries->min('delivery_date'));
            $maxdate = max($maxdate, $deliveries->max('delivery_date'));
        }        
        
        if(!$pickups->isEmpty()){            
            $mindate = min($mindate, $pickups->min('pickup_date'));
            $maxdate = max($maxdate, $pickups->max('pickup_date'));
        }        
        
        if(!$staffs->isEmpty()){
            $mindate = min($mindate, $staffs->min('ok_date'));
            $maxdate = max($maxdate, $staffs->max('ok_date'));            
        }        
        
        if(!$agendas->isEmpty()){
            $mindate = min($mindate, $agendas->min('agenda_date'));
            $maxdate = max($maxdate, $agendas->max('agenda_date'));            
        }
        
        $date = array('min' => $mindate, 'max' => $maxdate);  
        
        return $date;
                
    }

}