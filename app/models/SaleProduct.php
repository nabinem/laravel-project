<?php

class SaleProduct extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
        //'delivery_date','pickup_date','ok_date', 'time', 'client_id', 'contact_id', 'operator_name', 'order_number', 'support_required', 'staff_id', 'driver_id', 'special_address'
	];

	// Don't forget to fill this array
    protected $fillable = [];   
    
    
    /**
     * Relationship
     */
    // SaleProducts __belongs_to__ Sale
    public function sale() {
        return $this->belongsTo('Sale');
    }
    
    /**
     * Relationship
     */
    // SaleProducts __belongs_to__ Product
    public function product() {
        return $this->belongsTo('Product');
    }
    
    /**
     * Relationship
     */
    // SaleProducts __belongs_to__ Stock
    public function stock() {
        return $this->belongsTo('Stock');
    }

}