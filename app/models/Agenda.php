<?php

class Agenda extends \Eloquent {
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'agendas';

    /**
     * Validataion
     */
    public static $rules = array(
        'title' => 'required',
        'agenda_date' => 'required',
        'time' => 'required',
    );
    	
    protected $fillable = ['title', 'agenda_date', 'time'];
    
    public function getAgendaArchive($search){
        $agendaArchiveData = $this->where('agenda_archived', '=', 1)
                ->orderBy('agenda_date', 'desc')
                ->where('title', 'like', '%'.$search.'%')
                ->paginate(LIMIT);
        return $agendaArchiveData;
    }

}