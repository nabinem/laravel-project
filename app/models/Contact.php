<?php

class Contact extends \Eloquent {
    
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'contacts';

    /**
     * Validataion
     */
    public static $rules = array(
        'first_name' => 'required',
        'last_name' => 'required',
        'function' => 'required',
        'phone_number' => 'required',
        'email' => 'required|email'//|unique:clients',
    );
    	
    protected $guarded = ['client_id'];
    
    public function isValid() {

        $validation = Validator::make($this->attributes, static::$rules);

        if ($validation->passes())
            return true;

        $this->errors = $validation->messages();
        
        return false;
    }
    
    public function checkContactEmailValidity($contacts){ 
        $counter = 0;
        $allContactEmails = array();
        foreach($contacts as $contact){
            $allContactEmails[$counter] = $contact['email'];     
            $counter++;
        }
        
        $mergeContactEmails = array_unique($allContactEmails);
        
        if(count($allContactEmails) == count($mergeContactEmails)){
            return false;
        }else{
            return true;
        }
        
    }
    
    /**
     * Relationship
     */
    // Contacts __belongs_to__ Client
    public function client() {
        return $this->belongsTo('Client');
    }
    
}