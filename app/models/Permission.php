<?php

use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission {
    
    public function getActionsToSync(){
        $allMethods = $this->getMethodNames();
        $dbPermissions = DB::table('permissions')
                        ->select('permissions.id','permissions.name','permissions.display_name')
                        ->get();
        $dbPermissionsCopy = $dbPermissions;
        
        for($i=0;$i<count($dbPermissions); $i++){
            foreach($allMethods as $permissionKey => $permission){                       
                if($permission['controller'] == $dbPermissions[$i]->display_name){
                    
                    foreach($permission['action'] as $actionKey => $action){
                        if($action == $dbPermissions[$i]->name){
                            unset($allMethods[$permissionKey]['action'][$actionKey]);
                            unset($dbPermissionsCopy[$i]);
                        }
                    }
                    
                }
            }
        }
        
        foreach($allMethods as $key => $methods){
            if(empty($methods['action'])){
                unset($allMethods[$key]);
            }
        }
        
        return array('add' => $allMethods, 'delete' => $dbPermissionsCopy);
        
    }

    public function getMethodNames() {
        $allController = $this->getControllers();
        $controller_methods = array();
        $count = 0;
        foreach($allController as $controller){
            $action = $this->getMethodForController($controller);
            if(!empty($action)){
                $controller_methods[$count]['controller'] = $controller;            
                $controller_methods[$count]['action'] = $action;
                $count++;
            }
        }
        
        return $controller_methods;
    }
    
    public function getControllers(){
        $filterController = array(
            'PermissionsController',
            'BaseController'
        );
        $controllerDirectory = app_path().'/controllers';
        $files = File::allFiles($controllerDirectory);
        $controller_names = array();
        $count = 0;
        foreach ($files as $file) {            
            $file = $file->getRelativePathName();
            if(substr($file, -14) == 'Controller.php'){
                $controller_names[$count] = substr($file, 0, -4); 
                // '0 digit from front and '.php' is 4 from back
                $count++;
            }
        }
        
        $controllers = array_diff($controller_names, $filterController);
        
        return $controllers;
    }
    
    public function getMethodForController($controller){  
        $filterMethod = array(
            '__construct',
            'beforeFilter',
            'afterFilter',
            'forgetBeforeFilter',
            'forgetAfterFilter',
            'getBeforeFilters',
            'getAfterFilters',
            'getFilterer',
            'setFilterer',
            'callAction',
            'missingMethod',
            '__call'            
        );
        
        $allMethods = get_class_methods($controller);
        
        $result = array_diff($allMethods, $filterMethod);
        
        return $result;
        
    }
    
    
    public function getAssignedPermissions() {
        $assignedPermissions = DB::table('permission_role')
                ->get();
        return $assignedPermissions;
    }
    
    
    public function assignPermission($data){      
        $add = false;
        $getPermission = DB::table('permission_role')
                ->where('permission_id', $data['permission_id'])
                ->where('role_id', $data['role_id'])
                ->get();
        
        if(empty($getPermission)){ 
            //if there is no such permission, add it else delete it
            $add = true;
        }
        $assign = $this->assignPermissionRole($data,$add);
        return $assign;
    }
    
    private function assignPermissionRole($data,$add){  
        if($add){
            DB::table('permission_role')
                ->insert($data);    
            return true;
        }else{ //delete permission_role
            DB::table('permission_role')
                ->where('permission_id', $data['permission_id'])
                ->where('role_id', $data['role_id'])
                ->delete();              
            return true;
        }
        
        return false;
    }
    
    public function groupAssignPermission($data){ 
        $this->deleteAllRole($data['role_id']);
        if((bool) $data['group_assign']){            
            $this->addAllRole($data['role_id']);
        }
        return true;
    }
    
    private function deleteAllRole($role_id) {
        //delete all permission roles
        DB::table('permission_role')
                ->where('role_id', $role_id)
                ->delete();
        return;
    }
    
    private function addAllRole($role_id) {
        $getPermission = DB::table('permissions')
                ->get();
        foreach($getPermission as $permission){
            $permission_role = array(
                'permission_id' => $permission->id,
                'role_id' => $role_id
            );
            DB::table('permission_role')
                ->insert($permission_role); 
        }
        return;
    }
    
    
    public function hasAccessTo($controller,$action)
    {
        foreach ($this->roles as $role) {
            // Deprecated permission value within the role table.
            if (is_array($role->permissions) && in_array($action, $role->permissions) ) {
                return true;
            }

            // Validate against the Permission table
            foreach ($role->perms as $perm) {
                if ($perm->name == $permission) {
                    return true;
                }
            }
        }

        return false;
    }

}