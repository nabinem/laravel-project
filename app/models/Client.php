<?php

class Client extends \Eloquent {
    
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'clients';

    /**
     * Validataion
     */
    public static $rules = array(
        'hospital' => 'required',
        'address' => 'required',
        'postcode' => 'required',
        'place' => 'required',
        'customer_number' => 'required',
        'country_id' => 'required|numeric',
        'house_number' => 'required',
        'remarks' => 'required',
    );
    	
    protected $fillable = ['hospital', 'address', 'postcode', 'place', 'customer_number', 'country_id', 'house_number', 'remarks'];
    
    public function isValid($contacts) {

        $validation = Validator::make($this->attributes, static::$rules);
        
        $return1 = true;
        $return2 = true;
        $return3 = true;
        
        if ($validation->fails()){        
            $this->errors = $validation->messages();                
            $return1 = false;
        }     
                
        $ContactModel = new Contact;
        foreach($contacts as $key => $contact){
            if (!$ContactModel->fill($contacts[$key])->isValid()) { 
                if($this->errors == NULL){
                    $this->errors = $ContactModel->errors;
                }else{
                    $this->errors->merge($ContactModel->errors); 
                }
                $return2 = false;
            }
        }
        
        $contactEmailValidationError = $ContactModel->checkContactEmailValidity($contacts); 
        if ($contactEmailValidationError) {   
            
            $custom_message_bag = new Illuminate\Support\MessageBag;
            $custom_message_bag->add('contact_email', Lang::get('messages.Email should be unique.'));
                        
            if($this->errors == NULL){              
                $validation->getMessageBag()->add('Contact[1][email]', 'Name already exists!');
                $this->errors = $custom_message_bag;
            }else{
                $this->errors->merge($custom_message_bag);  
            }            
            $return3 = false;
        }
        
        if($return1 && $return2 && $return3){
            return 'valid';
        }
        
        return $this->errors;
    }
    
    /**
     * Relationship
     */
    // Clients __belongs_to__ Country
    public function country() {
        return $this->belongsTo('Country');
    }
    
    // Clients__hasMany__ Contact
    public function contacts() //plural for has many just a convention
    {
        return $this->hasMany('Contact');
    }
    
    // Clients__first__ Contact
    public function first_contact() //plural for has many just a convention
    {
        return $this->hasMany('Contact')->take(1);
    }
    
    public function deleteAssociatedContacts(){
        $this->contacts()->delete();
    }
    
    
}