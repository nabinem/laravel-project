<?php

class Staff extends \Eloquent {   
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'staffs';

    /**
     * Validataion
     */
    public static $rules = array(
        'name' => 'required',
        'phone_number' => 'required',
        'email' => 'required|email|unique:staffs,email,{id}',
    );
    	
    protected $fillable = ['name', 'phone_number', 'email'];
    
    public function isValid($id = null) {
        
        $rules = static::$rules;
            
        if(!empty($id)){
            $rules['email'] = str_replace('{id}', $id, $rules['email']);
        }
        
        $validation = Validator::make($this->attributes, $rules);

        if ($validation->passes())
            return true;
        
        $this->errors = $validation->messages();

        return false;
    }
    
}