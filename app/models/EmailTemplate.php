<?php

class EmailTemplate extends \Eloquent {
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'email_templates';

	// Add your validation rules here
	public static $rules = [
		'name' => 'required',
		//'code' => 'required|unique:email_templates,code,{id}',
		'code' => 'required|unique:email_templates,code',
		'subject' => 'required',
                'attachment' => 'mimes:doc,docx,pdf',
		'description' => 'required',
	];

	// Don't forget to fill this array
	protected $guarded = [];
    
    /**
     * Relationship
     */
    // Clients __belongs_to__ Country
    public function country() {
        return $this->belongsTo('Country');
    }
    
    public function isValid($id = null) {
            
        $rules = static::$rules;
                
        if(!empty($id)){
            unset($rules['code']);
            //$rules['code'] = str_replace('{id}', $id, $rules['code']);
        }
        
        $validation = Validator::make($this->attributes, $rules);

        if ($validation->passes())
            return true;
        
        $this->errors = $validation->messages();

        return false;
    }
    
    private function send_email($data){     
        
         if(isset($data['attachment']) && !empty($data['attachment'])){ 
            $attachmentLocation = EMAILATTACHMENTLOCATION. $data['attachment']; 
            if(File::exists($attachmentLocation)){
                $attachedFile = $attachmentLocation;
            }else{
                $attachedFile = false;
            }
        }else{
            $attachedFile = false;
        }
        
        Mail::send('email_templates.template', $data, function($message) use ($data, $attachedFile)
        {   
            $message->to($data['email'], $data['name'])->subject($data['subject']);
            if($attachedFile){
                $message->attach($attachedFile);
            }
        });
        
        return;
        
    }
    
    public function confirmation_email($contact_id, $client_id){        
        $code = 'sale_confirmation_email';
        
        $contactModel = new Contact;
        $contactDetails = $contactModel
                ->where('id', '=', $contact_id)                
                ->first(array('id','email','first_name'));
        
        $clientModel = new Client;
        $clientDetails = $clientModel
                ->where('id', '=', $client_id)                
                ->first(array('id','country_id','hospital'));
        
        $getEmailTemplate = $this->where('code', '=', $code)->where('country_id', '=', $clientDetails->country_id)->first();
        if(empty($getEmailTemplate)){
            $getEmailTemplate = $this->where('code', '=', $code)->where('country_id', '=', '51')->first();
        } //51 is for UK(english) as default
        
        $data = array(
            'name' => $contactDetails->first_name,
            'email' => $contactDetails->email,
            'description' => $getEmailTemplate->description,
            'attachment' => $getEmailTemplate->attachment,
            'subject' => $getEmailTemplate->subject
        );
        
        $data['description'] = str_replace("[#BUYER#]", $clientDetails->hospital, $data['description']);

        $this->send_email($data);
        
        return;
        
    }
    
    public function reminder_email(){        
        $code = 'reminder_email';
        
        $saleModel = new Sale;        
        $clientModel = new Client;
        
        $tomorrow = date('Y-m-d', strtotime("+1 day"));
        
        $saleReturnReminders = $saleModel
                ->leftJoin('contacts', 'contacts.id', '=', 'sales.contact_id')
                ->where('pickup_date', '=', $tomorrow)
                ->get();
        
        foreach ($saleReturnReminders as $reminder){  
        
            $clientDetails = $clientModel
                ->where('id', '=', $reminder->client_id)                
                ->first(array('id','country_id','hospital'));
            
            $getEmailTemplate = $this->where('code', '=', $code)->where('country_id', '=', $clientDetails->country_id)->first();
            if(empty($getEmailTemplate)){
                $getEmailTemplate = $this->where('code', '=', $code)->where('country_id', '=', '51')->first();
            } //51 is for UK(english) as default
            
            $data = array(
                'name' => $reminder->first_name,
                'email' => $reminder->email,
                'description' => $getEmailTemplate->description,
                'attachment' => $getEmailTemplate->attachment,
                'subject' => $getEmailTemplate->subject
            ); 
                    
            $data['description'] = str_replace("[#BUYER#]", $clientDetails->hospital, $data['description']);
            
            $this->send_email($data);            
        }       
        
    }

}