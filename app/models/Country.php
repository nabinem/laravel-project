<?php

class Country extends \Eloquent {
    
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'countries';
    
    /**
     * Relationship
     */
    // Country__hasMany__ Client
    public function clients() //plural for has many just a convention
    {
        return $this->hasMany('Client');
    }
    
}