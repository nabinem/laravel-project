<?php

class Sale extends \Eloquent {
    
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sales';

	// Add your validation rules here
	public static $rules = [
        'delivery_date' => 'required',
        'pickup_date' => 'required',
        'ok_date' => 'required',
        'client_id' => 'required',
        'contact_id' => 'required',
        'operator_name' => 'required',
        'order_number' => 'required',
        'driver_id' => 'required',
        'support_required' => 'required',
        'products' => 'required'
	];

	// Don't forget to fill this array
    protected $guarded = [];
    
    
        
    
    /**
     * Relationship
     */
    // Sale__hasMany__ SaleProducts
    public function saleproducts() //plural for has many just a convention
    {
        return $this->hasMany('SaleProduct');
    }    
    
    // Sales__belongs_to__ Client
    public function client() {
        return $this->belongsTo('Client');
    }
    
    // Sales__belongs_to__ Contact
    public function contact() {
        return $this->belongsTo('Contact');
    }

    // Sales__belongs_to__ Driver
    public function driver() {
        return $this->belongsTo('Driver');
    }

    // Sales__belongs_to__ Staff
    public function staff() {
        return $this->belongsTo('Staff');
    }
    
    
    
    
    public function getTotalProductCount($product_id){
        return DB::table('stocks')->where('product_id', '=', $product_id)->count();
    }
    
    public function getSoldProductCount($product_id){            
        $saleProductModel = new SaleProduct;
        $today = date('Y-m-d');
        $sales = $this->where('pickup_date', '>=', $today)->get();
        $soldProduct = array();
        $count = 0;
        //get all valid sales
        foreach($sales as $sale){   
            $saleproducts = $saleProductModel
                    ->where('product_id', '=', $product_id)
                    ->where('sale_id', '=', $sale->id)
                    ->count();
            $count = $count + $saleproducts;
        }
        
        return $count;
    }
    
    
    public function getFirstAvailableStockId($product_id, $delivery_date, $pickup_date){
        
        $saleProductModel = new SaleProduct;
        $today = date('Y-m-d');
        $currentSales = $saleProductModel
                ->leftJoin('sales', 'sales.id', '=', 'sale_products.sale_id')
                ->where('product_id', '=', $product_id)
                ->where(function($query) use ($delivery_date, $pickup_date, $today) {
                    $query->where(function($query) use ($delivery_date){
                        $query->where('sales.delivery_date', '<=', $delivery_date)
                                ->where('sales.pickup_date', '>=', $delivery_date);
                    })
                    ->orWhere(function($query) use ($pickup_date){
                        $query->where('sales.delivery_date', '<=', $pickup_date)
                                ->where('sales.pickup_date', '>=', $pickup_date);
                    });  
                })
                ->lists('stock_id');
        
        if(empty($currentSales)){
            $currentSales = array(0);
        }
        $stockModel = new Stock;
        $available_first = $stockModel
                ->where('product_id', '=', $product_id)
                ->whereNotIn('id', $currentSales)
                ->first();
        
        return $available_first->id;
        
    }
    
        
    public function print_for_driver($data, $fromPlanning = false){
        
        
        if(!$fromPlanning){
            $print_data['print_'.$data] = true;            
        }else{            
            $print_data = $data;
        }
        
        /*
          require_once '../vendor/phpoffice/phpword/src/PhpWord/Autoloader.php';
          \PhpOffice\PhpWord\Autoloader::register();
         */
        // Creating the new document...
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        
        
        foreach($print_data as $key => $plan){                
            $attr = explode("_", $key);
            if($attr[0] != 'agenda'){     
                $sale_products_id = $attr[1];
                
                
        
                $sale = $this->select( array('sales.*', DB::raw('clients.hospital as client_name'), DB::raw('products.name as product_name') ) )   
                        ->leftJoin('clients', 'clients.id', '=', 'sales.client_id')
                        ->leftJoin('sale_products', 'sale_products.sale_id', '=', 'sales.id')
                        ->leftJoin('products', 'products.id', '=', 'sale_products.product_id')
                        ->where('sale_products.id', '=', $sale_products_id)
                        ->first();

                $contactModel = new Contact;
                $contacts = $contactModel->where('client_id', '=', $sale->client_id)->get();

                /* Note: any element you append to a document must reside inside of a Section. */

                // Adding an empty Section to the document...
                $section = $phpWord->addSection();

                //Title Style of table 
                $phpWord->addFontStyle('titleStyle', array('bold' => true));
                $cellStyle = array('valign' => 'center', 'borderSize' => 10, 'borderColor' => '000000');

                $cellRowSpan = array('vMerge' => 'restart', 'valign' => 'center');
                $cellRowContinue = array('vMerge' => 'continue');

                $widthCol1 = 1330;
                $widthCol2 = 2660;
                $widthCol3 = 3990;

                $colspan2 = array_merge(array('gridSpan' => 2), $cellStyle);
                $colspan3 = array_merge(array('gridSpan' => 3), $cellStyle);



                $table = $section->addTable(array(
                    'cellMargin' => 0, 
                    'cellMarginRight' => 0, 
                    'cellMarginBottom' => 0, 
                    'cellMarginLeft' => 0,
                    'borderSize' => 6
                ));

                //First Row starts here
                $table->addRow();
                $table->addCell($widthCol1, $cellStyle)->addText("AFLEVERDATUM", 'titleStyle');       
                $table->addCell($widthCol1, $cellStyle)->addText($sale->delivery_date);
                $table->addCell($widthCol1, $cellStyle);
                $table->addCell($widthCol1, $cellRowSpan)->addText("LEVEREN LEENSET: \n BEHANDELD DOOR: \n CHAUFFEUR", 'titleStyle');
                $table->addCell($widthCol2, $colspan2)->addText("0256");    


                //Second Row starts here
                $table->addRow();
                $table->addCell($widthCol1, $cellStyle)->addText("OK-DATUM", 'titleStyle');       
                $table->addCell($widthCol1, $cellStyle)->addText($sale->ok_date);
                $table->addCell($widthCol1, $cellStyle)->addText($sale->time);
                $table->addCell($widthCol1, $cellRowContinue);
                $table->addCell($widthCol2, $colspan2)->addText("Aubin Breman");

                //Third Row starts here       
                $table->addRow();
                $table->addCell($widthCol1, $cellStyle)->addText("OPHAALDATUM", 'titleStyle');       
                $table->addCell($widthCol1, $cellStyle)->addText($sale->pickup_date);
                $table->addCell($widthCol1, $cellStyle);
                $table->addCell($widthCol1, $cellRowContinue);
                $table->addCell($widthCol2, $colspan2)->addText("KOERIEREXPRESS");

                //Fourth Row starts here       
                $table->addRow();
                $table->addCell($widthCol1, $cellStyle)->addText("ORDERNUMMER:", 'titleStyle');       
                $table->addCell($widthCol2, $colspan2)->addText($sale->order_number);
                $table->addCell($widthCol1, $cellStyle);
                $table->addCell($widthCol2, $colspan2);

                //Fifth Row starts here             
                $table->addRow();
                $table->addCell($widthCol3, $colspan3)->addText("LEVERSADRES:", 'titleStyle');  
                $table->addCell($widthCol3, $colspan3)->addText('CONTACTPERSON:', 'titleStyle');

                $addressCell = array_merge($cellRowSpan, array('gridSpan' => 3));
                $addressCellContinue = array('vMerge' => 'continue', 'gridSpan' => 3);

                //Sixth Row starts here       
                $table->addRow();
                $currentTableCell = $table->addCell($widthCol1, $addressCell);
                $currentTableCell->addText($sale->special_address);

                foreach($contacts as $key => $contact){  
                    if($key == 0){
                        $table->addCell($widthCol2, $colspan2)->addText($contact->first_name.' '.$contact->last_name);
                        $table->addCell($widthCol1, $cellStyle)->addText($contact->phone_number);
                    }
                }

                foreach($contacts as $key => $contact){  
                    if($key != 0){
                        $table->addRow();
                        $table->addCell(null, $addressCellContinue);
                        $table->addCell($widthCol2, $colspan2)->addText($contact->first_name.' '.$contact->last_name);
                        $table->addCell($widthCol1, $cellStyle)->addText($contact->phone_number);
                    }
                }

                //Next Row starts here
                $table->addRow();
                $table->addCell($widthCol3, $colspan3)->addtext('BIJZONDERHEDEN ADRES:', 'titleStyle');
                $table->addCell($widthCol3, $colspan3)->addtext('LEENSET:', 'titleStyle');

                //Next Row starts here
                $table->addRow();
                $table->addCell($widthCol3, $colspan3)->addtext($sale->special_address);
                $table->addCell($widthCol3, $colspan3)->addtext($sale->product_name);

                //Next Row starts here
                $table->addRow();
                $table->addCell($widthCol3, $colspan3)->addtext('HANDTEKENING AKKOORD AFLEVERING:', 'titleStyle');
                $table->addCell($widthCol3, $colspan3)->addtext('BIJZONDERHEDEN LEVERING:
        ', 'titleStyle');

                $rowSpanCol3 = array_merge($cellRowSpan,$colspan3);
                $rowSpanContCol3 = array_merge($cellRowContinue,$colspan3);
                //Next Row starts here
                $table->addRow();
                $table->addCell($widthCol3, $rowSpanCol3);
                $table->addCell($widthCol3, $rowSpanCol3)->addtext('Meenemen RHS.45');

                $table->addRow();
                $table->addCell(null, $rowSpanContCol3);
                $table->addCell(null, $rowSpanContCol3);
                        
                $section->addPageBreak();
            }
        }
        
        
        
        $file = 'order_detail.docx';
        header("Content-Description: File Transfer");
        header('Content-Disposition: attachment; filename="' . $file . '"');
        header("Content-Type: application/docx");
        header('Content-Transfer-Encoding: binary');
        header("Cache-Control: public");
        header('Expires: 0');
        
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');  
        //$objWriter->save(base_path().'/files/'.$file); 
        $objWriter->save("php://output"); 
        
        return;
    }
    
    
    public function getDeliveryArchive($search = null){
        $deliveryArchiveData = $this->select( array(   
                    'sales.delivery_date',
                    'sales.order_number',
                    //'sales.special_address',
                    'sales.remarks', 
                    DB::raw('clients.hospital as client_name'), 
                    DB::raw('products.name as product_name'), 
                    DB::raw('drivers.name as driver_name'), 
                    DB::raw('sale_products.id as sale_product_id')
                ) )    
                ->leftJoin('clients', 'clients.id', '=', 'sales.client_id')
                ->leftJoin('sale_products', 'sale_products.sale_id', '=', 'sales.id')
                ->leftJoin('products', 'products.id', '=', 'sale_products.product_id')
                ->leftJoin('drivers', 'drivers.id', '=', 'sales.driver_id')
                ->where('delivery_archived', '=', 1)      
                ->where(function($query) use ($search) {
                    return $query->where('products.name', 'like', '%'.$search.'%')
                        ->orWhere('clients.hospital', 'like', '%'.$search.'%')
                        ->orWhere('drivers.name', 'like', '%'.$search.'%');
                })
                ->orderBy('delivery_date', 'desc')
                ->paginate(LIMIT);
        
        return $deliveryArchiveData;
    }
    
    
    public function getPickupArchive($search){
        $pickupArchiveData = $this->select( array(
                    'sales.pickup_date',
                    'sales.order_number',
                    //'sales.special_address',
                    'sales.remarks', 
                    DB::raw('clients.hospital as client_name'), 
                    DB::raw('products.name as product_name'),                 
                    DB::raw('drivers.name as driver_name'), 
                    DB::raw('sale_products.id as sale_product_id')
                ) )    
                ->leftJoin('clients', 'clients.id', '=', 'sales.client_id')
                ->leftJoin('sale_products', 'sale_products.sale_id', '=', 'sales.id')
                ->leftJoin('products', 'products.id', '=', 'sale_products.product_id')
                ->leftJoin('drivers', 'drivers.id', '=', 'sales.driver_id')
                ->where('pickup_archived', '=', 1)  
                ->where(function($query) use ($search) {
                    return $query->where('products.name', 'like', '%'.$search.'%')
                        ->orWhere('clients.hospital', 'like', '%'.$search.'%')
                        ->orWhere('drivers.name', 'like', '%'.$search.'%');
                })
                ->orderBy('pickup_date', 'desc')
                ->paginate(LIMIT);
        
        return $pickupArchiveData;
    }
    
    
    public function getStaffArchive($search){
        $staffArchiveData = $this->select( array(
                    'sales.ok_date',
                    'sales.time',
                    'sales.order_number',
                    //'sales.special_address',
                    'sales.remarks', 
                    DB::raw('clients.hospital as client_name'), 
                    DB::raw('products.name as product_name'),              
                    DB::raw('drivers.name as driver_name'),
                    DB::raw('sale_products.id as sale_product_id')
                ) )    
                ->leftJoin('clients', 'clients.id', '=', 'sales.client_id')
                ->leftJoin('sale_products', 'sale_products.sale_id', '=', 'sales.id')
                ->leftJoin('products', 'products.id', '=', 'sale_products.product_id')
                ->leftJoin('drivers', 'drivers.id', '=', 'sales.driver_id')
                ->leftJoin('staffs', 'staffs.id', '=', 'sales.staff_id')
                ->where('support_required', '=', 1)
                ->where('support_archived', '=', 1)  
                ->where(function($query) use ($search) {
                    return $query->where('products.name', 'like', '%'.$search.'%')
                        ->orWhere('clients.hospital', 'like', '%'.$search.'%')
                        ->orWhere('drivers.name', 'like', '%'.$search.'%');
                })
                ->orderBy('ok_date', 'desc')
                ->paginate(LIMIT);
        
        return $staffArchiveData;
    }

    
    
    /**
     * FUNCTION NOT USED AT THE MOMENT
     * @param type $delivery_date
     * @param type $pickup_date
     * @param type $product_id
     * @param type $quantity
     * @return boolean
     */
    /*public function check_product_avaibility($delivery_date, $pickup_date, $product_id, $quantity) {
        $result = $this->select(array(
                'sales.id',
                DB::raw('sale_products.id as sale_products_id'),
                DB::raw('stocks.id as stocks_id'),
                DB::raw('stocks.product_id as stocks_product_id'),
                DB::raw('(select count(*) from stocks where stocks.product_id = ' . $product_id . ') as total')
            ))
            ->leftJoin('sale_products', 'sale_products.sale_id', '=', 'sales.id')
            ->leftJoin('stocks', 'stocks.id', '=', 'sale_products.stock_id')
            ->where('sale_products.product_id', '=', $product_id)
            ->where(function($query) use ($delivery_date, $pickup_date) {
                $query->whereBetween('deldivery_date', array($delivery_date, $pickup_date))
                ->orWhereBetween('pickup_date', array($delivery_date, $pickup_date));
            })
            ->get();
            
        if(!$result->isEmpty()){
            $total = $result->first()->total;
            $onRent = $result->count();
            $available = $total - $onRent;
            if($quantity > $available){
                return false;//not valid
            }
            return true;
        }
                
        return true;
    }
    */
}