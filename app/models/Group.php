<?php

class Group extends \Eloquent {
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'roles';

    /**
     * Validataion
     */
    public static $rules = array(
        'name' => 'required|unique:roles,name,{id}'
    );
    
    protected $fillable = ['name'];
    
    public function isValid($id = null) {
            
        $rules = static::$rules;
        
        if(!empty($id)){
            $rules['name'] = str_replace('{id}', $id, $rules['name']);
        }
        
        $validation = Validator::make($this->attributes, $rules);

        if ($validation->passes())
            return true;
        
        $this->errors = $validation->messages();

        return false;
    }

}
