<?php

class Stock extends \Eloquent {

	// Add your validation rules here
	public static $rules = [		
        'serial_number' => 'unique:stocks,serial_number,{id}',        
	];
    
    public static $amount_rule = [
        'amount' => 'required|numeric'
    ];

	// Don't forget to fill this array
	protected $fillable = ['serial_number'];
    
    public function isValid($id = null) {
            
        $rules = static::$rules;
        
        if(!empty($id)){
            $rules['serial_number'] = str_replace('{id}', $id, $rules['serial_number']);
        }
        
        $validation = Validator::make($this->attributes, $rules);

        if ($validation->passes())
            return true;
        
        $this->errors = $validation->messages();

        return false;
    }
    
    
    /**
     * Relationship
     */
    // Stocks __belongs_to__ Product
    public function product() {
        return $this->belongsTo('Product');
    }
    
    // Stock__hasMany__ SaleProducts
    public function saleproducts() //plural for has many just a convention
    {
        return $this->hasMany('SaleProduct');
    }  
    
    public function getStockDetails($stock_id){
        
        $saleProductModel = new SaleProduct;
        $today = date('Y-m-d');
        $currentSaleStockDetails = $saleProductModel
                ->leftJoin('sales', 'sales.id', '=', 'sale_products.sale_id')                
                ->leftJoin('clients', 'clients.id', '=', 'sales.client_id')
                ->where('stock_id', '=', $stock_id)
                ->where('pickup_date', '>=', $today)
                ->first(array('sales.id','sale_products.id','sale_products.stock_id','sales.pickup_date', 'clients.hospital'));
        
        if(empty($currentSaleStockDetails)){
            return null;
        }
        return $currentSaleStockDetails;
        
    }

}