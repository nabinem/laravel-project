<?php

use Zizaco\Confide\ConfideUser;
use Zizaco\Confide\ConfideUserInterface;
use Zizaco\Entrust\HasRole;

class User extends Eloquent implements ConfideUserInterface
{
    use ConfideUser;
    use HasRole;
    
    protected $fillable = ['username', 'password', 'email'];
    
/*
    public $timestamps = false;
    
    protected $fillable = ['username', 'password', 'email'];
    
    public static $rules = [
        'username' => 'required', 
        'password' => 'required'
    ];
    
    public $errors;


    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	//protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	/*protected $hidden = array('password', 'remember_token');
    
    public function isValid(){
        
        $validation = Validator::make($this->attributes, static::$rules); 
        
        if($validation->passes()) return true;        
        
        $this->errors = $validation->messages();
        
        return false;
        
    }*/

}
