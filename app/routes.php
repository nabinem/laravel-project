<?php

// Confide routes
Route::get('users/login', 'UsersController@login');
Route::post('users/login', 'UsersController@doLogin');
Route::get('users/confirm/{code}', 'UsersController@confirm');
Route::get('users/forgot_password', 'UsersController@forgotPassword');
Route::post('users/forgot_password', 'UsersController@doForgotPassword');
Route::get('users/reset_password/{token}', 'UsersController@resetPassword');
Route::post('users/reset_password', 'UsersController@doResetPassword');
Route::get('users/logout', 'UsersController@logout');
Route::get('logout', 'UsersController@logout');

Route::get('/', 'HomeController@dashboard')->before('auth');


/*Allow only after authentication*/
Route::group(array('before' => 'auth'), function () {

    Route::resource('users', 'UsersController', array('except' => array('show')));
    
    Route::get('permissions', 'PermissionsController@index');
    Route::get('permissions/synchronize', 'PermissionsController@synchronize');
    Route::get('permissions/manage_role', 'PermissionsController@manage_role');
    Route::post('permissions/ajax_set_permission', 'PermissionsController@ajax_set_permission');
        
    Route::resource('clients', 'ClientsController', array('except' => array('show')));
    Route::get('clients/contact_person/{contact_id}', 'ClientsController@contact_person');
    Route::post('clients/ajax_get_contacts_list', 'ClientsController@ajax_get_contacts_list');
    
    Route::resource('drivers', 'DriversController', array('except' => array('show','create','edit')));
    Route::get('drivers/{id}', 'DriversController@index');
    
    Route::resource('staffs', 'StaffsController', array('except' => array('show','create','edit')));
    Route::get('staffs/{id}', 'StaffsController@index');
    
    Route::resource('products', 'ProductsController', array('except' => array('show','create','edit')));
    Route::post('products/ajax_get_product_availability', 'ProductsController@ajax_get_product_availability');
    Route::post('products/ajax_get_product_options', 'ProductsController@ajax_get_product_options');
    
    Route::resource('stocks', 'StocksController', array('except' => array('show','create','edit','index')));
    Route::get('stocks/{product_id}', 'StocksController@index');    
    Route::post('stocks/ajax_add_serial', 'StocksController@ajax_add_serial');
        
    Route::resource('sales', 'SalesController', array('except' => array('edit')));
    Route::get('sales/print_for_driver/{id}', 'SalesController@print_for_driver');
    Route::post('sales/ajax_check_product_validity', 'SalesController@ajax_check_product_validity');
    Route::get('sales', function()//since they don't want sales overview, they want to view from plannings
    {
        return Redirect::to('sales/create'); 
    });
    
    Route::resource('email_templates', 'EmailTemplatesController', array('except' => array('show')));
    Route::get('reminder_email', 'EmailTemplatesController@reminder_email');
    
    Route::resource('agendas', 'AgendasController', array('except' => array('show','create','edit')));
    Route::get('agendas/{id}', 'AgendasController@index');
    Route::get('agendas/view/{id}', 'AgendasController@show');
    
    Route::resource('archives', 'ArchivesController', array('except' => array('show','edit')));
    
    Route::get('plannings', 'PlanningsController@overview');
    Route::get('archives', 'PlanningsController@archives');
    Route::get('archives/{string}', 'PlanningsController@archives');
    //Route::get('print_drivers', 'PlanningsController@print_drivers');
    Route::post('plannings/action', array('uses' => 'PlanningsController@action'));    
    
    Route::resource('groups', 'GroupsController', array('except' => array('show')));
});
