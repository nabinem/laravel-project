<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSalesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sales', function(Blueprint $table)
		{
			$table->increments('id');
            $table->date('delivery_date');   
            $table->date('pickup_date'); 
            $table->date('ok_date');  
            $table->time('time')->nullable();    
            $table->integer('client_id');
            $table->integer('contact_id');
            $table->string('operator_name');
            $table->string('order_number');
            $table->boolean('support_required')->default(0); 
            $table->integer('staff_id')->nullable();    
            $table->integer('driver_id');
            $table->text('special_address'); 
            $table->text('remarks')->nullable();     
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sales');
	}

}
