<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAgendasTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('agendas', function(Blueprint $table) {
                    $table->increments('id');
                    $table->string('title');
                    $table->date('agenda_date');
                    $table->time('time');
                    $table->boolean('agenda_archived')->default(0);
                    $table->timestamps();
                });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('agendas');
    }

}
