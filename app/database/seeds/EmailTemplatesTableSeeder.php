<?php

class EmailTemplatesTableSeeder extends Seeder {

	public function run()
	{
        //delete email_templates table records
        DB::table('email_templates')->delete();
        //insert records
        DB::table('email_templates')->insert(array(
            array(
                'id' => '1', 
                'country_id' => '51', 
                'name' => 'Sale Confirmation Email',
                'code' => 'sale_confirmation_email',
                'subject' => 'Sale Confirmation Email.', 
                'description' => '<p>Dear [#BUYER#],</p><p>Thank you for purchasing. Please return in time.</p><p>Sincerely,</p><p>Tornier</p>'
            ),
            array(
                'id' => '2', 
                'country_id' => '51', 
                'name' => 'Reminder Email',
                'code' => 'reminder_email',
                'subject' => 'Reminder to return product.', 
                'description' => '<p>Dear [#BUYER#],</p><p>Please return our product by tomorrow.</p><p>Sincerely,</p><p>Tornier</p>'
            ),
            array(
                'id' => '3', 
                'country_id' => '15', 
                'name' => 'Vente Confirmation Email',
                'code' => 'sale_confirmation_email',
                'subject' => 'Vente Confirmation Email', 
                'description' => '<p>Cher [#BUYER#],</p><p>Merci pour votre achat. Se il vous pla&icirc;t revenir &agrave; temps.</p><p>Cordialement,</p><p>Tornier</p>'
            ),
            array(
                'id' => '4', 
                'country_id' => '15', 
                'name' => 'Rappel Email',
                'code' => 'reminder_email',
                'subject' => 'Rappel de retourner le produit', 
                'description' => '<p>Cher [#BUYER#],</p><p>Se il vous pla&icirc;t retourner notre produit d&#39;ici demain.</p><p>Cordialement,</p><p>Tornier</p>'
            ),
            array(
                'id' => '5', 
                'country_id' => '16', 
                'name' => 'Verkauf Bestätigung per E-Mail',
                'code' => 'sale_confirmation_email',
                'subject' => 'Verkauf Bestätigung per E-Mail', 
                'description' => '<p>Sehr geehrter&nbsp;[#BUYER#],</p><p>Danke f&uuml;r den Kauf. Bitte senden Sie in der Zeit.</p><p>Mit freundlichen Gr&uuml;&szlig;en,</p><p>Tornier</p>'
            ),
            array(
                'id' => '6', 
                'country_id' => '16', 
                'name' => 'Erinnerung per E-Mail',
                'code' => 'reminder_email',
                'subject' => 'Reminder, Produkt zurückzubringen', 
                'description' => '<p>Sehr geehrter&nbsp;[#BUYER#],</p><p>Bitte schicken Sie unser Produkt von morgen.</p><p>Mit freundlichen Gr&uuml;&szlig;en,</p><p>Tornier</p>'
            ),
            array(
                'id' => '7', 
                'country_id' => '36', 
                'name' => 'Verkoop bevestigingsmail',
                'code' => 'sale_confirmation_email',
                'subject' => 'Verkoop bevestigingsmail', 
                'description' => '<p>Beste [#BUYER#],</p><p>Bedankt voor uw aankoop. Gelieve terug te keren in de tijd.</p><p>Met vriendelijke groet,</p><p>Tornier</p>'
            ),
            array(
                'id' => '8', 
                'country_id' => '36', 
                'name' => 'Herinnering E-mail',
                'code' => 'reminder_email',
                'subject' => 'Herinnering om het product terug te keren', 
                'description' => '<p>Beste [#BUYER#],</p><p>Gelieve te keren ons product morgen.</p><p>Met vriendelijke groet,</p><p>Tornier</p>'
            ),
        ));

        $this->command->info('Email Templates Inserted!');
	}

}