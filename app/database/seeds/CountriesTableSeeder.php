<?php

class CountriesTableSeeder extends Seeder {

    public function run() {

        //delete countries table records
        DB::table('countries')->delete();
        //insert records
        DB::table('countries')->insert(array(
            array('id' => '1', 'name' => 'Albania'),
            array('id' => '2', 'name' => 'Andorra'),
            array('id' => '3', 'name' => 'Austria'),
            array('id' => '4', 'name' => 'Belarus'),
            array('id' => '5', 'name' => 'Belgium'),
            array('id' => '6', 'name' => 'Bosnia and Herzegovina'),
            array('id' => '7', 'name' => 'Bulgaria'),
            array('id' => '8', 'name' => 'Croatia'),
            array('id' => '9', 'name' => 'Cyprus'),
            array('id' => '10', 'name' => 'Czech Republic'),
            array('id' => '11', 'name' => 'Denmark'),
            array('id' => '12', 'name' => 'Estonia'),
            array('id' => '13', 'name' => 'Faroe Islands'),
            array('id' => '14', 'name' => 'Finland'),
            array('id' => '15', 'name' => 'France'),
            array('id' => '16', 'name' => 'Germany'),
            array('id' => '17', 'name' => 'Gibraltar'),
            array('id' => '18', 'name' => 'Greece'),
            array('id' => '19', 'name' => 'Guernsey'),
            array('id' => '20', 'name' => 'Hungary'),
            array('id' => '21', 'name' => 'Iceland'),
            array('id' => '22', 'name' => 'Ireland'),
            array('id' => '23', 'name' => 'Isle of Man'),
            array('id' => '24', 'name' => 'Italy'),
            array('id' => '25', 'name' => 'Jersey'),
            array('id' => '26', 'name' => 'Kosovo'),
            array('id' => '27', 'name' => 'Latvia'),
            array('id' => '28', 'name' => 'Liechtenstein'),
            array('id' => '29', 'name' => 'Lithuania'),
            array('id' => '30', 'name' => 'Luxembourg'),
            array('id' => '31', 'name' => 'Macedonia'),
            array('id' => '32', 'name' => 'Malta'),
            array('id' => '33', 'name' => 'Moldova'),
            array('id' => '34', 'name' => 'Monaco'),
            array('id' => '35', 'name' => 'Montenegro'),
            array('id' => '36', 'name' => 'Netherlands'),
            array('id' => '37', 'name' => 'Norway'),
            array('id' => '38', 'name' => 'Poland'),
            array('id' => '39', 'name' => 'Portugal'),
            array('id' => '40', 'name' => 'Romania'),
            array('id' => '41', 'name' => 'Russia'),
            array('id' => '42', 'name' => 'San Marino'),
            array('id' => '43', 'name' => 'Serbia'),
            array('id' => '44', 'name' => 'Slovakia'),
            array('id' => '45', 'name' => 'Slovenia'),
            array('id' => '46', 'name' => 'Spain'),
            array('id' => '47', 'name' => 'Svalbard and Jan Mayen'),
            array('id' => '48', 'name' => 'Sweden'),
            array('id' => '49', 'name' => 'Switzerland'),
            array('id' => '50', 'name' => 'Ukraine'),
            array('id' => '51', 'name' => 'United Kingdom'),
            array('id' => '52', 'name' => 'Vatican City'),
            array('id' => '53', 'name' => 'Åland')
        ));

        $this->command->info('European Countries Inserted!');
    }

}