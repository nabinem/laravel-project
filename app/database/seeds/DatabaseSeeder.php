<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		//Eloquent::unguard();
		
        //Create Admin User
        $admin = new User;
        $admin->username = 'administrator';
        $admin->email = 'admin@tornier.com';
        $admin->password = '!Q2w3e4';
        $admin->password_confirmation = '!Q2w3e4';
        $admin->confirmation_code = md5(uniqid(mt_rand(), true));     
        $admin->confirmed = 1;
        $admin->save();       
        
        $this->command->info('Created user "' . $admin->username . '" <' . $admin->email . '>');
        
        //Create Roles
        $admin_role = new Role;
        $admin_role->name = 'Admin';
        $admin_role->save();        
        
        $user_role = new Role;
        $user_role->name = 'Manager';
        $user_role->save();
        
        $this->command->info('Roles Created!');
        
        //Find admin user
        $user = User::where('username','=',$admin->username)->first(); //see previous seed

        /* role attach alias */
        $user->attachRole( $admin_role ); // Parameter can be an Role object, array or id.
        
        $this->command->info('Roles Attached to admin!');
	}

}
